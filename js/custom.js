
$(window).on('load',function() {
    $('#loading').fadeOut("slow");
    hideEmpty();
});

// video auto scroll

$(".videos").click(function(e){
  e.preventDefault();
  var toTop = $(".header2").offset().top - 85;
   console.log(toTop)
  $("html, body").animate({scrollTop: toTop},1000);

})


$(".subscribe").click(function(e){
  e.preventDefault();
  $(".modal-body").fadeIn();
})

$(".fa-close").click(function(){
  $(".modal-body").fadeOut();
})


$(window).resize(function(){
  appendScores();
});

function appendScores() {
  var winWidth = $(window).width();
  if (winWidth < 740){
     $("#score-list").appendTo("#scores");
  } 
  else {
     $("#score-list").appendTo(".append-back")
  }
};


$('.nav-mobile').hide();   


 $('.menu-mobile').click(function(){ 
  $("body, html").css({
      "height": "auto",
      "overflow": "hidden"
    });
  $('.nav-mobile').show().animate({top:"0px"}, "slow");                                            //on menu arrow click
  }

      );
 $('.x-mobile').click(function(){                                           //on menu arrow click
    $(".nav-mobile").animate({top:"-110%"}, "slow"); 
    $("body, html").css({
      "height": "auto",
      "overflow": "initial"
    });
  });
  $('.background-mobile').click(function(){                                           //on menu arrow click
    $(".nav-mobile").animate({top:"-110%"}, "slow"); 
  });



$(window).resize(function(){
    if( $(window).width() < 740 ){
        $('source').removeAttr('src');
      }
});



function hideEmpty() {
    $(".vids").each(function(){

     if($(this).children('tag:visible').length == 0) {
        $(this.parentNode.parentNode).hide(); 
      }
    else {
        $(this.parentNode.parentNode).show();
    }
  });

}




/*
        $( window ).resize(function() {
          simplemaps_worldmap.refresh();                                    //on resize refresh the map 
          var element = document.getElementsByClassName('right');           //look at the refreshed maps 
          var mapHeight = element[0].offsetHeight;                          //gets the hight 
        $('.left').css("height", mapHeight); 
    $('.fb-page').css("height", mapHeight); //sets the new menu height 
        });
       



// ------------------------------------- show all click 

$(".showall").click(function(){
   $('.arrows').show();
   $('.vids > tag').show();
   simplemaps_worldmap.region_zoom(-1);
   hideEmpty();
$(document).ready(function(){
onResize = function() {   
  if( $(window).width() > 740 )
{
   for (var location in simplemaps_worldmap_mapdata.locations){ 
      loc=simplemaps_worldmap_mapdata.locations[location];

        if (loc.hide!='no'){loc.hide='no'}                            //Hide or unhide it
        else{loc.hide='no'}
          }
    working=true;                                             //The map is processing
    simplemaps_worldmap.refresh(); 
    }
} 
$(document).ready(onResize);
$(window).bind('resize', onResize);
});
});


// ------------------------------------- 2nd nav toggles map icons



var working=false;                                              //Is the map processing right now? No
$(document).ready(function(){                                       //jQuery waits for the page to load first
  $(".group_toggle").click(function(event){                           //When links with this class are clicked...
   $('.arrows').show();
   $('.vids > tag').show();
    if (working){return}                                              //Don't do anything if map is processing
    var group_id=parseInt(this.id.substring(5));                         //Get the group # from the link's id 
    var catNames = [ "events", "games", "profiles", "featured" ];         //header2 vid list sort
    var catSelection = catNames[group_id];
    $(".vids > tag").each(function(){
        if (($(this).attr("class").split(' ')[3])==catSelection) {
          $(this).show();
           }
        else {
          $(this).hide();
        }
        hideEmpty();
    });

$(document).ready(function(){
onResize = function() {   
  
  if( $(window).width() > 740 )
{

    for (var location in simplemaps_worldmap_mapdata.locations){          //Iterate over locations in mapdata file
      loc=simplemaps_worldmap_mapdata.locations[location];
      if (loc.group==group_id){                                   //If the location is in this link's group
        if (loc.hide!='yes'){loc.hide='no'}                            //Hide or unhide it
        else{loc.hide='no'}
      }
    else 
    {
      (loc.hide='yes');
    }
    }
    working=true;                                             //The map is processing
    simplemaps_worldmap.refresh();
        }
} 
$(document).ready(onResize);
$(window).bind('resize', onResize);
});                                //Make the map reflect the changes to the mapdata.js file
  });
});
 


simplemaps_worldmap.hooks.refresh_complete=function(){                      
  working=false;                                              //The map is done processing
};

simplemaps_worldmap.hooks.complete=function(){ 

var element = document.getElementsByClassName('right');              //look for the class map (named right)
        var mapHeight = element[0].offsetHeight;                            // look at the height of the map
        $('.left').css("height", mapHeight);       


simplemaps_worldmap.hooks.back=function(){
  $(".arrows").show();
  $('.vids > tag').show();
  hideEmpty();
}

simplemaps_worldmap.hooks.zoomable_click_region=function(id){  // on contient click it sorts the vid list, send the region number    
 $( ".vids > tag" ).each(function() {               
  var selectionName = ("." + id);                         // gets region(id) and adds a period  
  var tagClassName = ("." + $(this).attr("class").split(' ')[0]);      // looks at the first class in <tag>
    if (tagClassName==selectionName){                                   //compares the two 
      $(".arrows").show();                                              //shows the list 
      $(this).show();                                                   
      hideEmpty();
    }
    else {
      $(this).hide();
      hideEmpty();
    }
   });
 }
};

// ---------------------------------------------------------------------------------------------------------------------------------- location click sort

simplemaps_worldmap.hooks.click_location = function(id) {
  var continentId = id.split("_")[0];
  var clubId = id.split("_")[2];
  var catId = id.split("_")[3];
  simplemaps_worldmap.region_zoom(continentId);         //zooms in on the continent on club click 

  $( ".vids > tag" ).each(function() {    
  var selectionNameClub = ("." + clubId);                         // gets region(id) and adds a period 
  var selectionNameCat=  ("." + catId);   
  var tagClassNameClub = ("." + $(this).attr("class").split(' ')[2]);    // looks at the club in the class <tag>
  var tagClassNameCat = ("." + $(this).attr("class").split(' ')[3]);    // looks at the club in the class <tag>


    if ((tagClassNameClub==selectionNameClub)&&(tagClassNameCat==selectionNameCat)){                                   //compares the two 
      $(".arrows").show();                                              //shows the list 
      $(this).show();                                                   
      hideEmpty();
    }
    else {
      $(this).hide();
      hideEmpty();
    }
   });


};

$('.regions > li').hide();
$('.clubs > li').hide();


*/

// ---------------------------------------------------------------------------------------------------------------------------------- scrolling videos



$('.right-arrow').click(function() {
   var obj = $(this).parents(".arrows").find(".cut-off"),
        dis = $(obj).scrollLeft();
        quarter  = ($(obj).width() * .5); 
  
    $(obj).animate({scrollLeft: dis+quarter}, 1000);
});


$('.left-arrow').click(function() {
   var obj = $(this).parents(".arrows").find(".cut-off"),
        dis = $(obj).scrollLeft();
        quarter  = ($(obj).width() * .5); 
 $(obj).animate({scrollLeft: dis-quarter}, 1000);
});






$(document).ready(function(){
  centerVid();
});

$(window).resize(function(){
  centerVid();
});

function centerVid() {
    var height = $(".vid-contain > iframe").height();
    var streamHeight = (height/2)-height;
   $(".live-stream").css("margin-top", streamHeight);
};


$(".close-stream").click(function(){
  $(".hide-vid").fadeOut();
  $(".vid-contain > iframe").removeAttr("src");
});


$(".live-stream").click(function(){
  $(".hide-vid").fadeIn();
  $(".vid-contain > iframe").addAttr("src");
});



 $('.vids > tag').each(function(){
 var videoType = $(this).find("a").data("videosite");
  if (videoType == "vimeo") {
       var vidid = $(this).find("[data-vimeo='vimeo-img']").attr('class').split('-')[1];
     //vimeoLoadingThumb(id);

     $.ajax({
        type: 'POST',
        url: 'save_img.php',
        data: {'id': vidid},
        success: function(response){
             console.log(response)
         }
     });  
  }
});


function vimeoLoadingThumb(id){    
    var url = "http://vimeo.com/api/v2/video/" + id + ".json?callback=showThumb";
      
    var id_img = ".vimeo-" + id;
    var script = document.createElement( 'script' );
    script.type = 'text/javascript';
    script.src = url;

    $(id_img).before(script);
}

function showThumb(data){
    var id_img = ".vimeo-" + data[0].id, 
        url = data[0].thumbnail_medium

    $(id_img).css({
      'background':'url('+url+') no-repeat center center',
      'background-size': 'cover'
    });
}







$('.vids > tag').each(function(){
 var videoType = $(this).find("a").data("videoid");
   var urlName = window.location.hash.split('#')[1];
   if (videoType == urlName) {
      $(this).find("a").click();
      return false;
   }
});


$(document).ready(function(){
  var winWidth = $(window).width();
  if (winWidth < 740){
    mobileBtn();
  }
  scoreLimit(winWidth);
});



$(window).resize(function(){
var winWidth = $(window).width();
if (winWidth < 768){
  mobileBtn();
}
scoreLimit(winWidth);
});


function mobileBtn() {
  $(".header > .headerlinks > li > .append").appendTo("#append");
}


$(document).ready(function(){
  $(".blog_post").first().addClass("active");
  $(".active").clone().appendTo(".large-col");
 
});


$(window).on('load',function(){
     $("#map").slideUp();
  $(".clubs").slideUp();
  appendScores();
});


$(".small-col > .blog_post").click(function(){
  $(".small-col > .blog_post").each(function(){
    $(this).removeClass("active");
  });
  $(this).addClass("active");
  $(".large-col > .blog_post").fadeOut();
  $(".large-col > .blog_post").remove();
   $(".active").clone().appendTo(".large-col");
});


$(".map-btn").click(function(){
    $("#map").slideToggle();

    $("html, body").animate({ scrollTop: $('.map-btn').offset().top - 100 }, 1000);

setTimeout(function() {
  var element = document.getElementsByClassName('right');           //look at the refreshed maps 
          var mapHeight = element[0].offsetHeight;                          //gets the hight 
        $('.left').css({
            "height": mapHeight,
            "transition": "all 0.3s ease-in-out" 


          }); 
}, 400);


});



 window.setInterval(function(){
   timer=0; 
},500); 

                    var swipeNumb = 0,
                        max = 0;


                    $(".scroller > .blog_post").each(function(){
                        max++;
                    });



  $(function() {      
          $(".scroller").swipe( {

            swipeStatus:function(event, phase, direction, distance, duration, fingers)
            {      
                var width = $(".scroller").outerWidth();
                if ((direction == "right")){

                     while(timer==0){
                      if (swipeNumb < 0){
                        swipeNumb = 0;
                      }
                      else {
                        swipeNumb--;
                      }
                        
                        scroller(swipeNumb, width);
                      timer++;
                    }

                }
                if ((direction == "left")){

                     while(timer==0){
                      if (swipeNumb < max){
                        swipeNumb++;
                      }
                      else{
                        swipeNumb = max;
                      }
                      
                        scroller(swipeNumb, width);
                      timer++;
                    }


                }
                 if ((direction == "up")){

                    return false;


                }
                if ((direction == "down")){

                    return false;


                }

            },
            threshold:100,
            maxTimeThreshold:1000,
            fingers:'all'
          });
        });

function scroller(swipeNumb, width){
 $(".scroller").animate({scrollLeft: swipeNumb*width}, 1000)
}


function scoreLimit(winWidth) {
  var count = 0;
  if (winWidth < 740){
      $(".score_post").each(function(){
          if(count < 5){
            $(this).show();
          }
          else {
            $(this).hide();
          }
          count++;
      })
  } 
  else {
    $(".score_post").show();
  }
}




