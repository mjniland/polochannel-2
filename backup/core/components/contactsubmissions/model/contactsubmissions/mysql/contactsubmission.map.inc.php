<?php

$xpdo_meta_map[ 'ContactSubmission' ] = array(
	 'package' => 'contactsubmissions',
	 'table' => 'contact_submissions',
	 'fields' =>
	 array(
		  'name' => '',
		  'email' => '',
		  'phone' => '',
		  'company' => '',
 		  'message' => '',
		  'createdon' => ''
	 ),
	 'fieldMeta' =>
	 array(
		  'name' =>
		  array(
				'dbtype' => 'varchar',
				'precision' => '255',
				'phptype' => 'string',
				'null' => false,
				'default' => '',
		  ),
		  'email' =>
		  array(
				'dbtype' => 'varchar',
				'precision' => '255',
				'phptype' => 'string',
				'null' => false,
				'default' => '',
		  ),
		  'phone' =>
		  array(
				'dbtype' => 'varchar',
				'precision' => '255',
				'phptype' => 'string',
				'null' => false,
				'default' => '',
		  ),
		   'company' =>
		  array(
				'dbtype' => 'varchar',
				'precision' => '255',
				'phptype' => 'string',
				'null' => false,
				'default' => '',
		  ),
		  'message' =>
		  array(
				'dbtype' => 'text',
				'phptype' => 'string',
				'null' => true,
				'default' => '',
		  ),
		  'createdon' =>
		  array(
				'dbtype' => 'datetime',
				'phptype' => 'datetime',
				'null' => false,
		  ),
	 ),
);