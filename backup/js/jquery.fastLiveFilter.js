/**
 * fastLiveFilter jQuery plugin 1.0.3
 * 
 * Copyright (c) 2011, Anthony Bush
 * License: <http://www.opensource.org/licenses/bsd-license.php>
 * Project Website: http://anthonybush.com/projects/jquery_fast_live_filter/
 **/

jQuery.fn.fastLiveFilter = function(list, options) {
	// Options: input, list, timeout, callback
	options = options || {};
	list = jQuery(list);
	var input = this;
	var lastFilter = '';
	var timeout = options.timeout || 0;
	var callback = options.callback || function() {};
	
	var keyTimeout;
	function hideEmpty() {
	$(".vids").each(function(){
 	if($(this).children('tag:visible').length == 0) {
   	$(this.parentNode.parentNode).hide(); 
	}
	else {
	$(this.parentNode.parentNode).show();
	}
	
});
}
	// NOTE: because we cache lis & len here, users would need to re-init the plugin
	// if they modify the list in the DOM later.  This doesn't give us that much speed
	// boost, so perhaps it's not worth putting it here.
	var lis = list.children('tag');
	var len = lis.length;
	var oldDisplay = len > 0 ? lis[0].style.display : "block";
	callback(len); // do a one-time callback on initialization to make sure everything's in sync
	
	input.change(function() {
		// var startTime = new Date().getTime();
		var filter = input.val().toLowerCase().replace(/ /g,'');
		var li, innerText;
		var numShown = 0;
		for (var i = 0; i < len; i++) {
			li = lis[i];
			var className = $(li).attr("class").replace(/-/g,'');


			innerText = !options.selector ? 

				(li.textContent || li.innerText || "" || className) : 
				$(li).find(options.selector).text();
				var stash = innerText;
				innerText = stash.replace(/\./g,'');
				

			if ((className.toLowerCase().replace(/ /g,'').indexOf(filter)   >= 0) || (innerText.toLowerCase().replace(/ /g,'').indexOf(filter)   >= 0))  {

				if (li.style.display == "none") {
					li.style.display = oldDisplay;
					$('.arrows').show();
   					$('.vids > tag').show();
				}
				numShown++;
			} else {
				if (li.style.display != "none") {
					li.style.display = "none";

				}

			}
			hideEmpty();
		}
		callback(numShown);
		// var endTime = new Date().getTime();
		// console.log('Search for ' + filter + ' took: ' + (endTime - startTime) + ' (' + numShown + ' results)');
		return false;
	}).keydown(function() {
		clearTimeout(keyTimeout);
		keyTimeout = setTimeout(function() {
			if( input.val() === lastFilter ) return;
			lastFilter = input.val();
			input.change();
		}, timeout);
	});
	return this; // maintain jQuery chainability
}
