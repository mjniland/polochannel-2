var simplemaps_worldmap_mapdata={

main_settings:{ 
		width: 'responsive',
		background_color: '#FFFFFF',
		background_transparent: 'yes',
		label_color: '#d5ddec',
		border_color: 'rgba(230, 230, 230, 1)',
		pop_ups: 'detect',
		state_description: 'Country description',
		state_color: 'rgba(230, 230, 230, 1)',
		state_hover_color: '#3B729F',
		state_url: '',
		all_states_inactive: 'no',
		all_states_zoomable: 'yes',
		location_description: '',
		location_color: '#FF0067',
		location_opacity: '.8',
		location_url: '',
		location_size: '50',
		all_locations_inactive: 'no',
		div: 'map',
		arrow_color: '#3B729F',
		arrow_color_border: '#88A4BC',
		border_size: '1',
		popup_color: 'white',
		popup_opacity: '0.9',
		popup_shadow: '1',
		popup_corners: '5',
		popup_font: '12px/1.5 Verdana, Arial, Helvetica, sans-serif',
		popup_nocss: 'no',
		initial_zoom: '-1',
		zoom_percentage: '.9',
		initial_zoom_solo: 'no',
		all_states_zoomable: 'yes',
		auto_load: 'yes',
		zoom: 'yes',
		js_hooks: 'yes',
		url_new_tab: 'no'
},

state_specific:{ 
	AE: { 
		name: 'United Arab Emirates',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	AF: { 
		name: 'Afghanistan',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	AL: { 
		name: 'Albania',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	AM: { 
		name: 'Armenia',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	AO: { 
		name: 'Angola',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	AR: { 
		name: 'Argentina',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	AT: { 
		name: 'Austria',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	AU: { 
		name: 'Australia',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	AZ: { 
		name: 'Azerbaijan',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	BA: { 
		name: 'Bosnia-Herzegovina',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	BD: { 
		name: 'Bangladesh',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	BE: { 
		name: 'Belgium',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	BF: { 
		name: 'Burkina Faso',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	BG: { 
		name: 'Bulgaria',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	BH: { 
		name: 'Bahrain',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	BI: { 
		name: 'Burundi',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	BJ: { 
		name: 'Benin',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	BN: { 
		name: 'Brunei Darussalam',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	BO: { 
		name: 'Bolivia',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	BR: { 
		name: 'Brazil',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	BS: { 
		name: 'Bahamas',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	BT: { 
		name: 'Bhutan',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	BW: { 
		name: 'Botswana',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	BY: { 
		name: 'Belarus',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	BZ: { 
		name: 'Belize',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	CA: { 
		name: 'Canada',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	CD: { 
		name: 'Democratic Republic of Congo',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	CF: { 
		name: 'Central African Republic',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	CG: { 
		name: 'Congo',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	CH: { 
		name: 'Switzerland',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	CI: { 
		name: 'Ivory Coast',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	CL: { 
		name: 'Chile',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	CM: { 
		name: 'Cameroon',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	CN: { 
		name: 'China',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	CO: { 
		name: 'Colombia',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	CR: { 
		name: 'Costa Rica',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	CU: { 
		name: 'Cuba',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	CV: { 
		name: 'Cape Verde',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	CY: { 
		name: 'Cyprus',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	CZ: { 
		name: 'Czech Republic',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	DE: { 
		name: 'Germany',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	DJ: { 
		name: 'Djibouti',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	DK: { 
		name: 'Denmark',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	DO: { 
		name: 'Dominican Republic',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	DZ: { 
		name: 'Algeria',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	EC: { 
		name: 'Ecuador',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	EE: { 
		name: 'Estonia',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	EG: { 
		name: 'Egypt',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	EH: { 
		name: 'Western Sahara',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	ER: { 
		name: 'Eritrea',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	ES: { 
		name: 'Spain',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	ET: { 
		name: 'Ethiopia',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	FI: { 
		name: 'Finland',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	FJ: { 
		name: 'Fiji',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	FK: { 
		name: 'Falkland Islands',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	FR: { 
		name: 'France',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	GA: { 
		name: 'Gabon',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	GB: { 
		name: 'Great Britain',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	GE: { 
		name: 'Georgia',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	GF: { 
		name: 'French Guyana',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	GH: { 
		name: 'Ghana',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	GL: { 
		name: 'Greenland',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	GM: { 
		name: 'Gambia',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	GN: { 
		name: 'Guinea',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	GQ: { 
		name: 'Equatorial Guinea',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	GR: { 
		name: 'Greece',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	GS: { 
		name: 'S. Georgia & S. Sandwich Isls.',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	GT: { 
		name: 'Guatemala',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	GW: { 
		name: 'Guinea Bissau',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	GY: { 
		name: 'Guyana',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	HN: { 
		name: 'Honduras',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	HR: { 
		name: 'Croatia',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	HT: { 
		name: 'Haiti',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	HU: { 
		name: 'Hungary',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	IC: { 
		name: 'Canary Islands',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	ID: { 
		name: 'Indonesia',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	IE: { 
		name: 'Ireland',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	IL: { 
		name: 'Israel',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	IN: { 
		name: 'India',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	IQ: { 
		name: 'Iraq',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	IR: { 
		name: 'Iran',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	IS: { 
		name: 'Iceland',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	IT: { 
		name: 'Italy',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	JM: { 
		name: 'Jamaica',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	JO: { 
		name: 'Jordan',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	JP: { 
		name: 'Japan',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	KE: { 
		name: 'Kenya',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	KG: { 
		name: 'Kyrgyzstan',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	KH: { 
		name: 'Cambodia',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	KP: { 
		name: 'North Korea',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	KR: { 
		name: 'South Korea',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	KW: { 
		name: 'Kuwait',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	KZ: { 
		name: 'Kazakhstan',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	LA: { 
		name: 'Laos',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	LB: { 
		name: 'Lebanon',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	LK: { 
		name: 'Sri Lanka',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	LR: { 
		name: 'Liberia',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	LS: { 
		name: 'Lesotho',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	LT: { 
		name: 'Lithuania',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	LU: { 
		name: 'Luxembourg',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	LV: { 
		name: 'Latvia',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	LY: { 
		name: 'Libya',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	MA: { 
		name: 'Morocco',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	MD: { 
		name: 'Moldova',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	ME: { 
		name: 'Montenegro',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	MG: { 
		name: 'Madagascar',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	MK: { 
		name: 'Macedonia',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	ML: { 
		name: 'Mali',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	MM: { 
		name: 'Myanmar',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	MN: { 
		name: 'Mongolia',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	MR: { 
		name: 'Mauritania',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	MW: { 
		name: 'Malawi',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	MX: { 
		name: 'Mexico',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	MY: { 
		name: 'Malaysia',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	MZ: { 
		name: 'Mozambique',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	NA: { 
		name: 'Namibia',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	NC: { 
		name: 'New Caledonia (French)',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	NE: { 
		name: 'Niger',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	NG: { 
		name: 'Nigeria',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	NI: { 
		name: 'Nicaragua',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	NL: { 
		name: 'Netherlands',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	NO: { 
		name: 'Norway',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	NP: { 
		name: 'Nepal',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	NZ: { 
		name: 'New Zealand',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	OM: { 
		name: 'Oman',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	PA: { 
		name: 'Panama',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	PE: { 
		name: 'Peru',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	PG: { 
		name: 'Papua New Guinea',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	PH: { 
		name: 'Philippines',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	PK: { 
		name: 'Pakistan',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	PL: { 
		name: 'Poland',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	PR: { 
		name: 'Puerto Rico',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	PS: { 
		name: 'Palestine',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	PT: { 
		name: 'Portugal',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	PY: { 
		name: 'Paraguay',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	QA: { 
		name: 'Qatar',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	RO: { 
		name: 'Romania',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	RS: { 
		name: 'Serbia',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	RU: { 
		name: 'Russia',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	RW: { 
		name: 'Rwanda',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	SA: { 
		name: 'Saudi Arabia',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	SB: { 
		name: 'Solomon Islands',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	SD: { 
		name: 'Sudan',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	SE: { 
		name: 'Sweden',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	SI: { 
		name: 'Slovenia',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	SK: { 
		name: 'Slovak Republic',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	SL: { 
		name: 'Sierra Leone',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	SN: { 
		name: 'Senegal',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	SO: { 
		name: 'Somalia',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	SR: { 
		name: 'Suriname',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	SS: { 
		name: 'South Sudan',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	SV: { 
		name: 'El Salvador',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	SY: { 
		name: 'Syria',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	SZ: { 
		name: 'Swaziland',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	TD: { 
		name: 'Chad',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	TG: { 
		name: 'Togo',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	TH: { 
		name: 'Thailand',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	TJ: { 
		name: 'Tajikistan',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	TL: { 
		name: 'East Timor',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	TM: { 
		name: 'Turkmenistan',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	TN: { 
		name: 'Tunisia',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	TR: { 
		name: 'Turkey',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	TT: { 
		name: 'Trinidad and Tobago',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	TW: { 
		name: 'Taiwan',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	TZ: { 
		name: 'Tanzania',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	UA: { 
		name: 'Ukraine',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	UG: { 
		name: 'Uganda',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	US: { 
		name: 'United States',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes',

	},

	UY: { 
		name: 'Uruguay',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	UZ: { 
		name: 'Uzbekistan',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	VE: { 
		name: 'Venezuela',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	VN: { 
		name: 'Vietnam',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	VU: { 
		name: 'Vanuatu',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	YE: { 
		name: 'Yemen',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	ZA: { 
		name: 'South Africa',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	ZM: { 
		name: 'Zambia',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	},

	ZW: { 
		name: 'Zimbabwe',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'yes'
	}

},


regions: {
	northamerica: {
		name: "North America",
		states: ["MX","CA","US","GL"], 
	},
	southamerica: {
		name: "South America",
		states: ["EC","AR","VE","BR","CO","BO","PE","BZ","CL","CR","CU","DO","SV","GT","GY","GF","HN","NI","PA","PY","PR","SR","UY","JM","HT","BS"]
	},
	europe: {
		name: "Europe",
		states:["IT","NL","NO","DK","IE","GB","RO","DE","FR","AL","AM","AT","BY","BE","LU","BG","CZ","EE","GE","GR","HU","IS","LV","LT","MD","PL","PT","RS","SI","HR","BA","ME","MK","SK","ES","FI","SE","CH","TR","CY","UA","XK"]
	},
	africa: {
		name: "Africa",
		states: ["NE","AO","EG","TN","GA","DZ","LY","CG","GQ","BJ","BW","BF","BI","CM","CF","TD","CI","CD","DJ","ET","GM","GH","GN","GW","KE","LS","LR","MG","MW","ML","MA","MR","MZ","NA","NG","ER","RW","SN","SL","SO","ZA","SD","SS","SZ","TZ","TG","UG","EH","ZM","ZW"]
	},
	middleeast: {
		name: "Middle East",
		states: ["QA","SA","AE","SY","OM","KW","PK","AZ","AF","IR","IQ","IL","PS","JO","LB","YE","TJ","TM","UZ","KG"]
	},
	asia: {
		name: "Asia",
		states:["TW","IN","MY","NP","NZ","TH","BN","JP","VN","LK","SB","FJ","BD","BT","KH","LA","MM","KP","PG","PH","KR","ID","CN","MN", "RU", "KZ"]
	},
	australia:	{
		name: "Australia",
		states: ["AU"]
	}
},


locationss:{ 
     northamerica_north_ipc_news: { 
		region_id: 'north',
		name: 'IPC',
		description: '<img width="50" height="50" style="float:left; margin-right: 10px;" src="map_images/ipc.png"/>TESTING is some text about ipc club<br>the text and image can be changed to fit the location',
		lat: '40.71',
		lng: '-74.0059731',
		type: 'image',
		image_source: 'location(orange).png',
		group: 0
	},

	northamerica_south_aiken_featured: {
		region_id: 'south',
		name: 'AIKEN',
		description: '<img width="50" height="50" style="float:left; margin-right: 10px;" src="map_images/ipc.png"/>This is some text about ipc club<br>the text and image can be changed to fit the location',
		lat: '61.2180556',
		lng: '-149.9002778',

		type: 'image',
		image_source: 'location(yellow).png',
		size: '45',
		group: 3
	},

	northamerica_east_test_profiles: { 
		region_id: 'east',
		name: 'TEST',
		description: '<img width="50" height="50" style="float:left; margin-right: 10px;" src="map_images/ipc.png"/>This is some text about ipc club<br>the text and image can be changed to fit the location',
		lat: '34.000000000',
		lng: '-118.250000000',

		url: 'default',
		size: '45',
		type: 'image',
		image_source: 'location(blue).png',
		group: 1		
	},

	europe_west_hamptons_news: { 
		region_id: 'west',
		name: 'HAMPTONS',
		description: '<img width="50" height="50" style="float:left; margin-right: 10px;" src="map_images/ipc.png"/>This is some text about ipc club<br>the text and image can be changed to fit the location',
		lat: '48.866666670',
		lng: '2.333333333',

		url: 'default',
		size: '45',
		type: 'image',
		image_source: 'location(orange).png',
		group: 0		
	},

	europe_west_aiken_games: { 
		region_id: 'west',
		name: 'AIKEN',
		description: '<img width="50" height="50" style="float:left; margin-right: 10px;" src="map_images/ipc.png"/>This is some text about ipc club<br>the text and image can be changed to fit the location',
		lat: '51.500000000',
		lng: '-0.166666667',
		url: 'default',
		size: '45',
		type: 'image',
		image_source: 'location(blue).png',
		group: 1		
	},

	southamerica_east_aiken_news: { 
		region_id: 'east',
		name: 'AIKEN',
		description: '<img width="50" height="50" style="float:left; margin-right: 10px;" src="map_images/ipc.png"/>This is some text about ipc club<br>the text and image can be changed to fit the location',
		lat: '-0.233333333',
		lng: '-78.500000000',
		url: 'default',
		size: '45',
		type: 'image',
		image_source: 'location(orange).png',
		group: 0		
	},

	southamerica_north_aiken_games: { 
		region_id: 'north',
		name: 'AIKEN',
		description: '<img width="50" height="50" style="float:left; margin-right: 10px;" src="map_images/ipc.png"/>This is some text about ipc club<br>the text and image can be changed to fit the location',
		lat: '-34.883333330',
		lng: '-56.183333330',

		url: 'default',
		size: '45',
		type: 'image',
		image_source: 'location(blue).png',
		group: 1		
	},

	southamerica_west_hamptons_news: { 
		region_id: 'west',
		name: 'HAMPTONS',
		description: '<img width="50" height="50" style="float:left; margin-right: 10px;" src="map_images/ipc.png"/>This is some text about ipc club<br>the text and image can be changed to fit the location',
		lat: '-22.900000000',
		lng: '-43.233333330',

		url: 'default',
		size: '45',
		type: 'image',
		image_source: 'location(orange).png',
		group: 0
	},


	africa_north_ipc_news: { 
		region_id: 'north',
		name: 'IPC',
		description: '<img width="50" height="50" style="float:left; margin-right: 10px;" src="map_images/ipc.png"/>This is some text about ipc club<br>the text and image can be changed to fit the location',
		lat: '30.050000000',
		lng: '31.250000000',

		url: 'default',
		size: '45',
		type: 'image',
		image_source: 'location(orange).png',
		group: 0
	},

	asia_west_hamptons_games: { 
		region_id: 'west',
		name: 'HAMPTONS',
		description: '<img width="50" height="50" style="float:left; margin-right: 10px;" src="map_images/ipc.png"/>This is some text about ipc club<br>the text and image can be changed to fit the location',
		lat: '1.300000000',
		lng: '103.833333300',

		url: 'default',
		size: '45',
		type: 'image',
		image_source: 'location(blue).png',
		group: 1		
	},

	australia_south_hamptons_profiles: { 
		region_id: 'south',
		name: 'HAMPTONS',
		description: '<img width="50" height="50" style="float:left; margin-right: 10px;" src="map_images/ipc.png"/>This is some text about ipc club<br>the text and image can be changed to fit the location',
		lat: '-31.933333330',
		lng: '115.833333300',

		url: 'default',
		type: 'image',
		image_source: 'location(green).png',
		group: 2		
	},

	europe_east_aiken_profiles: { 
		region_id: 'east',
		name: 'AIKEN',
		description: '<img width="50" height="50" style="float:left; margin-right: 10px;" src="map_images/ipc.png"/>This is some text about ipc club<br>the text and image can be changed to fit the location',
		lat: '50.416666670',
		lng: '30.716666670',

		url: 'default',
		type: 'image',
		image_source: 'location(green).png',
		group: 2		
	},

	asia_east_aiken_profiles: { 
		region_id: 'east',
		name: 'AIKEN',
		description: '<img width="50" height="50" style="float:left; margin-right: 10px;" src="map_images/ipc.png"/>This is some text about ipc club<br>the text and image can be changed to fit the location',
		lat: '30.583333330',
		lng: '108.900000000',

		url: 'default',
		size: '45',
		type: 'image',
		image_source: 'location(green).png',
		group: 2		
	},

	asia_east_ipc_featured: { 
		region_id: 'east',
		name: 'IPC',
		description: '<img width="50" height="50" style="float:left; margin-right: 10px;" src="map_images/ipc.png"/>This is some text about ipc club<br>the text and image can be changed to fit the location',
		lat: '18.933333330',
		lng: '74.583333330',

		url: 'default',
		size: '45',
		type: 'image',
		image_source: 'location(yellow).png',
		group: 3		
	}


}


}