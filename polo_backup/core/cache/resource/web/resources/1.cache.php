<?php  return array (
  'resourceClass' => 'modDocument',
  'resource' => 
  array (
    'id' => 1,
    'type' => 'document',
    'contentType' => 'text/html',
    'pagetitle' => 'Home',
    'longtitle' => 'Polochannel',
    'description' => 'Polo Channel hosts videos of polo clubs, tournaments, and events from around the globe',
    'alias' => 'index',
    'link_attributes' => '',
    'published' => 1,
    'pub_date' => 0,
    'unpub_date' => 0,
    'parent' => 0,
    'isfolder' => 1,
    'introtext' => '',
    'content' => '<pre class="_5s-8 prettyprint lang-code prettyprinted"><span class="tag"><br /><br /><br /></span></pre>',
    'richtext' => 1,
    'template' => 5,
    'menuindex' => 0,
    'searchable' => 1,
    'cacheable' => 1,
    'createdby' => 1,
    'createdon' => 1380601770,
    'editedby' => 8,
    'editedon' => 1478730278,
    'deleted' => 0,
    'deletedon' => 0,
    'deletedby' => 0,
    'publishedon' => 1436916120,
    'publishedby' => 8,
    'menutitle' => '',
    'donthit' => 0,
    'privateweb' => 0,
    'privatemgr' => 0,
    'content_dispo' => 0,
    'hidemenu' => 0,
    'class_key' => 'modDocument',
    'context_key' => 'web',
    'content_type' => 1,
    'uri' => 'index/',
    'uri_override' => 0,
    'hide_children_in_tree' => 0,
    'show_in_tree' => 1,
    'properties' => NULL,
    'mp4-video' => 
    array (
      0 => 'mp4-video',
      1 => '',
      2 => 'default',
      3 => NULL,
      4 => 'image',
    ),
    'ogv-video' => 
    array (
      0 => 'ogv-video',
      1 => 'video/2015 BG Web BG_1.oggtheora.ogv',
      2 => 'default',
      3 => NULL,
      4 => 'image',
    ),
    'link-1-text' => 
    array (
      0 => 'link-1-text',
      1 => '',
      2 => 'default',
      3 => NULL,
      4 => 'text',
    ),
    'link-1' => 
    array (
      0 => 'link-1',
      1 => '',
      2 => 'default',
      3 => NULL,
      4 => 'text',
    ),
    'link-2-text' => 
    array (
      0 => 'link-2-text',
      1 => '',
      2 => 'default',
      3 => NULL,
      4 => 'text',
    ),
    'link-2' => 
    array (
      0 => 'link-2',
      1 => '',
      2 => 'default',
      3 => NULL,
      4 => 'text',
    ),
    'live-stream' => 
    array (
      0 => 'live-stream',
      1 => '',
      2 => 'default',
      3 => NULL,
      4 => 'text',
    ),
    'next-match-name' => 
    array (
      0 => 'next-match-name',
      1 => '',
      2 => 'default',
      3 => NULL,
      4 => 'text',
    ),
    'timezone' => 
    array (
      0 => 'timezone',
      1 => ' America/Buenos_Aires',
      2 => 'default',
      3 => NULL,
      4 => 'listbox',
    ),
    'next-match-date' => 
    array (
      0 => 'next-match-date',
      1 => '2016-11-09 16:30:00',
      2 => 'date',
      3 => NULL,
      4 => 'date',
    ),
    '_content' => '<!doctype html>
<head>
<meta charset="UTF-8">
<title>Polo Channel</title>
<link href="http://local.polo.com//css/main.css" rel="stylesheet" type="text/css" />
<link rel="icon" type="image/png" href="http://polochannel.com/favicon.png">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script type="text/javascript" src="http://local.polo.com//js/lightbox.min.js"></script>
<script type="text/javascript" src="http://local.polo.com//js/touchSwipe.min.js"></script>
<meta name="description" content="Polo Channel hosts videos of polo clubs, tournaments, and events from around the globe">
        <meta name="viewport" content="width=device-width,initial-scale=1.0">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

        <!-- THIS IS NEEDED FOR FURL -->
        <base href="http://local.polo.com/" />
        
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width,initial-scale=1.0">

        <!-- Facebook OpenGraph Data -->
        <meta property="og:title" content="Polo Channel" />
        <meta property="og:type" content="website" />
        <!-- If it\'s an article:
        <meta property="og:type" content="article" />
        <meta property="article:published_time" content = "" />
        -->
        <meta property="og:description" content="Polo Channel hosts videos of polo clubs, tournaments, and events from around the globe" />        
        <meta property="og:url" content="http://local.polo.com/"/>
        <meta property="og:image" content="http://local.polo.com/images/ogimage.jpg" />
        <meta property="og:site_name" content="Polo Channel" />


<script src="js/respond.min.js"></script>

<script src="http://local.polo.com//js/mapdata.js"></script>
<script src="http://local.polo.com//js/worldmap.js"></script>
<script src="http://local.polo.com//js/jquery.fastLiveFilter.js"></script>
<script src="http://local.polo.com//js/jquery.countdown.min.js"></script>
<script src="http://local.polo.com//js/modernizr.custom.js"></script>
<link rel="stylesheet" type="text/css" href="css/lightbox.min.css">
<meta name="google-site-verification" content="lyzPX5GCX9Yi9NxiLhZGlEnTRGSsqA6xKj1sbBaz_Yo" />
<link href="http://local.polo.com//css/normalize.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<script>
  (function(i,s,o,g,r,a,m){i[\'GoogleAnalyticsObject\']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,\'script\',\'https://www.google-analytics.com/analytics.js\',\'ga\');

  ga(\'create\', \'UA-82621934-1\', \'auto\');
  ga(\'send\', \'pageview\');

</script>
 <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
 <![endif]-->
<script>
    $(function() {
        $(\'#search_input\').fastLiveFilter(\'.search_list\');
        $(\'#search_input_mobile\').fastLiveFilter(\'.search_list\');
    });

       $(window).load(function() {
      $(\'#loading\').fadeOut("slow");
      hideEmpty();
    });

    function hideEmpty() {
$(".vids").each(function(){

 if($(this).children(\'tag:visible\').length == 0) {
   $(this.parentNode.parentNode).hide(); 
}
else {
 $(this.parentNode.parentNode).show();
}
});

}
</script>



<!--[if lt IE 9]>
<script>
document.createElement(\'video\');
</script>
<![endif]-->

</head>


<div class="modal-body" id="subscribe">

     [[!FormIt?
   &hooks=`email, FormItSaveForm, redirect`
   &emailTpl=`email`
   &emailFrom=`no-reply@polochannel.com`
   &emailTo=`michael@ten25designs.com`
   &validate=`name:required,
      email:required`
   &redirectTo=`1`
]]

<form class="contact" action="http://local.polo.com/" method="post">
<i style="position: absolute; right:0; top: 0; color: gray; text-align: right; float: right; margin-right: 10px; margin-top: 10px; font-size: 20px; z-index: 5000;" class="fa fa-close"></i>
    <fieldset>
         <h3>Subscribe to our news</h3>
        <input type="text" name="name" value="[[!+fi.first_name]]" placeholder="First Name">
        <input type="text" name="email" value="[[!+fi.email]]" placeholder="email">
       <input class="send" type="submit" value="Send"> <!-- Send button-->
   </fieldset>
</form>

</div>



<body>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5";
  fjs.parentNode.insertBefore(js, fjs);
}(document, \'script\', \'facebook-jssdk\'));</script>
 <div id="loading">
     <img class="loading-image main-logo"src="images/mainlogo_black.png" alt="">
       <img class="loading-image" src="images/loading.gif" alt="Loading..." />
  </div>
</div>
  

<div class="fixed-header">
  <div class="header">

<div class="header3">
	  <ul class="nav">
		<li><a href="/">Home</a></li>
  
		<li><a class="videos" href="/">Videos</a></li>
  
		<li><a href="/polo-blog">News</a></li>
  

		<li><div><a href="/" class="subscribe">Subscribe</a></div></li>
	  </ul>
	</div>

      <ul class="headerlinks">
	      

      
          <li><a href="https://www.facebook.com/polochannelnetwork" target="_blank"><div class="fa fa-facebook-official  fa-lg"></div></a></li>
      <li><a href="https://instagram.com/thepolochannel" target="_blank"><div class="fa fa-instagram  fa-lg"></div></a></li>
      <li><a href="mailto:contact@horseplay.tv"><div class="fa fa-envelope  fa-lg"></div></a></li>
  </ul>
    <a href="/"><img src="images/mainlogo.png" alt="" height="70" class="headerimage"></a>
  </div>

  
</div>
<div class="spacer"></div>
<div class="not-so-fixie">
<div class="polo-feed">
      <div class="three-col small-col">
			[[!getResources? &parents=`187` &limit=`999` &tpl=`blog_post` &includeTVs=`1` &processTVs=`1`]]
      </div>
      <div class="three-col large-col">

      </div>
	  <div class="append-back">
      <div class="three-col small-col" id="score-list">
			[[!getResources? &parents=`190` &limit=`999` &tpl=`score_post` &includeTVs=`1` &processTVs=`1`]]
      </div>
	  </div>
</div>


<div class="header2">
    <ul class="nav">
      <li class="group_toggle" id="group1">games&nbsp;<img src="map_images/blue.png" height="20"></li>
      <li><div class="v-bar"></div></li>
      <li class="group_toggle" id="group3">featured&nbsp;<img src="map_images/yellow.png" height="20"></li>
      <li><div class="v-bar"></div></li>
      <li class="group_toggle" id="group2">profiles&nbsp;<img src="map_images/green.png" height="20"></li>
      <li><div class="v-bar"></div></li>
      <li class="group_toggle" id="group0">events&nbsp;<img src="map_images/orange.png" height="20"></li>
      <li><div class="v-bar" height="20"></div></li>
      <li><div class="showall">show all</div></li>
      <li><input type="text" id="search_input" placeholder="search..."></li>


	  <li><div class="map-btn">Map</div></li>
       
    </ul>

  </div>

<div class="vid-scroll"></div>
  <div class="row">
    <div class="left col">
      <div class="checkboxes">
          <ul class="continents sub-nav">
            [[!getResources? &depth=`0` &parents=`1` &resources=`-4,-5` &sortby=`{"menuindex":"ASC"}` &limit=`` &tpl=`continents` &includeTVs=`1` &processTVs=`1` &tvPrefix=``]]
          </ul>

          <ul class="regions sub-nav">
            <li>north<input id="north" class="checkbox" type="checkbox"/></li>
            <li>south<input id="south" class="checkbox" type="checkbox"/></li>
            <li>east<input id="east" class="checkbox" type="checkbox"></li>
            <li>west<input id="west" class="checkbox" type="checkbox"></li>
            <li>central<input id="central" class="checkbox" type="checkbox"></li>
          </ul>

           <ul class="clubs sub-nav">
            <li>International-Polo-Club<input id="northamerica,south" class="checkbox international-polo-club" type="checkbox"/></li>

<li>Guards-Polo-Club<input id="europe,west" class="checkbox guards-polo-club" type="checkbox"/></li>

<li>Cowdray-Park-Polo-Club<input id="europe,south" class="checkbox cowdray-park-polo-club" type="checkbox"/></li>

<li>Ghantoot-Racing-and-Polo-Club<input id="africa,east" class="checkbox ghantoot-racing-and-polo-club" type="checkbox"/></li>

<li>Desert-Palm-Polo-Club<input id="northamerica,north" class="checkbox desert-palm-polo-club" type="checkbox"/></li>

<li>Ellerstina-Polo-Club<input id="southamerica,east" class="checkbox ellerstina-polo-club" type="checkbox"/></li>

<li>Maragata-Polo-Club<input id="southamerica,north" class="checkbox maragata-polo-club" type="checkbox"/></li>

<li>Santa-Maria-Polo-Club<input id="europe,south" class="checkbox santa-maria-polo-club" type="checkbox"/></li>

<li>Santa-Barbara-Polo-Club<input id="northamerica,west" class="checkbox santa-barbara-polo-club" type="checkbox"/></li>

<li>La-Dolfina<input id="southamerica,south" class="checkbox la-dolfina" type="checkbox"/></li>

<li>Ellerston-Polo-Club<input id="australia,south" class="checkbox ellerston-polo-club" type="checkbox"/></li>

<li>Hawaii-Polo-Club<input id="northamerica,west" class="checkbox hawaii-polo-club" type="checkbox"/></li>

<li>Copenhagen-Polo-Club<input id="europe,central" class="checkbox copenhagen-polo-club" type="checkbox"/></li>

<li>Tang-Polo-Club<input id="asia,east" class="checkbox tang-polo-club" type="checkbox"/></li>

<li>Jnan-Amar-Polo-Club<input id="africa,west" class="checkbox jnan-amar-polo-club" type="checkbox"/></li>

<li>Eldorado-Polo-Club<input id="northamerica,west" class="checkbox eldorado-polo-club" type="checkbox"/></li>

<li>Tanoira-Polo-Club<input id="southamerica,east" class="checkbox tanoira-polo-club" type="checkbox"/></li>

<li>Myopia-Polo-Club<input id="northamerica,east" class="checkbox myopia-polo-club" type="checkbox"/></li>

<li>Hilario Ulloa<input id="southamerica,west" class="checkbox hilario ulloa" type="checkbox"/></li>
                        
          </ul>
      </div>
      <div class="description"></div>
    </div>

    <div class="right col">
      <div class="limiter">
        <div class="map" id="map"></div>
      </div>
      <video loop autoplay>
        <source src="" type="video/mp4">
        <source src="[[*obg-video]]" type="video/ogg">
      </video>
    </div>
  </div>
</div>


<!-- _____________________________________________________________________________________ mobile menu !-->


<div class="mobile-nav">
  
  <div class="header-mobile">
    <ul class="mobile-right-box">

      <li><a href="https://www.facebook.com/polochannelnetwork" target="_blank"><div class="fa fa-facebook-official"></div></a></li>
      <li><a href="https://instagram.com/thepolochannel" target="_blank"><div class="fa fa-instagram"></div></a></li>
      <li><a href="mailto:contact@horseplay.tv"><div class="fa fa-envelope"></div></a></li>
       <li> 
          <ul class="menu-mobile">
              <li></li>
              <li></li>
              <li></li>
          </ul>
       </li>
    </ul>
      <a href="/"><img src="images/mainlogo.png" alt="" height="40" ></a>
  </div>
  

    <div class="nav-mobile">
      <div class="checkboxes mobile">
        <div class="x-mobile"><img src="images/x.png" width="20" alt=""></div>
          <div class="background-mobile"></div>

        <ul class="search_showall"> 
		    <li><div class="header3">
	  <ul class="nav">
		<li><a href="/">Home</a></li>
  
		<li><a class="videos" href="/">Videos</a></li>
  
		<li><a href="/polo-blog">News</a></li>
  

		<li><div><a href="/" class="subscribe">Subscribe</a></div></li>
	  </ul>
	</div></li>
            <li><input type="text" id="search_input_mobile" placeholder="search..."></li>
            <li><div class="showall">show all</div></li>
          </ul>


          <ul class="cat sub-nav">
            <li>news<input id="news" class="checkbox" type="checkbox"/></li>
            <li>games<input id="games" class="checkbox" type="checkbox"/></li>
            <li>profiles<input id="profiles" class="checkbox" type="checkbox"></li>
            <li>featured<input id="featured" class="checkbox" type="checkbox"></li>
          </ul>

          <ul class="continents sub-nav">
            [[!getResources? &depth=`0` &parents=`1` &resources=`-4,-5` &sortby=`{"menuindex":"ASC"}` &limit=`` &tpl=`continents` &includeTVs=`1` &processTVs=`1` &tvPrefix=``]]
          </ul>

          <ul class="regions sub-nav">
            <li>north<input id="north" class="checkbox" type="checkbox"/></li>
            <li>south<input id="south" class="checkbox" type="checkbox"/></li>
            <li>east<input id="east" class="checkbox" type="checkbox"></li>
            <li>west<input id="west" class="checkbox" type="checkbox"></li>
            <li>central<input id="central" class="checkbox" type="checkbox"></li>
          </ul>

           <ul class="clubs sub-nav">
            <li>International-Polo-Club<input id="northamerica,south" class="checkbox international-polo-club" type="checkbox"/></li>

<li>Guards-Polo-Club<input id="europe,west" class="checkbox guards-polo-club" type="checkbox"/></li>

<li>Cowdray-Park-Polo-Club<input id="europe,south" class="checkbox cowdray-park-polo-club" type="checkbox"/></li>

<li>Ghantoot-Racing-and-Polo-Club<input id="africa,east" class="checkbox ghantoot-racing-and-polo-club" type="checkbox"/></li>

<li>Desert-Palm-Polo-Club<input id="northamerica,north" class="checkbox desert-palm-polo-club" type="checkbox"/></li>

<li>Ellerstina-Polo-Club<input id="southamerica,east" class="checkbox ellerstina-polo-club" type="checkbox"/></li>

<li>Maragata-Polo-Club<input id="southamerica,north" class="checkbox maragata-polo-club" type="checkbox"/></li>

<li>Santa-Maria-Polo-Club<input id="europe,south" class="checkbox santa-maria-polo-club" type="checkbox"/></li>

<li>Santa-Barbara-Polo-Club<input id="northamerica,west" class="checkbox santa-barbara-polo-club" type="checkbox"/></li>

<li>La-Dolfina<input id="southamerica,south" class="checkbox la-dolfina" type="checkbox"/></li>

<li>Ellerston-Polo-Club<input id="australia,south" class="checkbox ellerston-polo-club" type="checkbox"/></li>

<li>Hawaii-Polo-Club<input id="northamerica,west" class="checkbox hawaii-polo-club" type="checkbox"/></li>

<li>Copenhagen-Polo-Club<input id="europe,central" class="checkbox copenhagen-polo-club" type="checkbox"/></li>

<li>Tang-Polo-Club<input id="asia,east" class="checkbox tang-polo-club" type="checkbox"/></li>

<li>Jnan-Amar-Polo-Club<input id="africa,west" class="checkbox jnan-amar-polo-club" type="checkbox"/></li>

<li>Eldorado-Polo-Club<input id="northamerica,west" class="checkbox eldorado-polo-club" type="checkbox"/></li>

<li>Tanoira-Polo-Club<input id="southamerica,east" class="checkbox tanoira-polo-club" type="checkbox"/></li>

<li>Myopia-Polo-Club<input id="northamerica,east" class="checkbox myopia-polo-club" type="checkbox"/></li>

<li>Hilario Ulloa<input id="southamerica,west" class="checkbox hilario ulloa" type="checkbox"/></li>
                        
          </ul>
      
      <ul class="headerlinks">
	      

      
          <li><a href="https://www.facebook.com/polochannelnetwork" target="_blank"><div class="fa fa-facebook-official  fa-lg"></div></a></li>
      <li><a href="https://instagram.com/thepolochannel" target="_blank"><div class="fa fa-instagram  fa-lg"></div></a></li>
      <li><a href="mailto:contact@horseplay.tv"><div class="fa fa-envelope  fa-lg"></div></a></li>
  </ul>
    </div>

    </div>




      <div class="polo-feed-mobile">
		<h3>News</h3>
		<div class="scroller">
         [[!getResources? &parents=`187` &limit=`10` &tpl=`blog-list` &includeTVs=`1` &processTVs=`1`]]
		</div>
		<div style="text-align: center; font-size: 10px; color: gray;"><i class="fa fa-arrow-left"></i> swipe to scroll <i class="fa fa-arrow-right"></i></div>
</div>
    

</div>

<div id="scores"></div>



<ul class="video-list"> 
  <!-- Most Recent -->
    <div class="arrows">
      <div class="left-arrow most-recent"><i class="fa fa-arrow-left" aria-hidden="true"></i></div>
      <div class="right-arrow most-recent"><i class="fa fa-arrow-right" aria-hidden="true"></i></div>
		 <h3>Most Recent</h3>
    <div class="cut-off" id="most-recent">

      <div class="vids search_list">

          <tag class=" west hilario ulloa profiles">
 <a href="#191725822" class="link-lightbox" data-videoid="191725822" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-191725822" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">Hilario Ulloa</h4><h4 class="vs">10 Goals</h4></a>
</tag>
<tag class=" south international-polo-club featured">
 <a href="#K9Kvs0lUJBA" class="link-lightbox" data-videoid="K9Kvs0lUJBA" data-videosite="youtube">
 	<div class="img-contain">
	<div style="background: url(http://img.youtube.com/vi/K9Kvs0lUJBA/mqdefault.jpg) no-repeat center center; background-size: cover;"></div>
	</div>
    <br><h4 class="match-name">DJI Polo</h4><h4 class="vs"></h4></a>
</tag>
<tag class=" north cowdray-park-polo-club profiles">
 <a href="#172747778" class="link-lightbox" data-videoid="172747778" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-172747778" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">PoloChannel Stories: Kian Hall</h4><h4 class="vs"></h4></a>
</tag>
<tag class=" south cowdray-park-polo-club games">
 <a href="#175311549" class="link-lightbox" data-videoid="175311549" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-175311549" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2016 British Open Gold Cup Finals</h4><h4 class="vs"></h4></a>
</tag>
<tag class=" south cowdray-park-polo-club featured">
 <a href="#174733471" class="link-lightbox" data-videoid="174733471" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-174733471" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2016 British Open Finals Promo</h4><h4 class="vs"></h4></a>
</tag>
<tag class=" south cowdray-park-polo-club games">
 <a href="#174722845" class="link-lightbox" data-videoid="174722845" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-174722845" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">07/13 King Power vs Valiente</h4><h4 class="vs">2016 British Open Semi-final</h4></a>
</tag>
<tag class=" south cowdray-park-polo-club games">
 <a href="#174805828" class="link-lightbox" data-videoid="174805828" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-174805828" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2016 British Open Quarter Final Recap</h4><h4 class="vs"></h4></a>
</tag>
<tag class=" south cowdray-park-polo-club games">
 <a href="#174669425" class="link-lightbox" data-videoid="174669425" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-174669425" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">07/13 La Indiana vs Zacara</h4><h4 class="vs">2016 British Open Semi-final</h4></a>
</tag>
<tag class=" south cowdray-park-polo-club games">
 <a href="#174440002" class="link-lightbox" data-videoid="174440002" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-174440002" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">07/10 King Power vs El Remanso</h4><h4 class="vs">2016 British Open Quarter Finals</h4></a>
</tag>
<tag class=" south cowdray-park-polo-club games">
 <a href="#174602074" class="link-lightbox" data-videoid="174602074" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-174602074" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">07/10 Talandracas vs La Indiana</h4><h4 class="vs">2016 British Open Quarter Finals</h4></a>
</tag>
<tag class=" south cowdray-park-polo-club games">
 <a href="#174182658" class="link-lightbox" data-videoid="174182658" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-174182658" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">07/09 Clarke and Green vs Valiente</h4><h4 class="vs">2016 British Open Quarter Finals</h4></a>
</tag>
<tag class=" south cowdray-park-polo-club games">
 <a href="#174136115" class="link-lightbox" data-videoid="174136115" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-174136115" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">07/09 Zacara vs HB Polo</h4><h4 class="vs">2016 British Open Quarter Final</h4></a>
</tag>
<tag class=" south cowdray-park-polo-club games">
 <a href="#174793966" class="link-lightbox" data-videoid="174793966" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-174793966" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2016 British Open Pre-Quarter Final Recap</h4><h4 class="vs"></h4></a>
</tag>
<tag class=" south cowdray-park-polo-club featured">
 <a href="#174661628" class="link-lightbox" data-videoid="174661628" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-174661628" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2016 British Open Gold Cup</h4><h4 class="vs">Introduction</h4></a>
</tag>
<tag class=" south cowdray-park-polo-club games">
 <a href="#174081451" class="link-lightbox" data-videoid="174081451" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-174081451" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">07/06 La Indiana vs Murus Sanctus</h4><h4 class="vs">2016 British OpenÂ </h4></a>
</tag>
<tag class=" south cowdray-park-polo-club games">
 <a href="#173996540" class="link-lightbox" data-videoid="173996540" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-173996540" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">07/06 Valiente vs El Remanso</h4><h4 class="vs">2016 British Open</h4></a>
</tag>
<tag class=" south cowdray-park-polo-club games">
 <a href="#173965653" class="link-lightbox" data-videoid="173965653" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-173965653" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">07/05 King Power vs Talandracs</h4><h4 class="vs">2016 British Open</h4></a>
</tag>
<tag class=" south cowdray-park-polo-club games">
 <a href="#173653268" class="link-lightbox" data-videoid="173653268" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-173653268" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">07/05 Zacara vs Apes Hill</h4><h4 class="vs">2016 British OpenÂ </h4></a>
</tag>
<tag class=" south cowdray-park-polo-club games">
 <a href="#173448941" class="link-lightbox" data-videoid="173448941" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-173448941" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">07/03 Cowdray Vikings vs La Indiana</h4><h4 class="vs">2016 British Open</h4></a>
</tag>
<tag class=" south cowdray-park-polo-club games">
 <a href="#173384466" class="link-lightbox" data-videoid="173384466" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-173384466" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">07/03 Britannia El Remanso vs Murus Sanctus</h4><h4 class="vs">2016 British Open</h4></a>
</tag>
<tag class=" south cowdray-park-polo-club games">
 <a href="#173380859" class="link-lightbox" data-videoid="173380859" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-173380859" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">07/02 Zacara vs HB Polo</h4><h4 class="vs">2016 British Open</h4></a>
</tag>
<tag class=" south cowdray-park-polo-club games">
 <a href="#173377177" class="link-lightbox" data-videoid="173377177" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-173377177" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">07/02 King Power Foxes vs Clarke & Green</h4><h4 class="vs">2016 British OpenÂ </h4></a>
</tag>
<tag class=" south cowdray-park-polo-club games">
 <a href="#173043122" class="link-lightbox" data-videoid="173043122" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-173043122" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">06/30 Valiente vs Murus Sanctus</h4><h4 class="vs">2016 British Open</h4></a>
</tag>
<tag class=" south cowdray-park-polo-club games">
 <a href="#173040928" class="link-lightbox" data-videoid="173040928" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-173040928" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">06/29 Cowdray Vikings vs El Remanso</h4><h4 class="vs">2016 British Open</h4></a>
</tag>
<tag class=" south cowdray-park-polo-club games">
 <a href="#173038119" class="link-lightbox" data-videoid="173038119" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-173038119" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">06/29 HB Polo vs Clarke & Green</h4><h4 class="vs">2016 British Open</h4></a>
</tag>
<tag class=" south cowdray-park-polo-club games">
 <a href="#172869892" class="link-lightbox" data-videoid="172869892" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-172869892" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">06/29 Apes Hill vs La Bamba De Areco</h4><h4 class="vs">2016 British OpenÂ </h4></a>
</tag>
<tag class=" south cowdray-park-polo-club games">
 <a href="#172697733" class="link-lightbox" data-videoid="172697733" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-172697733" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">06/28 King Power Foxes vs Zacara</h4><h4 class="vs">2016 British OpenÂ </h4></a>
</tag>
<tag class=" south cowdray-park-polo-club games">
 <a href="#172465164" class="link-lightbox" data-videoid="172465164" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-172465164" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">06/27 La Indiana vs Valiente</h4><h4 class="vs">2016 British Open</h4></a>
</tag>
<tag class=" south cowdray-park-polo-club games">
 <a href="#172338259" class="link-lightbox" data-videoid="172338259" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-172338259" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">06/26 Murus Sanctus vs Cowdray Vikings</h4><h4 class="vs">2016 British Open</h4></a>
</tag>
<tag class=" south cowdray-park-polo-club games">
 <a href="#172334336" class="link-lightbox" data-videoid="172334336" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-172334336" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">06/26 La Bamba de Areco vs HB Polo</h4><h4 class="vs">2016 British Open</h4></a>
</tag>
<tag class=" south cowdray-park-polo-club games">
 <a href="#172318944" class="link-lightbox" data-videoid="172318944" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-172318944" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">06/25 Talandracas vs Apes Hill</h4><h4 class="vs">2016 British Open</h4></a>
</tag>
<tag class=" south cowdray-park-polo-club games">
 <a href="#172274886" class="link-lightbox" data-videoid="172274886" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-172274886" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">06/25 Clarke and Greene vs RH Polo</h4><h4 class="vs">2016 British Open</h4></a>
</tag>
<tag class=" central cowdray-park-polo-club games">
 <a href="#172092603" class="link-lightbox" data-videoid="172092603" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-172092603" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">Apes Hill vs Clarke&Greene</h4><h4 class="vs">2016 British Open</h4></a>
</tag>
<tag class=" south cowdray-park-polo-club games">
 <a href="#172092615" class="link-lightbox" data-videoid="172092615" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-172092615" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">Zacara vs RH Polo</h4><h4 class="vs">2016 British Open</h4></a>
</tag>
<tag class=" south cowdray-park-polo-club games">
 <a href="#171906979" class="link-lightbox" data-videoid="171906979" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-171906979" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">Valiente vs Cowdray Vikings</h4><h4 class="vs">2016 British open</h4></a>
</tag>
<tag class=" south cowdray-park-polo-club games">
 <a href="#171844797" class="link-lightbox" data-videoid="171844797" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-171844797" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">Talandracas vs HB Polo</h4><h4 class="vs">2016 British Open</h4></a>
</tag>
<tag class=" central guards-polo-club featured">
 <a href="#174359658" class="link-lightbox" data-videoid="174359658" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-174359658" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2016 Queens Cup</h4><h4 class="vs">Finals Promo</h4></a>
</tag>
<tag class=" central guards-polo-club games">
 <a href="#170693576" class="link-lightbox" data-videoid="170693576" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-170693576" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">Dubai vs La Indiana</h4><h4 class="vs">2016 Queens Cup Finals</h4></a>
</tag>
<tag class=" central guards-polo-club games">
 <a href="#170098809" class="link-lightbox" data-videoid="170098809" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-170098809" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">Quarter-finals Recap</h4><h4 class="vs">2016 Queens Cup</h4></a>
</tag>
<tag class=" central guards-polo-club games">
 <a href="#169927505" class="link-lightbox" data-videoid="169927505" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-169927505" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">Semi-finals Dubai vs Zacara</h4><h4 class="vs">2016 Queens CupÂ </h4></a>
</tag>
<tag class=" central guards-polo-club games">
 <a href="#169919645" class="link-lightbox" data-videoid="169919645" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-169919645" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">King Power Foxes vs La Indiana</h4><h4 class="vs">2016 Queens CupÂ Semi-finalsÂ </h4></a>
</tag>
<tag class=" central guards-polo-club featured">
 <a href="#174359809" class="link-lightbox" data-videoid="174359809" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-174359809" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2016 Queens Cup Introduction</h4><h4 class="vs"></h4></a>
</tag>
<tag class=" central guards-polo-club games">
 <a href="#168448946" class="link-lightbox" data-videoid="168448946" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-168448946" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">Dubai VS Zacara</h4><h4 class="vs">2016 Cartier Queen\'s Cup</h4></a>
</tag>
<tag class=" central guards-polo-club games">
 <a href="#167953076" class="link-lightbox" data-videoid="167953076" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-167953076" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">La Bamba VS La Indiana 24th</h4><h4 class="vs">2016 Cartier Queen\'s Cup</h4></a>
</tag>
<tag class=" central guards-polo-club games">
 <a href="#167642622" class="link-lightbox" data-videoid="167642622" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-167642622" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">Sifani VS Zacara</h4><h4 class="vs">2016 Cartier Queens Cup</h4></a>
</tag>
<tag class=" central guards-polo-club games">
 <a href="#167326092" class="link-lightbox" data-videoid="167326092" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-167326092" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">El Remanso VS Sifani</h4><h4 class="vs">Cartier Queen\'s Cup</h4></a>
</tag>
<tag class=" central guards-polo-club games">
 <a href="#167731444" class="link-lightbox" data-videoid="167731444" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-167731444" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">22/5 King Power Foxes VS Talandracas</h4><h4 class="vs">2016 Cartier Queens Cup</h4></a>
</tag>
<tag class=" central guards-polo-club games">
 <a href="#167938734" class="link-lightbox" data-videoid="167938734" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-167938734" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">Dubai VS El Remanso</h4><h4 class="vs">2016 Cartier Queen\'s Cup</h4></a>
</tag>
<tag class=" central guards-polo-club games">
 <a href="#167567613" class="link-lightbox" data-videoid="167567613" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-167567613" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">Valiente vs RH Polo</h4><h4 class="vs">2016 Cartier Queens Cup</h4></a>
</tag>
<tag class=" central guards-polo-club games">
 <a href="#167564233" class="link-lightbox" data-videoid="167564233" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-167564233" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">HB Polo vs Apes Hill</h4><h4 class="vs">2016 Cartier Queens Cup</h4></a>
</tag>
<tag class=" north guards-polo-club events">
 <a href="#167331563" class="link-lightbox" data-videoid="167331563" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-167331563" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">King Power Foxes vs La Indiana</h4><h4 class="vs">2016 Cartier Queens Cup</h4></a>
</tag>
<tag class=" central guards-polo-club games">
 <a href="#167180358" class="link-lightbox" data-videoid="167180358" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-167180358" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">Valiente vs Dubai</h4><h4 class="vs">2016 Cartier Queens Cup</h4></a>
</tag>
<tag class=" central guards-polo-club games">
 <a href="#167139005" class="link-lightbox" data-videoid="167139005" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-167139005" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">Apes Hill vs La Bamba</h4><h4 class="vs">2016 Cartier Queens Cup</h4></a>
</tag>
<tag class=" central guards-polo-club games">
 <a href="#167180524" class="link-lightbox" data-videoid="167180524" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-167180524" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">Talandracas vs HB Polo</h4><h4 class="vs">2016 Cartier Queens Cup</h4></a>
</tag>
<tag class=" central guards-polo-club games">
 <a href="#167011228" class="link-lightbox" data-videoid="167011228" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-167011228" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">RH Polo vs Zacara</h4><h4 class="vs">2016 Cartier Queens Cup</h4></a>
</tag>
<tag class=" central guards-polo-club games">
 <a href="#167731444" class="link-lightbox" data-videoid="167731444" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-167731444" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">King Power Foxes vs Talandracas</h4><h4 class="vs">2016 Cartier Queens Cup</h4></a>
</tag>
<tag class=" north guards-polo-club events">
 <a href="#167326092" class="link-lightbox" data-videoid="167326092" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-167326092" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">El Remanso vs Sifani</h4><h4 class="vs">2016 Cartier Queens Cup</h4></a>
</tag>
<tag class=" central guards-polo-club games">
 <a href="#167953076" class="link-lightbox" data-videoid="167953076" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-167953076" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">La Bamba VS La Indiana</h4><h4 class="vs">2016 Cartier Queens Cup</h4></a>
</tag>
<tag class=" east ellerston-polo-club events">
 <a href="#168184996" class="link-lightbox" data-videoid="168184996" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-168184996" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">Ellerston 2016</h4><h4 class="vs">Autumn Polo Season</h4></a>
</tag>
<tag class=" east tanoira-polo-club profiles">
 <a href="#157750810" class="link-lightbox" data-videoid="157750810" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-157750810" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">All Pro Polo League</h4><h4 class="vs"></h4></a>
</tag>
<tag class=" east desert-palm-polo-club events">
 <a href="#163535831" class="link-lightbox" data-videoid="163535831" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-163535831" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">Gold Cup Experience</h4><h4 class="vs">Dubai 2016</h4></a>
</tag>
<tag class=" north eldorado-polo-club events">
 <a href="#158221484" class="link-lightbox" data-videoid="158221484" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-158221484" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">Eldorado Polo Club</h4><h4 class="vs"></h4></a>
</tag>
<tag class=" east tang-polo-club events">
 <a href="#CYMd9y9XzVQ" class="link-lightbox" data-videoid="CYMd9y9XzVQ" data-videosite="youtube">
 	<div class="img-contain">
	<div style="background: url(http://img.youtube.com/vi/CYMd9y9XzVQ/mqdefault.jpg) no-repeat center center; background-size: cover;"></div>
	</div>
    <br><h4 class="match-name">British Polo Day</h4><h4 class="vs">China 2014</h4></a>
</tag>
<tag class=" east ghantoot-racing-and-polo-club events">
 <a href="#UbJGodUUlj0" class="link-lightbox" data-videoid="UbJGodUUlj0" data-videosite="youtube">
 	<div class="img-contain">
	<div style="background: url(http://img.youtube.com/vi/UbJGodUUlj0/mqdefault.jpg) no-repeat center center; background-size: cover;"></div>
	</div>
    <br><h4 class="match-name">Sentebale Polo</h4><h4 class="vs">Cup 2014</h4></a>
</tag>
<tag class=" central copenhagen-polo-club profiles">
 <a href="#TYMcbXdJa1Q" class="link-lightbox" data-videoid="TYMcbXdJa1Q" data-videosite="youtube">
 	<div class="img-contain">
	<div style="background: url(http://img.youtube.com/vi/TYMcbXdJa1Q/mqdefault.jpg) no-repeat center center; background-size: cover;"></div>
	</div>
    <br><h4 class="match-name">The Player</h4><h4 class="vs">Copenhagen Polo Club</h4></a>
</tag>
<tag class=" west hawaii-polo-club profiles">
 <a href="#NJ6puAUhvUQ" class="link-lightbox" data-videoid="NJ6puAUhvUQ" data-videosite="youtube">
 	<div class="img-contain">
	<div style="background: url(http://img.youtube.com/vi/NJ6puAUhvUQ/mqdefault.jpg) no-repeat center center; background-size: cover;"></div>
	</div>
    <br><h4 class="match-name">The Rider</h4><h4 class="vs">Hawaii Polo Club</h4></a>
</tag>
<tag class=" central maragata-polo-club featured">
 <a href="#2AZNJdMUJxc" class="link-lightbox" data-videoid="2AZNJdMUJxc" data-videosite="youtube">
 	<div class="img-contain">
	<div style="background: url(http://img.youtube.com/vi/2AZNJdMUJxc/mqdefault.jpg) no-repeat center center; background-size: cover;"></div>
	</div>
    <br><h4 class="match-name">Maragata Polo Experience</h4><h4 class="vs">Maragata Zapalla vs Urban Arts</h4></a>
</tag>
<tag class=" east ellerston-polo-club featured">
 <a href="#142716292" class="link-lightbox" data-videoid="142716292" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-142716292" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">Ellerston 2015 Polo Season</h4><h4 class="vs"></h4></a>
</tag>
<tag class=" west santa-barbara-polo-club profiles">
 <a href="#142764142" class="link-lightbox" data-videoid="142764142" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-142764142" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2015 Santa Barbara</h4><h4 class="vs">Polo Club</h4></a>
</tag>
<tag class=" west santa-barbara-polo-club featured">
 <a href="#136267957" class="link-lightbox" data-videoid="136267957" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-136267957" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2015 Heritage Cup Highlights</h4><h4 class="vs"></h4></a>
</tag>
<tag class=" south cowdray-park-polo-club featured">
 <a href="#134500976" class="link-lightbox" data-videoid="134500976" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-134500976" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2015 British Open</h4><h4 class="vs">Finals Higlights</h4></a>
</tag>
<tag class=" south cowdray-park-polo-club games">
 <a href="#134007561" class="link-lightbox" data-videoid="134007561" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-134007561" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2015 British Open Finals</h4><h4 class="vs">King Power Foxes vs UAE</h4></a>
</tag>
<tag class=" south cowdray-park-polo-club featured">
 <a href="#133860509" class="link-lightbox" data-videoid="133860509" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-133860509" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2015 British Open</h4><h4 class="vs">Finals Promo</h4></a>
</tag>
<tag class=" south cowdray-park-polo-club games">
 <a href="#133666093" class="link-lightbox" data-videoid="133666093" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-133666093" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2015 British Open Semi-finals</h4><h4 class="vs">King Power Foxes vs Zacara</h4></a>
</tag>
<tag class=" south cowdray-park-polo-club games">
 <a href="#133669382" class="link-lightbox" data-videoid="133669382" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-133669382" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2015 British Open Semi-finals</h4><h4 class="vs">UAE vs Apes Hill</h4></a>
</tag>
<tag class=" south cowdray-park-polo-club featured">
 <a href="#133498820" class="link-lightbox" data-videoid="133498820" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-133498820" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2015 British Open</h4><h4 class="vs">Â Quarterfinals Recap</h4></a>
</tag>
<tag class=" south cowdray-park-polo-club games">
 <a href="#133476170" class="link-lightbox" data-videoid="133476170" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-133476170" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2015 British Open Quarterfinals</h4><h4 class="vs">King Power Foxes vs RH Polo</h4></a>
</tag>
<tag class=" south cowdray-park-polo-club games">
 <a href="#133478289" class="link-lightbox" data-videoid="133478289" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-133478289" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2015 British Open Quarterfinals</h4><h4 class="vs">UAE vs Salkeld</h4></a>
</tag>
<tag class=" south cowdray-park-polo-club games">
 <a href="#133472900" class="link-lightbox" data-videoid="133472900" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-133472900" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2015 British Open Quarterfinals</h4><h4 class="vs">Zacara vs Dubai</h4></a>
</tag>
<tag class=" south cowdray-park-polo-club games">
 <a href="#133476424" class="link-lightbox" data-videoid="133476424" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-133476424" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2015 British Open Quarterfinals</h4><h4 class="vs">El Remanso vs Apes Hill</h4></a>
</tag>
<tag class=" south cowdray-park-polo-club featured">
 <a href="#133150949" class="link-lightbox" data-videoid="133150949" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-133150949" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2015 British Open</h4><h4 class="vs">Pre-quarterfinals Recap</h4></a>
</tag>
<tag class=" central guards-polo-club games">
 <a href="#131505477" class="link-lightbox" data-videoid="131505477" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-131505477" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2015 Queens Cup Finals</h4><h4 class="vs">King Power Foxes vs. Dubai</h4></a>
</tag>
<tag class=" south guards-polo-club games">
 <a href="#s-aGJtbQdp8" class="link-lightbox" data-videoid="s-aGJtbQdp8" data-videosite="youtube">
 	<div class="img-contain">
	<div style="background: url(http://img.youtube.com/vi/s-aGJtbQdp8/mqdefault.jpg) no-repeat center center; background-size: cover;"></div>
	</div>
    <br><h4 class="match-name">2015 Queens Cup Semi-finals</h4><h4 class="vs">King Power Foxes vs Talandracas</h4></a>
</tag>
<tag class=" west guards-polo-club games">
 <a href="#IDVb3bqhJdw" class="link-lightbox" data-videoid="IDVb3bqhJdw" data-videosite="youtube">
 	<div class="img-contain">
	<div style="background: url(http://img.youtube.com/vi/IDVb3bqhJdw/mqdefault.jpg) no-repeat center center; background-size: cover;"></div>
	</div>
    <br><h4 class="match-name">2015 Queens Cup Semi-finals</h4><h4 class="vs">Dubai vs UAE</h4></a>
</tag>
<tag class=" south international-polo-club featured">
 <a href="#125358856" class="link-lightbox" data-videoid="125358856" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-125358856" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2015 U.S. Open Promo</h4><h4 class="vs"></h4></a>
</tag>
<tag class=" south international-polo-club featured">
 <a href="#122855433" class="link-lightbox" data-videoid="122855433" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-122855433" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2015 Gold Cup Highlights</h4><h4 class="vs"></h4></a>
</tag>
<tag class=" north guards-polo-club featured">
 <a href="#131678798" class="link-lightbox" data-videoid="131678798" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-131678798" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2015 Queens Cup</h4><h4 class="vs">Â Finals Highlights</h4></a>
</tag>
<tag class=" south ellerstina-polo-club featured">
 <a href="#115073839" class="link-lightbox" data-videoid="115073839" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-115073839" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2014 Ellerstina Gold Cup</h4><h4 class="vs"></h4></a>
</tag>
<tag class=" south ellerstina-polo-club featured">
 <a href="#Y4sIMf2wQLA" class="link-lightbox" data-videoid="Y4sIMf2wQLA" data-videosite="youtube">
 	<div class="img-contain">
	<div style="background: url(http://img.youtube.com/vi/Y4sIMf2wQLA/mqdefault.jpg) no-repeat center center; background-size: cover;"></div>
	</div>
    <br><h4 class="match-name">2014 Ellerstina Polo Team</h4><h4 class="vs"></h4></a>
</tag>
<tag class=" south international-polo-club games">
 <a href="#133684062" class="link-lightbox" data-videoid="133684062" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-133684062" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2015 U.S. Gold Cup</h4><h4 class="vs">Audi vs Orchard Hill</h4></a>
</tag>
<tag class=" south international-polo-club games">
 <a href="#133078840" class="link-lightbox" data-videoid="133078840" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-133078840" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2015 U.S. Open Final</h4><h4 class="vs">Valiente vs Orchard Hill</h4></a>
</tag>
<tag class=" south international-polo-club games">
 <a href="#121202096" class="link-lightbox" data-videoid="121202096" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-121202096" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2015 C.V. Whitney Final</h4><h4 class="vs">Orchard Hill vs. Audi</h4></a>
</tag>
<tag class=" east ghantoot-racing-and-polo-club featured">
 <a href="#W_i6DYKFJP4" class="link-lightbox" data-videoid="W_i6DYKFJP4" data-videosite="youtube">
 	<div class="img-contain">
	<div style="background: url(http://img.youtube.com/vi/W_i6DYKFJP4/mqdefault.jpg) no-repeat center center; background-size: cover;"></div>
	</div>
    <br><h4 class="match-name">Duplicate of 2015 President\'s Cup</h4><h4 class="vs">H.H President of the
UAE Polo Cup Highlights</h4></a>
</tag>
<tag class=" east desert-palm-polo-club events">
 <a href="#i2521qkdeJ4" class="link-lightbox" data-videoid="i2521qkdeJ4" data-videosite="youtube">
 	<div class="img-contain">
	<div style="background: url(http://img.youtube.com/vi/i2521qkdeJ4/mqdefault.jpg) no-repeat center center; background-size: cover;"></div>
	</div>
    <br><h4 class="match-name">Duplicate of Cartier Dubai</h4><h4 class="vs">Challenge 2015</h4></a>
</tag>
<tag class=" east desert-palm-polo-club events">
 <a href="#_35_-rOiPmg" class="link-lightbox" data-videoid="_35_-rOiPmg" data-videosite="youtube">
 	<div class="img-contain">
	<div style="background: url(http://img.youtube.com/vi/_35_-rOiPmg/mqdefault.jpg) no-repeat center center; background-size: cover;"></div>
	</div>
    <br><h4 class="match-name">Duplicate of 2015 Silver Cup Dubai</h4><h4 class="vs">Hildon Cup Final Highlights</h4></a>
</tag>
<tag class=" south international-polo-club featured">
 <a href="#120401367" class="link-lightbox" data-videoid="120401367" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-120401367" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2015 Joe Barry Memorial</h4><h4 class="vs">Finals Promo</h4></a>
</tag>
<tag class=" south international-polo-club games">
 <a href="#120438874" class="link-lightbox" data-videoid="120438874" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-120438874" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2015 Joe Barry Memorial</h4><h4 class="vs">Orchard Hill vs Villa Del Lago</h4></a>
</tag>
<tag class=" east ghantoot-racing-and-polo-club events">
 <a href="#W53lelM2NFM" class="link-lightbox" data-videoid="W53lelM2NFM" data-videosite="youtube">
 	<div class="img-contain">
	<div style="background: url(http://img.youtube.com/vi/W53lelM2NFM/mqdefault.jpg) no-repeat center center; background-size: cover;"></div>
	</div>
    <br><h4 class="match-name">Duplicate of 2015 Emirates Open</h4><h4 class="vs">Finals Highlights</h4></a>
</tag>
<tag class=" north la-dolfina events">
 <a href="#85665738" class="link-lightbox" data-videoid="85665738" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-85665738" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2014 La Dolfina Polo Tips</h4><h4 class="vs"></h4></a>
</tag>
      </div>
    </div>
  </div>
<!-- Favorite -->
<div class="arrows">
      <div class="left-arrow most-popular"><i class="fa fa-arrow-left" aria-hidden="true"></i></div>
      <div class="right-arrow most-popular"><i class="fa fa-arrow-right" aria-hidden="true"></i></div>
      <h3>Most Popular</h3>
    <div class="cut-off" id="most-popular">

      <div class="vids search_list">

          <tag class=" south international-polo-club featured">
 <a href="#K9Kvs0lUJBA" class="link-lightbox" data-videoid="K9Kvs0lUJBA" data-videosite="youtube">
 	<div class="img-contain">
	<div style="background: url(http://img.youtube.com/vi/K9Kvs0lUJBA/mqdefault.jpg) no-repeat center center; background-size: cover;"></div>
	</div>
    <br><h4 class="match-name">DJI Polo</h4><h4 class="vs"></h4></a>
</tag>
<tag class=" south cowdray-park-polo-club games">
 <a href="#175311549" class="link-lightbox" data-videoid="175311549" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-175311549" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2016 British Open Gold Cup Finals</h4><h4 class="vs"></h4></a>
</tag>
<tag class=" south cowdray-park-polo-club featured">
 <a href="#174661628" class="link-lightbox" data-videoid="174661628" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-174661628" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2016 British Open Gold Cup</h4><h4 class="vs">Introduction</h4></a>
</tag>
<tag class=" central guards-polo-club featured">
 <a href="#174359658" class="link-lightbox" data-videoid="174359658" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-174359658" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2016 Queens Cup</h4><h4 class="vs">Finals Promo</h4></a>
</tag>
<tag class=" central guards-polo-club games">
 <a href="#170693576" class="link-lightbox" data-videoid="170693576" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-170693576" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">Dubai vs La Indiana</h4><h4 class="vs">2016 Queens Cup Finals</h4></a>
</tag>
<tag class=" central guards-polo-club featured">
 <a href="#174359809" class="link-lightbox" data-videoid="174359809" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-174359809" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2016 Queens Cup Introduction</h4><h4 class="vs"></h4></a>
</tag>
<tag class=" east ellerston-polo-club events">
 <a href="#168184996" class="link-lightbox" data-videoid="168184996" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-168184996" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">Ellerston 2016</h4><h4 class="vs">Autumn Polo Season</h4></a>
</tag>
<tag class=" east tanoira-polo-club profiles">
 <a href="#157750810" class="link-lightbox" data-videoid="157750810" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-157750810" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">All Pro Polo League</h4><h4 class="vs"></h4></a>
</tag>
<tag class=" north eldorado-polo-club events">
 <a href="#158221484" class="link-lightbox" data-videoid="158221484" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-158221484" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">Eldorado Polo Club</h4><h4 class="vs"></h4></a>
</tag>
<tag class=" west hawaii-polo-club profiles">
 <a href="#NJ6puAUhvUQ" class="link-lightbox" data-videoid="NJ6puAUhvUQ" data-videosite="youtube">
 	<div class="img-contain">
	<div style="background: url(http://img.youtube.com/vi/NJ6puAUhvUQ/mqdefault.jpg) no-repeat center center; background-size: cover;"></div>
	</div>
    <br><h4 class="match-name">The Rider</h4><h4 class="vs">Hawaii Polo Club</h4></a>
</tag>
<tag class=" west santa-barbara-polo-club profiles">
 <a href="#142764142" class="link-lightbox" data-videoid="142764142" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-142764142" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2015 Santa Barbara</h4><h4 class="vs">Polo Club</h4></a>
</tag>
<tag class=" south cowdray-park-polo-club featured">
 <a href="#134500976" class="link-lightbox" data-videoid="134500976" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-134500976" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2015 British Open</h4><h4 class="vs">Finals Higlights</h4></a>
</tag>
<tag class=" south cowdray-park-polo-club games">
 <a href="#134007561" class="link-lightbox" data-videoid="134007561" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-134007561" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2015 British Open Finals</h4><h4 class="vs">King Power Foxes vs UAE</h4></a>
</tag>
<tag class=" south cowdray-park-polo-club featured">
 <a href="#133860509" class="link-lightbox" data-videoid="133860509" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-133860509" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2015 British Open</h4><h4 class="vs">Finals Promo</h4></a>
</tag>
<tag class=" south cowdray-park-polo-club featured">
 <a href="#133498820" class="link-lightbox" data-videoid="133498820" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-133498820" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2015 British Open</h4><h4 class="vs">Â Quarterfinals Recap</h4></a>
</tag>
<tag class=" central guards-polo-club games">
 <a href="#131505477" class="link-lightbox" data-videoid="131505477" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-131505477" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2015 Queens Cup Finals</h4><h4 class="vs">King Power Foxes vs. Dubai</h4></a>
</tag>
<tag class=" south international-polo-club featured">
 <a href="#125358856" class="link-lightbox" data-videoid="125358856" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-125358856" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2015 U.S. Open Promo</h4><h4 class="vs"></h4></a>
</tag>
<tag class=" south international-polo-club featured">
 <a href="#122855433" class="link-lightbox" data-videoid="122855433" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-122855433" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2015 Gold Cup Highlights</h4><h4 class="vs"></h4></a>
</tag>
<tag class=" north guards-polo-club featured">
 <a href="#131678798" class="link-lightbox" data-videoid="131678798" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-131678798" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2015 Queens Cup</h4><h4 class="vs">Â Finals Highlights</h4></a>
</tag>
<tag class=" east ghantoot-racing-and-polo-club featured">
 <a href="#W_i6DYKFJP4" class="link-lightbox" data-videoid="W_i6DYKFJP4" data-videosite="youtube">
 	<div class="img-contain">
	<div style="background: url(http://img.youtube.com/vi/W_i6DYKFJP4/mqdefault.jpg) no-repeat center center; background-size: cover;"></div>
	</div>
    <br><h4 class="match-name">Duplicate of 2015 President\'s Cup</h4><h4 class="vs">H.H President of the
UAE Polo Cup Highlights</h4></a>
</tag>
<tag class=" south international-polo-club featured">
 <a href="#120401367" class="link-lightbox" data-videoid="120401367" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-120401367" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2015 Joe Barry Memorial</h4><h4 class="vs">Finals Promo</h4></a>
</tag>
<tag class=" south international-polo-club games">
 <a href="#120438874" class="link-lightbox" data-videoid="120438874" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-120438874" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2015 Joe Barry Memorial</h4><h4 class="vs">Orchard Hill vs Villa Del Lago</h4></a>
</tag>
      </div>
    </div>
  </div>
    
    [[!getResources? &depth=`0` &parents=`1` &resources=`-4,-5` &sortby=`{"menuindex":"ASC"}` &limit=`` &tpl=`parent` &includeTVs=`1` &processTVs=`1` &tvPrefix=``]]

</ul>
<script>
simplemaps_worldmap_mapdata.locations = [[!polo_locations]];
</script>

<!-- javascript !-->

<script type="text/javascript"> 
$(\'.vids a\').simpleLightboxVideo();
</script>
<script src="js/custom.js"></script>

</body>
</html>
',
    '_isForward' => false,
  ),
  'contentType' => 
  array (
    'id' => 1,
    'name' => 'HTML',
    'description' => 'HTML content',
    'mime_type' => 'text/html',
    'file_extensions' => '',
    'headers' => NULL,
    'binary' => 0,
  ),
  'policyCache' => 
  array (
  ),
  'elementCache' => 
  array (
    '[[*description]]' => 'Polo Channel hosts videos of polo clubs, tournaments, and events from around the globe',
    '[[$doc_head]]' => '<!doctype html>
<head>
<meta charset="UTF-8">
<title>Polo Channel</title>
<link href="http://local.polo.com//css/main.css" rel="stylesheet" type="text/css" />
<link rel="icon" type="image/png" href="http://polochannel.com/favicon.png">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script type="text/javascript" src="http://local.polo.com//js/lightbox.min.js"></script>
<script type="text/javascript" src="http://local.polo.com//js/touchSwipe.min.js"></script>
<meta name="description" content="Polo Channel hosts videos of polo clubs, tournaments, and events from around the globe">
        <meta name="viewport" content="width=device-width,initial-scale=1.0">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

        <!-- THIS IS NEEDED FOR FURL -->
        <base href="http://local.polo.com/" />
        
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width,initial-scale=1.0">

        <!-- Facebook OpenGraph Data -->
        <meta property="og:title" content="Polo Channel" />
        <meta property="og:type" content="website" />
        <!-- If it\'s an article:
        <meta property="og:type" content="article" />
        <meta property="article:published_time" content = "" />
        -->
        <meta property="og:description" content="Polo Channel hosts videos of polo clubs, tournaments, and events from around the globe" />        
        <meta property="og:url" content="http://local.polo.com/"/>
        <meta property="og:image" content="http://local.polo.com/images/ogimage.jpg" />
        <meta property="og:site_name" content="Polo Channel" />


<script src="js/respond.min.js"></script>

<script src="http://local.polo.com//js/mapdata.js"></script>
<script src="http://local.polo.com//js/worldmap.js"></script>
<script src="http://local.polo.com//js/jquery.fastLiveFilter.js"></script>
<script src="http://local.polo.com//js/jquery.countdown.min.js"></script>
<script src="http://local.polo.com//js/modernizr.custom.js"></script>
<link rel="stylesheet" type="text/css" href="css/lightbox.min.css">
<meta name="google-site-verification" content="lyzPX5GCX9Yi9NxiLhZGlEnTRGSsqA6xKj1sbBaz_Yo" />
<link href="http://local.polo.com//css/normalize.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<script>
  (function(i,s,o,g,r,a,m){i[\'GoogleAnalyticsObject\']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,\'script\',\'https://www.google-analytics.com/analytics.js\',\'ga\');

  ga(\'create\', \'UA-82621934-1\', \'auto\');
  ga(\'send\', \'pageview\');

</script>
 <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
 <![endif]-->
<script>
    $(function() {
        $(\'#search_input\').fastLiveFilter(\'.search_list\');
        $(\'#search_input_mobile\').fastLiveFilter(\'.search_list\');
    });

       $(window).load(function() {
      $(\'#loading\').fadeOut("slow");
      hideEmpty();
    });

    function hideEmpty() {
$(".vids").each(function(){

 if($(this).children(\'tag:visible\').length == 0) {
   $(this.parentNode.parentNode).hide(); 
}
else {
 $(this.parentNode.parentNode).show();
}
});

}
</script>



<!--[if lt IE 9]>
<script>
document.createElement(\'video\');
</script>
<![endif]-->

</head>',
    '[[*id]]' => 1,
    '[[~1]]' => 'http://local.polo.com/',
    '[[$subscribe]]' => '

<div class="modal-body" id="subscribe">

     [[!FormIt?
   &hooks=`email, FormItSaveForm, redirect`
   &emailTpl=`email`
   &emailFrom=`no-reply@polochannel.com`
   &emailTo=`michael@ten25designs.com`
   &validate=`name:required,
      email:required`
   &redirectTo=`1`
]]

<form class="contact" action="http://local.polo.com/" method="post">
<i style="position: absolute; right:0; top: 0; color: gray; text-align: right; float: right; margin-right: 10px; margin-top: 10px; font-size: 20px; z-index: 5000;" class="fa fa-close"></i>
    <fieldset>
         <h3>Subscribe to our news</h3>
        <input type="text" name="name" value="[[!+fi.first_name]]" placeholder="First Name">
        <input type="text" name="email" value="[[!+fi.email]]" placeholder="email">
       <input class="send" type="submit" value="Send"> <!-- Send button-->
   </fieldset>
</form>

</div>',
    '[[*live-stream]]' => '',
    '[[*live-stream:notempty=`<div class="hide-vid">
  <div class="live-stream">
    <div class="vid-contain">
      <iframe  src="" frameborder="0" allowfullscreen></iframe>
    </div>
  </div>
  <div class="background-color close-stream"><img src="images/x.png" class="close-stream" alt=""></div>
</div>`]]' => '',
    '[[$header3]]' => '<div class="header3">
	  <ul class="nav">
		<li><a href="/">Home</a></li>
  
		<li><a class="videos" href="/">Videos</a></li>
  
		<li><a href="/polo-blog">News</a></li>
  

		<li><div><a href="/" class="subscribe">Subscribe</a></div></li>
	  </ul>
	</div>',
    '[[*live-stream:notempty=`<li><a href=""><button class="show-stream" type="button">live stream </button></a></li>`]]' => '',
    '[[*link-1]]' => '',
    '[[*link-1-text]]' => '',
    '[[*link-1-text:notempty=`<li><a href="" class="append"><button type="button"></button></a><span id="append-back"></span></li>`]]' => '',
    '[[*link-2]]' => '',
    '[[*link-2-text]]' => '',
    '[[*link-2-text:notempty=`<li><a href=""><button type="button"></button></a></li>`]]' => '',
    '[[$buttons]]' => '<ul class="headerlinks">
	      

      
          <li><a href="https://www.facebook.com/polochannelnetwork" target="_blank"><div class="fa fa-facebook-official  fa-lg"></div></a></li>
      <li><a href="https://instagram.com/thepolochannel" target="_blank"><div class="fa fa-instagram  fa-lg"></div></a></li>
      <li><a href="mailto:contact@horseplay.tv"><div class="fa fa-envelope  fa-lg"></div></a></li>
  </ul>',
    '[[$header]]' => '<div class="fixed-header">
  <div class="header">

<div class="header3">
	  <ul class="nav">
		<li><a href="/">Home</a></li>
  
		<li><a class="videos" href="/">Videos</a></li>
  
		<li><a href="/polo-blog">News</a></li>
  

		<li><div><a href="/" class="subscribe">Subscribe</a></div></li>
	  </ul>
	</div>

      <ul class="headerlinks">
	      

      
          <li><a href="https://www.facebook.com/polochannelnetwork" target="_blank"><div class="fa fa-facebook-official  fa-lg"></div></a></li>
      <li><a href="https://instagram.com/thepolochannel" target="_blank"><div class="fa fa-instagram  fa-lg"></div></a></li>
      <li><a href="mailto:contact@horseplay.tv"><div class="fa fa-envelope  fa-lg"></div></a></li>
  </ul>
    <a href="/"><img src="images/mainlogo.png" alt="" height="70" class="headerimage"></a>
  </div>

  
</div>
<div class="spacer"></div>
<div class="not-so-fixie">',
    '[[$feed]]' => '<div class="polo-feed">
      <div class="three-col small-col">
			[[!getResources? &parents=`187` &limit=`999` &tpl=`blog_post` &includeTVs=`1` &processTVs=`1`]]
      </div>
      <div class="three-col large-col">

      </div>
	  <div class="append-back">
      <div class="three-col small-col" id="score-list">
			[[!getResources? &parents=`190` &limit=`999` &tpl=`score_post` &includeTVs=`1` &processTVs=`1`]]
      </div>
	  </div>
</div>',
    '[[$header2]]' => '<div class="header2">
    <ul class="nav">
      <li class="group_toggle" id="group1">games&nbsp;<img src="map_images/blue.png" height="20"></li>
      <li><div class="v-bar"></div></li>
      <li class="group_toggle" id="group3">featured&nbsp;<img src="map_images/yellow.png" height="20"></li>
      <li><div class="v-bar"></div></li>
      <li class="group_toggle" id="group2">profiles&nbsp;<img src="map_images/green.png" height="20"></li>
      <li><div class="v-bar"></div></li>
      <li class="group_toggle" id="group0">events&nbsp;<img src="map_images/orange.png" height="20"></li>
      <li><div class="v-bar" height="20"></div></li>
      <li><div class="showall">show all</div></li>
      <li><input type="text" id="search_input" placeholder="search..."></li>


	  <li><div class="map-btn">Map</div></li>
       
    </ul>

  </div>',
    '[[getResources? &parents=`32` &sortby=`{"menuindex":"ASC"}` &limit=`0` &tpl=`clubs` &includeTVs=`1` &processTVs=`1` &showUnpublished=`1` &showHidded=`1` &tvPrefix=``]]' => '<li>International-Polo-Club<input id="northamerica,south" class="checkbox international-polo-club" type="checkbox"/></li>

<li>Guards-Polo-Club<input id="europe,west" class="checkbox guards-polo-club" type="checkbox"/></li>

<li>Cowdray-Park-Polo-Club<input id="europe,south" class="checkbox cowdray-park-polo-club" type="checkbox"/></li>

<li>Ghantoot-Racing-and-Polo-Club<input id="africa,east" class="checkbox ghantoot-racing-and-polo-club" type="checkbox"/></li>

<li>Desert-Palm-Polo-Club<input id="northamerica,north" class="checkbox desert-palm-polo-club" type="checkbox"/></li>

<li>Ellerstina-Polo-Club<input id="southamerica,east" class="checkbox ellerstina-polo-club" type="checkbox"/></li>

<li>Maragata-Polo-Club<input id="southamerica,north" class="checkbox maragata-polo-club" type="checkbox"/></li>

<li>Santa-Maria-Polo-Club<input id="europe,south" class="checkbox santa-maria-polo-club" type="checkbox"/></li>

<li>Santa-Barbara-Polo-Club<input id="northamerica,west" class="checkbox santa-barbara-polo-club" type="checkbox"/></li>

<li>La-Dolfina<input id="southamerica,south" class="checkbox la-dolfina" type="checkbox"/></li>

<li>Ellerston-Polo-Club<input id="australia,south" class="checkbox ellerston-polo-club" type="checkbox"/></li>

<li>Hawaii-Polo-Club<input id="northamerica,west" class="checkbox hawaii-polo-club" type="checkbox"/></li>

<li>Copenhagen-Polo-Club<input id="europe,central" class="checkbox copenhagen-polo-club" type="checkbox"/></li>

<li>Tang-Polo-Club<input id="asia,east" class="checkbox tang-polo-club" type="checkbox"/></li>

<li>Jnan-Amar-Polo-Club<input id="africa,west" class="checkbox jnan-amar-polo-club" type="checkbox"/></li>

<li>Eldorado-Polo-Club<input id="northamerica,west" class="checkbox eldorado-polo-club" type="checkbox"/></li>

<li>Tanoira-Polo-Club<input id="southamerica,east" class="checkbox tanoira-polo-club" type="checkbox"/></li>

<li>Myopia-Polo-Club<input id="northamerica,east" class="checkbox myopia-polo-club" type="checkbox"/></li>

<li>Hilario Ulloa<input id="southamerica,west" class="checkbox hilario ulloa" type="checkbox"/></li>
',
    '[[$clubs-nav]]' => '<li>International-Polo-Club<input id="northamerica,south" class="checkbox international-polo-club" type="checkbox"/></li>

<li>Guards-Polo-Club<input id="europe,west" class="checkbox guards-polo-club" type="checkbox"/></li>

<li>Cowdray-Park-Polo-Club<input id="europe,south" class="checkbox cowdray-park-polo-club" type="checkbox"/></li>

<li>Ghantoot-Racing-and-Polo-Club<input id="africa,east" class="checkbox ghantoot-racing-and-polo-club" type="checkbox"/></li>

<li>Desert-Palm-Polo-Club<input id="northamerica,north" class="checkbox desert-palm-polo-club" type="checkbox"/></li>

<li>Ellerstina-Polo-Club<input id="southamerica,east" class="checkbox ellerstina-polo-club" type="checkbox"/></li>

<li>Maragata-Polo-Club<input id="southamerica,north" class="checkbox maragata-polo-club" type="checkbox"/></li>

<li>Santa-Maria-Polo-Club<input id="europe,south" class="checkbox santa-maria-polo-club" type="checkbox"/></li>

<li>Santa-Barbara-Polo-Club<input id="northamerica,west" class="checkbox santa-barbara-polo-club" type="checkbox"/></li>

<li>La-Dolfina<input id="southamerica,south" class="checkbox la-dolfina" type="checkbox"/></li>

<li>Ellerston-Polo-Club<input id="australia,south" class="checkbox ellerston-polo-club" type="checkbox"/></li>

<li>Hawaii-Polo-Club<input id="northamerica,west" class="checkbox hawaii-polo-club" type="checkbox"/></li>

<li>Copenhagen-Polo-Club<input id="europe,central" class="checkbox copenhagen-polo-club" type="checkbox"/></li>

<li>Tang-Polo-Club<input id="asia,east" class="checkbox tang-polo-club" type="checkbox"/></li>

<li>Jnan-Amar-Polo-Club<input id="africa,west" class="checkbox jnan-amar-polo-club" type="checkbox"/></li>

<li>Eldorado-Polo-Club<input id="northamerica,west" class="checkbox eldorado-polo-club" type="checkbox"/></li>

<li>Tanoira-Polo-Club<input id="southamerica,east" class="checkbox tanoira-polo-club" type="checkbox"/></li>

<li>Myopia-Polo-Club<input id="northamerica,east" class="checkbox myopia-polo-club" type="checkbox"/></li>

<li>Hilario Ulloa<input id="southamerica,west" class="checkbox hilario ulloa" type="checkbox"/></li>
                        ',
    '[[*mp4-video]]' => '',
    '[[$feed-mobile]]' => '<div class="polo-feed-mobile">
		<h3>News</h3>
		<div class="scroller">
         [[!getResources? &parents=`187` &limit=`10` &tpl=`blog-list` &includeTVs=`1` &processTVs=`1`]]
		</div>
		<div style="text-align: center; font-size: 10px; color: gray;"><i class="fa fa-arrow-left"></i> swipe to scroll <i class="fa fa-arrow-right"></i></div>
</div>',
    '[[#183.pagetitle:lcase]]' => 'hilario ulloa',
    '[[#44.pagetitle:lcase]]' => 'international-polo-club',
    '[[#53.pagetitle:lcase]]' => 'cowdray-park-polo-club',
    '[[#41.pagetitle:lcase]]' => 'guards-polo-club',
    '[[#104.pagetitle:lcase]]' => 'ellerston-polo-club',
    '[[#117.pagetitle:lcase]]' => 'tanoira-polo-club',
    '[[#60.pagetitle:lcase]]' => 'desert-palm-polo-club',
    '[[#115.pagetitle:lcase]]' => 'eldorado-polo-club',
    '[[#111.pagetitle:lcase]]' => 'tang-polo-club',
    '[[#58.pagetitle:lcase]]' => 'ghantoot-racing-and-polo-club',
    '[[#108.pagetitle:lcase]]' => 'copenhagen-polo-club',
    '[[#106.pagetitle:lcase]]' => 'hawaii-polo-club',
    '[[#70.pagetitle:lcase]]' => 'maragata-polo-club',
    '[[#86.pagetitle:lcase]]' => 'santa-barbara-polo-club',
    '[[#65.pagetitle:lcase]]' => 'ellerstina-polo-club',
    '[[#93.pagetitle:lcase]]' => 'la-dolfina',
    '[[getResources? &parents=`1` &depth=`1`  &where=`{"template:=":1}` &sortby=`{"publishedon":"DESC"}` &limit=`0` &includeContent=`1` &tpl=`item-recent` &includeTVs=`1` &processTVs=`1` &tvPrefix=``]]' => '<tag class=" west hilario ulloa profiles">
 <a href="#191725822" class="link-lightbox" data-videoid="191725822" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-191725822" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">Hilario Ulloa</h4><h4 class="vs">10 Goals</h4></a>
</tag>
<tag class=" south international-polo-club featured">
 <a href="#K9Kvs0lUJBA" class="link-lightbox" data-videoid="K9Kvs0lUJBA" data-videosite="youtube">
 	<div class="img-contain">
	<div style="background: url(http://img.youtube.com/vi/K9Kvs0lUJBA/mqdefault.jpg) no-repeat center center; background-size: cover;"></div>
	</div>
    <br><h4 class="match-name">DJI Polo</h4><h4 class="vs"></h4></a>
</tag>
<tag class=" north cowdray-park-polo-club profiles">
 <a href="#172747778" class="link-lightbox" data-videoid="172747778" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-172747778" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">PoloChannel Stories: Kian Hall</h4><h4 class="vs"></h4></a>
</tag>
<tag class=" south cowdray-park-polo-club games">
 <a href="#175311549" class="link-lightbox" data-videoid="175311549" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-175311549" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2016 British Open Gold Cup Finals</h4><h4 class="vs"></h4></a>
</tag>
<tag class=" south cowdray-park-polo-club featured">
 <a href="#174733471" class="link-lightbox" data-videoid="174733471" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-174733471" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2016 British Open Finals Promo</h4><h4 class="vs"></h4></a>
</tag>
<tag class=" south cowdray-park-polo-club games">
 <a href="#174722845" class="link-lightbox" data-videoid="174722845" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-174722845" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">07/13 King Power vs Valiente</h4><h4 class="vs">2016 British Open Semi-final</h4></a>
</tag>
<tag class=" south cowdray-park-polo-club games">
 <a href="#174805828" class="link-lightbox" data-videoid="174805828" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-174805828" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2016 British Open Quarter Final Recap</h4><h4 class="vs"></h4></a>
</tag>
<tag class=" south cowdray-park-polo-club games">
 <a href="#174669425" class="link-lightbox" data-videoid="174669425" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-174669425" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">07/13 La Indiana vs Zacara</h4><h4 class="vs">2016 British Open Semi-final</h4></a>
</tag>
<tag class=" south cowdray-park-polo-club games">
 <a href="#174440002" class="link-lightbox" data-videoid="174440002" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-174440002" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">07/10 King Power vs El Remanso</h4><h4 class="vs">2016 British Open Quarter Finals</h4></a>
</tag>
<tag class=" south cowdray-park-polo-club games">
 <a href="#174602074" class="link-lightbox" data-videoid="174602074" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-174602074" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">07/10 Talandracas vs La Indiana</h4><h4 class="vs">2016 British Open Quarter Finals</h4></a>
</tag>
<tag class=" south cowdray-park-polo-club games">
 <a href="#174182658" class="link-lightbox" data-videoid="174182658" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-174182658" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">07/09 Clarke and Green vs Valiente</h4><h4 class="vs">2016 British Open Quarter Finals</h4></a>
</tag>
<tag class=" south cowdray-park-polo-club games">
 <a href="#174136115" class="link-lightbox" data-videoid="174136115" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-174136115" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">07/09 Zacara vs HB Polo</h4><h4 class="vs">2016 British Open Quarter Final</h4></a>
</tag>
<tag class=" south cowdray-park-polo-club games">
 <a href="#174793966" class="link-lightbox" data-videoid="174793966" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-174793966" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2016 British Open Pre-Quarter Final Recap</h4><h4 class="vs"></h4></a>
</tag>
<tag class=" south cowdray-park-polo-club featured">
 <a href="#174661628" class="link-lightbox" data-videoid="174661628" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-174661628" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2016 British Open Gold Cup</h4><h4 class="vs">Introduction</h4></a>
</tag>
<tag class=" south cowdray-park-polo-club games">
 <a href="#174081451" class="link-lightbox" data-videoid="174081451" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-174081451" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">07/06 La Indiana vs Murus Sanctus</h4><h4 class="vs">2016 British OpenÂ </h4></a>
</tag>
<tag class=" south cowdray-park-polo-club games">
 <a href="#173996540" class="link-lightbox" data-videoid="173996540" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-173996540" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">07/06 Valiente vs El Remanso</h4><h4 class="vs">2016 British Open</h4></a>
</tag>
<tag class=" south cowdray-park-polo-club games">
 <a href="#173965653" class="link-lightbox" data-videoid="173965653" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-173965653" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">07/05 King Power vs Talandracs</h4><h4 class="vs">2016 British Open</h4></a>
</tag>
<tag class=" south cowdray-park-polo-club games">
 <a href="#173653268" class="link-lightbox" data-videoid="173653268" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-173653268" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">07/05 Zacara vs Apes Hill</h4><h4 class="vs">2016 British OpenÂ </h4></a>
</tag>
<tag class=" south cowdray-park-polo-club games">
 <a href="#173448941" class="link-lightbox" data-videoid="173448941" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-173448941" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">07/03 Cowdray Vikings vs La Indiana</h4><h4 class="vs">2016 British Open</h4></a>
</tag>
<tag class=" south cowdray-park-polo-club games">
 <a href="#173384466" class="link-lightbox" data-videoid="173384466" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-173384466" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">07/03 Britannia El Remanso vs Murus Sanctus</h4><h4 class="vs">2016 British Open</h4></a>
</tag>
<tag class=" south cowdray-park-polo-club games">
 <a href="#173380859" class="link-lightbox" data-videoid="173380859" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-173380859" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">07/02 Zacara vs HB Polo</h4><h4 class="vs">2016 British Open</h4></a>
</tag>
<tag class=" south cowdray-park-polo-club games">
 <a href="#173377177" class="link-lightbox" data-videoid="173377177" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-173377177" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">07/02 King Power Foxes vs Clarke & Green</h4><h4 class="vs">2016 British OpenÂ </h4></a>
</tag>
<tag class=" south cowdray-park-polo-club games">
 <a href="#173043122" class="link-lightbox" data-videoid="173043122" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-173043122" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">06/30 Valiente vs Murus Sanctus</h4><h4 class="vs">2016 British Open</h4></a>
</tag>
<tag class=" south cowdray-park-polo-club games">
 <a href="#173040928" class="link-lightbox" data-videoid="173040928" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-173040928" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">06/29 Cowdray Vikings vs El Remanso</h4><h4 class="vs">2016 British Open</h4></a>
</tag>
<tag class=" south cowdray-park-polo-club games">
 <a href="#173038119" class="link-lightbox" data-videoid="173038119" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-173038119" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">06/29 HB Polo vs Clarke & Green</h4><h4 class="vs">2016 British Open</h4></a>
</tag>
<tag class=" south cowdray-park-polo-club games">
 <a href="#172869892" class="link-lightbox" data-videoid="172869892" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-172869892" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">06/29 Apes Hill vs La Bamba De Areco</h4><h4 class="vs">2016 British OpenÂ </h4></a>
</tag>
<tag class=" south cowdray-park-polo-club games">
 <a href="#172697733" class="link-lightbox" data-videoid="172697733" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-172697733" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">06/28 King Power Foxes vs Zacara</h4><h4 class="vs">2016 British OpenÂ </h4></a>
</tag>
<tag class=" south cowdray-park-polo-club games">
 <a href="#172465164" class="link-lightbox" data-videoid="172465164" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-172465164" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">06/27 La Indiana vs Valiente</h4><h4 class="vs">2016 British Open</h4></a>
</tag>
<tag class=" south cowdray-park-polo-club games">
 <a href="#172338259" class="link-lightbox" data-videoid="172338259" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-172338259" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">06/26 Murus Sanctus vs Cowdray Vikings</h4><h4 class="vs">2016 British Open</h4></a>
</tag>
<tag class=" south cowdray-park-polo-club games">
 <a href="#172334336" class="link-lightbox" data-videoid="172334336" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-172334336" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">06/26 La Bamba de Areco vs HB Polo</h4><h4 class="vs">2016 British Open</h4></a>
</tag>
<tag class=" south cowdray-park-polo-club games">
 <a href="#172318944" class="link-lightbox" data-videoid="172318944" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-172318944" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">06/25 Talandracas vs Apes Hill</h4><h4 class="vs">2016 British Open</h4></a>
</tag>
<tag class=" south cowdray-park-polo-club games">
 <a href="#172274886" class="link-lightbox" data-videoid="172274886" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-172274886" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">06/25 Clarke and Greene vs RH Polo</h4><h4 class="vs">2016 British Open</h4></a>
</tag>
<tag class=" central cowdray-park-polo-club games">
 <a href="#172092603" class="link-lightbox" data-videoid="172092603" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-172092603" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">Apes Hill vs Clarke&Greene</h4><h4 class="vs">2016 British Open</h4></a>
</tag>
<tag class=" south cowdray-park-polo-club games">
 <a href="#172092615" class="link-lightbox" data-videoid="172092615" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-172092615" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">Zacara vs RH Polo</h4><h4 class="vs">2016 British Open</h4></a>
</tag>
<tag class=" south cowdray-park-polo-club games">
 <a href="#171906979" class="link-lightbox" data-videoid="171906979" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-171906979" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">Valiente vs Cowdray Vikings</h4><h4 class="vs">2016 British open</h4></a>
</tag>
<tag class=" south cowdray-park-polo-club games">
 <a href="#171844797" class="link-lightbox" data-videoid="171844797" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-171844797" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">Talandracas vs HB Polo</h4><h4 class="vs">2016 British Open</h4></a>
</tag>
<tag class=" central guards-polo-club featured">
 <a href="#174359658" class="link-lightbox" data-videoid="174359658" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-174359658" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2016 Queens Cup</h4><h4 class="vs">Finals Promo</h4></a>
</tag>
<tag class=" central guards-polo-club games">
 <a href="#170693576" class="link-lightbox" data-videoid="170693576" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-170693576" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">Dubai vs La Indiana</h4><h4 class="vs">2016 Queens Cup Finals</h4></a>
</tag>
<tag class=" central guards-polo-club games">
 <a href="#170098809" class="link-lightbox" data-videoid="170098809" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-170098809" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">Quarter-finals Recap</h4><h4 class="vs">2016 Queens Cup</h4></a>
</tag>
<tag class=" central guards-polo-club games">
 <a href="#169927505" class="link-lightbox" data-videoid="169927505" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-169927505" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">Semi-finals Dubai vs Zacara</h4><h4 class="vs">2016 Queens CupÂ </h4></a>
</tag>
<tag class=" central guards-polo-club games">
 <a href="#169919645" class="link-lightbox" data-videoid="169919645" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-169919645" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">King Power Foxes vs La Indiana</h4><h4 class="vs">2016 Queens CupÂ Semi-finalsÂ </h4></a>
</tag>
<tag class=" central guards-polo-club featured">
 <a href="#174359809" class="link-lightbox" data-videoid="174359809" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-174359809" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2016 Queens Cup Introduction</h4><h4 class="vs"></h4></a>
</tag>
<tag class=" central guards-polo-club games">
 <a href="#168448946" class="link-lightbox" data-videoid="168448946" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-168448946" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">Dubai VS Zacara</h4><h4 class="vs">2016 Cartier Queen\'s Cup</h4></a>
</tag>
<tag class=" central guards-polo-club games">
 <a href="#167953076" class="link-lightbox" data-videoid="167953076" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-167953076" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">La Bamba VS La Indiana 24th</h4><h4 class="vs">2016 Cartier Queen\'s Cup</h4></a>
</tag>
<tag class=" central guards-polo-club games">
 <a href="#167642622" class="link-lightbox" data-videoid="167642622" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-167642622" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">Sifani VS Zacara</h4><h4 class="vs">2016 Cartier Queens Cup</h4></a>
</tag>
<tag class=" central guards-polo-club games">
 <a href="#167326092" class="link-lightbox" data-videoid="167326092" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-167326092" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">El Remanso VS Sifani</h4><h4 class="vs">Cartier Queen\'s Cup</h4></a>
</tag>
<tag class=" central guards-polo-club games">
 <a href="#167731444" class="link-lightbox" data-videoid="167731444" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-167731444" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">22/5 King Power Foxes VS Talandracas</h4><h4 class="vs">2016 Cartier Queens Cup</h4></a>
</tag>
<tag class=" central guards-polo-club games">
 <a href="#167938734" class="link-lightbox" data-videoid="167938734" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-167938734" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">Dubai VS El Remanso</h4><h4 class="vs">2016 Cartier Queen\'s Cup</h4></a>
</tag>
<tag class=" central guards-polo-club games">
 <a href="#167567613" class="link-lightbox" data-videoid="167567613" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-167567613" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">Valiente vs RH Polo</h4><h4 class="vs">2016 Cartier Queens Cup</h4></a>
</tag>
<tag class=" central guards-polo-club games">
 <a href="#167564233" class="link-lightbox" data-videoid="167564233" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-167564233" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">HB Polo vs Apes Hill</h4><h4 class="vs">2016 Cartier Queens Cup</h4></a>
</tag>
<tag class=" north guards-polo-club events">
 <a href="#167331563" class="link-lightbox" data-videoid="167331563" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-167331563" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">King Power Foxes vs La Indiana</h4><h4 class="vs">2016 Cartier Queens Cup</h4></a>
</tag>
<tag class=" central guards-polo-club games">
 <a href="#167180358" class="link-lightbox" data-videoid="167180358" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-167180358" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">Valiente vs Dubai</h4><h4 class="vs">2016 Cartier Queens Cup</h4></a>
</tag>
<tag class=" central guards-polo-club games">
 <a href="#167139005" class="link-lightbox" data-videoid="167139005" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-167139005" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">Apes Hill vs La Bamba</h4><h4 class="vs">2016 Cartier Queens Cup</h4></a>
</tag>
<tag class=" central guards-polo-club games">
 <a href="#167180524" class="link-lightbox" data-videoid="167180524" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-167180524" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">Talandracas vs HB Polo</h4><h4 class="vs">2016 Cartier Queens Cup</h4></a>
</tag>
<tag class=" central guards-polo-club games">
 <a href="#167011228" class="link-lightbox" data-videoid="167011228" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-167011228" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">RH Polo vs Zacara</h4><h4 class="vs">2016 Cartier Queens Cup</h4></a>
</tag>
<tag class=" central guards-polo-club games">
 <a href="#167731444" class="link-lightbox" data-videoid="167731444" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-167731444" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">King Power Foxes vs Talandracas</h4><h4 class="vs">2016 Cartier Queens Cup</h4></a>
</tag>
<tag class=" north guards-polo-club events">
 <a href="#167326092" class="link-lightbox" data-videoid="167326092" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-167326092" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">El Remanso vs Sifani</h4><h4 class="vs">2016 Cartier Queens Cup</h4></a>
</tag>
<tag class=" central guards-polo-club games">
 <a href="#167953076" class="link-lightbox" data-videoid="167953076" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-167953076" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">La Bamba VS La Indiana</h4><h4 class="vs">2016 Cartier Queens Cup</h4></a>
</tag>
<tag class=" east ellerston-polo-club events">
 <a href="#168184996" class="link-lightbox" data-videoid="168184996" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-168184996" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">Ellerston 2016</h4><h4 class="vs">Autumn Polo Season</h4></a>
</tag>
<tag class=" east tanoira-polo-club profiles">
 <a href="#157750810" class="link-lightbox" data-videoid="157750810" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-157750810" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">All Pro Polo League</h4><h4 class="vs"></h4></a>
</tag>
<tag class=" east desert-palm-polo-club events">
 <a href="#163535831" class="link-lightbox" data-videoid="163535831" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-163535831" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">Gold Cup Experience</h4><h4 class="vs">Dubai 2016</h4></a>
</tag>
<tag class=" north eldorado-polo-club events">
 <a href="#158221484" class="link-lightbox" data-videoid="158221484" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-158221484" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">Eldorado Polo Club</h4><h4 class="vs"></h4></a>
</tag>
<tag class=" east tang-polo-club events">
 <a href="#CYMd9y9XzVQ" class="link-lightbox" data-videoid="CYMd9y9XzVQ" data-videosite="youtube">
 	<div class="img-contain">
	<div style="background: url(http://img.youtube.com/vi/CYMd9y9XzVQ/mqdefault.jpg) no-repeat center center; background-size: cover;"></div>
	</div>
    <br><h4 class="match-name">British Polo Day</h4><h4 class="vs">China 2014</h4></a>
</tag>
<tag class=" east ghantoot-racing-and-polo-club events">
 <a href="#UbJGodUUlj0" class="link-lightbox" data-videoid="UbJGodUUlj0" data-videosite="youtube">
 	<div class="img-contain">
	<div style="background: url(http://img.youtube.com/vi/UbJGodUUlj0/mqdefault.jpg) no-repeat center center; background-size: cover;"></div>
	</div>
    <br><h4 class="match-name">Sentebale Polo</h4><h4 class="vs">Cup 2014</h4></a>
</tag>
<tag class=" central copenhagen-polo-club profiles">
 <a href="#TYMcbXdJa1Q" class="link-lightbox" data-videoid="TYMcbXdJa1Q" data-videosite="youtube">
 	<div class="img-contain">
	<div style="background: url(http://img.youtube.com/vi/TYMcbXdJa1Q/mqdefault.jpg) no-repeat center center; background-size: cover;"></div>
	</div>
    <br><h4 class="match-name">The Player</h4><h4 class="vs">Copenhagen Polo Club</h4></a>
</tag>
<tag class=" west hawaii-polo-club profiles">
 <a href="#NJ6puAUhvUQ" class="link-lightbox" data-videoid="NJ6puAUhvUQ" data-videosite="youtube">
 	<div class="img-contain">
	<div style="background: url(http://img.youtube.com/vi/NJ6puAUhvUQ/mqdefault.jpg) no-repeat center center; background-size: cover;"></div>
	</div>
    <br><h4 class="match-name">The Rider</h4><h4 class="vs">Hawaii Polo Club</h4></a>
</tag>
<tag class=" central maragata-polo-club featured">
 <a href="#2AZNJdMUJxc" class="link-lightbox" data-videoid="2AZNJdMUJxc" data-videosite="youtube">
 	<div class="img-contain">
	<div style="background: url(http://img.youtube.com/vi/2AZNJdMUJxc/mqdefault.jpg) no-repeat center center; background-size: cover;"></div>
	</div>
    <br><h4 class="match-name">Maragata Polo Experience</h4><h4 class="vs">Maragata Zapalla vs Urban Arts</h4></a>
</tag>
<tag class=" east ellerston-polo-club featured">
 <a href="#142716292" class="link-lightbox" data-videoid="142716292" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-142716292" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">Ellerston 2015 Polo Season</h4><h4 class="vs"></h4></a>
</tag>
<tag class=" west santa-barbara-polo-club profiles">
 <a href="#142764142" class="link-lightbox" data-videoid="142764142" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-142764142" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2015 Santa Barbara</h4><h4 class="vs">Polo Club</h4></a>
</tag>
<tag class=" west santa-barbara-polo-club featured">
 <a href="#136267957" class="link-lightbox" data-videoid="136267957" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-136267957" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2015 Heritage Cup Highlights</h4><h4 class="vs"></h4></a>
</tag>
<tag class=" south cowdray-park-polo-club featured">
 <a href="#134500976" class="link-lightbox" data-videoid="134500976" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-134500976" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2015 British Open</h4><h4 class="vs">Finals Higlights</h4></a>
</tag>
<tag class=" south cowdray-park-polo-club games">
 <a href="#134007561" class="link-lightbox" data-videoid="134007561" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-134007561" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2015 British Open Finals</h4><h4 class="vs">King Power Foxes vs UAE</h4></a>
</tag>
<tag class=" south cowdray-park-polo-club featured">
 <a href="#133860509" class="link-lightbox" data-videoid="133860509" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-133860509" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2015 British Open</h4><h4 class="vs">Finals Promo</h4></a>
</tag>
<tag class=" south cowdray-park-polo-club games">
 <a href="#133666093" class="link-lightbox" data-videoid="133666093" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-133666093" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2015 British Open Semi-finals</h4><h4 class="vs">King Power Foxes vs Zacara</h4></a>
</tag>
<tag class=" south cowdray-park-polo-club games">
 <a href="#133669382" class="link-lightbox" data-videoid="133669382" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-133669382" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2015 British Open Semi-finals</h4><h4 class="vs">UAE vs Apes Hill</h4></a>
</tag>
<tag class=" south cowdray-park-polo-club featured">
 <a href="#133498820" class="link-lightbox" data-videoid="133498820" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-133498820" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2015 British Open</h4><h4 class="vs">Â Quarterfinals Recap</h4></a>
</tag>
<tag class=" south cowdray-park-polo-club games">
 <a href="#133476170" class="link-lightbox" data-videoid="133476170" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-133476170" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2015 British Open Quarterfinals</h4><h4 class="vs">King Power Foxes vs RH Polo</h4></a>
</tag>
<tag class=" south cowdray-park-polo-club games">
 <a href="#133478289" class="link-lightbox" data-videoid="133478289" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-133478289" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2015 British Open Quarterfinals</h4><h4 class="vs">UAE vs Salkeld</h4></a>
</tag>
<tag class=" south cowdray-park-polo-club games">
 <a href="#133472900" class="link-lightbox" data-videoid="133472900" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-133472900" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2015 British Open Quarterfinals</h4><h4 class="vs">Zacara vs Dubai</h4></a>
</tag>
<tag class=" south cowdray-park-polo-club games">
 <a href="#133476424" class="link-lightbox" data-videoid="133476424" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-133476424" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2015 British Open Quarterfinals</h4><h4 class="vs">El Remanso vs Apes Hill</h4></a>
</tag>
<tag class=" south cowdray-park-polo-club featured">
 <a href="#133150949" class="link-lightbox" data-videoid="133150949" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-133150949" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2015 British Open</h4><h4 class="vs">Pre-quarterfinals Recap</h4></a>
</tag>
<tag class=" central guards-polo-club games">
 <a href="#131505477" class="link-lightbox" data-videoid="131505477" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-131505477" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2015 Queens Cup Finals</h4><h4 class="vs">King Power Foxes vs. Dubai</h4></a>
</tag>
<tag class=" south guards-polo-club games">
 <a href="#s-aGJtbQdp8" class="link-lightbox" data-videoid="s-aGJtbQdp8" data-videosite="youtube">
 	<div class="img-contain">
	<div style="background: url(http://img.youtube.com/vi/s-aGJtbQdp8/mqdefault.jpg) no-repeat center center; background-size: cover;"></div>
	</div>
    <br><h4 class="match-name">2015 Queens Cup Semi-finals</h4><h4 class="vs">King Power Foxes vs Talandracas</h4></a>
</tag>
<tag class=" west guards-polo-club games">
 <a href="#IDVb3bqhJdw" class="link-lightbox" data-videoid="IDVb3bqhJdw" data-videosite="youtube">
 	<div class="img-contain">
	<div style="background: url(http://img.youtube.com/vi/IDVb3bqhJdw/mqdefault.jpg) no-repeat center center; background-size: cover;"></div>
	</div>
    <br><h4 class="match-name">2015 Queens Cup Semi-finals</h4><h4 class="vs">Dubai vs UAE</h4></a>
</tag>
<tag class=" south international-polo-club featured">
 <a href="#125358856" class="link-lightbox" data-videoid="125358856" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-125358856" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2015 U.S. Open Promo</h4><h4 class="vs"></h4></a>
</tag>
<tag class=" south international-polo-club featured">
 <a href="#122855433" class="link-lightbox" data-videoid="122855433" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-122855433" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2015 Gold Cup Highlights</h4><h4 class="vs"></h4></a>
</tag>
<tag class=" north guards-polo-club featured">
 <a href="#131678798" class="link-lightbox" data-videoid="131678798" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-131678798" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2015 Queens Cup</h4><h4 class="vs">Â Finals Highlights</h4></a>
</tag>
<tag class=" south ellerstina-polo-club featured">
 <a href="#115073839" class="link-lightbox" data-videoid="115073839" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-115073839" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2014 Ellerstina Gold Cup</h4><h4 class="vs"></h4></a>
</tag>
<tag class=" south ellerstina-polo-club featured">
 <a href="#Y4sIMf2wQLA" class="link-lightbox" data-videoid="Y4sIMf2wQLA" data-videosite="youtube">
 	<div class="img-contain">
	<div style="background: url(http://img.youtube.com/vi/Y4sIMf2wQLA/mqdefault.jpg) no-repeat center center; background-size: cover;"></div>
	</div>
    <br><h4 class="match-name">2014 Ellerstina Polo Team</h4><h4 class="vs"></h4></a>
</tag>
<tag class=" south international-polo-club games">
 <a href="#133684062" class="link-lightbox" data-videoid="133684062" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-133684062" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2015 U.S. Gold Cup</h4><h4 class="vs">Audi vs Orchard Hill</h4></a>
</tag>
<tag class=" south international-polo-club games">
 <a href="#133078840" class="link-lightbox" data-videoid="133078840" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-133078840" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2015 U.S. Open Final</h4><h4 class="vs">Valiente vs Orchard Hill</h4></a>
</tag>
<tag class=" south international-polo-club games">
 <a href="#121202096" class="link-lightbox" data-videoid="121202096" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-121202096" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2015 C.V. Whitney Final</h4><h4 class="vs">Orchard Hill vs. Audi</h4></a>
</tag>
<tag class=" east ghantoot-racing-and-polo-club featured">
 <a href="#W_i6DYKFJP4" class="link-lightbox" data-videoid="W_i6DYKFJP4" data-videosite="youtube">
 	<div class="img-contain">
	<div style="background: url(http://img.youtube.com/vi/W_i6DYKFJP4/mqdefault.jpg) no-repeat center center; background-size: cover;"></div>
	</div>
    <br><h4 class="match-name">Duplicate of 2015 President\'s Cup</h4><h4 class="vs">H.H President of the
UAE Polo Cup Highlights</h4></a>
</tag>
<tag class=" east desert-palm-polo-club events">
 <a href="#i2521qkdeJ4" class="link-lightbox" data-videoid="i2521qkdeJ4" data-videosite="youtube">
 	<div class="img-contain">
	<div style="background: url(http://img.youtube.com/vi/i2521qkdeJ4/mqdefault.jpg) no-repeat center center; background-size: cover;"></div>
	</div>
    <br><h4 class="match-name">Duplicate of Cartier Dubai</h4><h4 class="vs">Challenge 2015</h4></a>
</tag>
<tag class=" east desert-palm-polo-club events">
 <a href="#_35_-rOiPmg" class="link-lightbox" data-videoid="_35_-rOiPmg" data-videosite="youtube">
 	<div class="img-contain">
	<div style="background: url(http://img.youtube.com/vi/_35_-rOiPmg/mqdefault.jpg) no-repeat center center; background-size: cover;"></div>
	</div>
    <br><h4 class="match-name">Duplicate of 2015 Silver Cup Dubai</h4><h4 class="vs">Hildon Cup Final Highlights</h4></a>
</tag>
<tag class=" south international-polo-club featured">
 <a href="#120401367" class="link-lightbox" data-videoid="120401367" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-120401367" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2015 Joe Barry Memorial</h4><h4 class="vs">Finals Promo</h4></a>
</tag>
<tag class=" south international-polo-club games">
 <a href="#120438874" class="link-lightbox" data-videoid="120438874" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-120438874" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2015 Joe Barry Memorial</h4><h4 class="vs">Orchard Hill vs Villa Del Lago</h4></a>
</tag>
<tag class=" east ghantoot-racing-and-polo-club events">
 <a href="#W53lelM2NFM" class="link-lightbox" data-videoid="W53lelM2NFM" data-videosite="youtube">
 	<div class="img-contain">
	<div style="background: url(http://img.youtube.com/vi/W53lelM2NFM/mqdefault.jpg) no-repeat center center; background-size: cover;"></div>
	</div>
    <br><h4 class="match-name">Duplicate of 2015 Emirates Open</h4><h4 class="vs">Finals Highlights</h4></a>
</tag>
<tag class=" north la-dolfina events">
 <a href="#85665738" class="link-lightbox" data-videoid="85665738" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-85665738" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2014 La Dolfina Polo Tips</h4><h4 class="vs"></h4></a>
</tag>',
    '[[getResources? &parents=`1` &depth=`1`  &where=`{"template:=":1}` &sortby=`{"publishedon":"DESC"}` &limit=`0` &includeContent=`1`  &tvFilters=`featured==Most popular` &tpl=`item-recent` &includeTVs=`1` &processTVs=`1` &tvPrefix=``]]' => '<tag class=" south international-polo-club featured">
 <a href="#K9Kvs0lUJBA" class="link-lightbox" data-videoid="K9Kvs0lUJBA" data-videosite="youtube">
 	<div class="img-contain">
	<div style="background: url(http://img.youtube.com/vi/K9Kvs0lUJBA/mqdefault.jpg) no-repeat center center; background-size: cover;"></div>
	</div>
    <br><h4 class="match-name">DJI Polo</h4><h4 class="vs"></h4></a>
</tag>
<tag class=" south cowdray-park-polo-club games">
 <a href="#175311549" class="link-lightbox" data-videoid="175311549" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-175311549" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2016 British Open Gold Cup Finals</h4><h4 class="vs"></h4></a>
</tag>
<tag class=" south cowdray-park-polo-club featured">
 <a href="#174661628" class="link-lightbox" data-videoid="174661628" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-174661628" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2016 British Open Gold Cup</h4><h4 class="vs">Introduction</h4></a>
</tag>
<tag class=" central guards-polo-club featured">
 <a href="#174359658" class="link-lightbox" data-videoid="174359658" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-174359658" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2016 Queens Cup</h4><h4 class="vs">Finals Promo</h4></a>
</tag>
<tag class=" central guards-polo-club games">
 <a href="#170693576" class="link-lightbox" data-videoid="170693576" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-170693576" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">Dubai vs La Indiana</h4><h4 class="vs">2016 Queens Cup Finals</h4></a>
</tag>
<tag class=" central guards-polo-club featured">
 <a href="#174359809" class="link-lightbox" data-videoid="174359809" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-174359809" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2016 Queens Cup Introduction</h4><h4 class="vs"></h4></a>
</tag>
<tag class=" east ellerston-polo-club events">
 <a href="#168184996" class="link-lightbox" data-videoid="168184996" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-168184996" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">Ellerston 2016</h4><h4 class="vs">Autumn Polo Season</h4></a>
</tag>
<tag class=" east tanoira-polo-club profiles">
 <a href="#157750810" class="link-lightbox" data-videoid="157750810" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-157750810" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">All Pro Polo League</h4><h4 class="vs"></h4></a>
</tag>
<tag class=" north eldorado-polo-club events">
 <a href="#158221484" class="link-lightbox" data-videoid="158221484" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-158221484" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">Eldorado Polo Club</h4><h4 class="vs"></h4></a>
</tag>
<tag class=" west hawaii-polo-club profiles">
 <a href="#NJ6puAUhvUQ" class="link-lightbox" data-videoid="NJ6puAUhvUQ" data-videosite="youtube">
 	<div class="img-contain">
	<div style="background: url(http://img.youtube.com/vi/NJ6puAUhvUQ/mqdefault.jpg) no-repeat center center; background-size: cover;"></div>
	</div>
    <br><h4 class="match-name">The Rider</h4><h4 class="vs">Hawaii Polo Club</h4></a>
</tag>
<tag class=" west santa-barbara-polo-club profiles">
 <a href="#142764142" class="link-lightbox" data-videoid="142764142" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-142764142" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2015 Santa Barbara</h4><h4 class="vs">Polo Club</h4></a>
</tag>
<tag class=" south cowdray-park-polo-club featured">
 <a href="#134500976" class="link-lightbox" data-videoid="134500976" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-134500976" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2015 British Open</h4><h4 class="vs">Finals Higlights</h4></a>
</tag>
<tag class=" south cowdray-park-polo-club games">
 <a href="#134007561" class="link-lightbox" data-videoid="134007561" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-134007561" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2015 British Open Finals</h4><h4 class="vs">King Power Foxes vs UAE</h4></a>
</tag>
<tag class=" south cowdray-park-polo-club featured">
 <a href="#133860509" class="link-lightbox" data-videoid="133860509" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-133860509" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2015 British Open</h4><h4 class="vs">Finals Promo</h4></a>
</tag>
<tag class=" south cowdray-park-polo-club featured">
 <a href="#133498820" class="link-lightbox" data-videoid="133498820" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-133498820" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2015 British Open</h4><h4 class="vs">Â Quarterfinals Recap</h4></a>
</tag>
<tag class=" central guards-polo-club games">
 <a href="#131505477" class="link-lightbox" data-videoid="131505477" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-131505477" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2015 Queens Cup Finals</h4><h4 class="vs">King Power Foxes vs. Dubai</h4></a>
</tag>
<tag class=" south international-polo-club featured">
 <a href="#125358856" class="link-lightbox" data-videoid="125358856" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-125358856" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2015 U.S. Open Promo</h4><h4 class="vs"></h4></a>
</tag>
<tag class=" south international-polo-club featured">
 <a href="#122855433" class="link-lightbox" data-videoid="122855433" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-122855433" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2015 Gold Cup Highlights</h4><h4 class="vs"></h4></a>
</tag>
<tag class=" north guards-polo-club featured">
 <a href="#131678798" class="link-lightbox" data-videoid="131678798" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-131678798" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2015 Queens Cup</h4><h4 class="vs">Â Finals Highlights</h4></a>
</tag>
<tag class=" east ghantoot-racing-and-polo-club featured">
 <a href="#W_i6DYKFJP4" class="link-lightbox" data-videoid="W_i6DYKFJP4" data-videosite="youtube">
 	<div class="img-contain">
	<div style="background: url(http://img.youtube.com/vi/W_i6DYKFJP4/mqdefault.jpg) no-repeat center center; background-size: cover;"></div>
	</div>
    <br><h4 class="match-name">Duplicate of 2015 President\'s Cup</h4><h4 class="vs">H.H President of the
UAE Polo Cup Highlights</h4></a>
</tag>
<tag class=" south international-polo-club featured">
 <a href="#120401367" class="link-lightbox" data-videoid="120401367" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-120401367" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2015 Joe Barry Memorial</h4><h4 class="vs">Finals Promo</h4></a>
</tag>
<tag class=" south international-polo-club games">
 <a href="#120438874" class="link-lightbox" data-videoid="120438874" data-videosite="vimeo">
 	<div class="img-contain">
	<div class="vimeo-120438874" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2015 Joe Barry Memorial</h4><h4 class="vs">Orchard Hill vs Villa Del Lago</h4></a>
</tag>',
    '[[$doc_footer]]' => '
<!-- javascript !-->

<script type="text/javascript"> 
$(\'.vids a\').simpleLightboxVideo();
</script>
<script src="js/custom.js"></script>

</body>
</html>',
    '[[~188]]' => 'polo-blog/blogposts/post',
    '[[~189]]' => 'polo-blog/blogposts/post2',
    '[[#6.pagetitle:lcase:replace=` ==`]]' => 'northamerica',
    '[[getResources? &parents=`6`&depth=`1`  &sortby=`{"menuindex":"ASC"}` &limit=`0` &tpl=`item` &includeContent=`1` &includeTVs=`1` &processTVs=`1` &tvPrefix=``]]' => '<tag class="northamerica south international-polo-club featured">
  <a href="#125358856" class="link-lightbox" data-videoid="125358856" data-videosite="vimeo">
  	<div class="img-contain">
	<div class="vimeo-125358856" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2015 U.S. Open Promo</h4><h4 class="vs"></h4></a>
</tag>
<tag class="northamerica south international-polo-club featured">
  <a href="#122855433" class="link-lightbox" data-videoid="122855433" data-videosite="vimeo">
  	<div class="img-contain">
	<div class="vimeo-122855433" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2015 Gold Cup Highlights</h4><h4 class="vs"></h4></a>
</tag>
<tag class="northamerica south international-polo-club games">
  <a href="#120438874" class="link-lightbox" data-videoid="120438874" data-videosite="vimeo">
  	<div class="img-contain">
	<div class="vimeo-120438874" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2015 Joe Barry Memorial</h4><h4 class="vs">Orchard Hill vs Villa Del Lago</h4></a>
</tag>
<tag class="northamerica west hawaii-polo-club profiles">
  <a href="#NJ6puAUhvUQ" class="link-lightbox" data-videoid="NJ6puAUhvUQ" data-videosite="youtube">
  	<div class="img-contain">
	<div style="background: url(http://img.youtube.com/vi/NJ6puAUhvUQ/mqdefault.jpg) no-repeat center center; background-size: cover;"></div>
	</div>
    <br><h4 class="match-name">The Rider</h4><h4 class="vs">Hawaii Polo Club</h4></a>
</tag>
<tag class="northamerica west santa-barbara-polo-club featured">
  <a href="#136267957" class="link-lightbox" data-videoid="136267957" data-videosite="vimeo">
  	<div class="img-contain">
	<div class="vimeo-136267957" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2015 Heritage Cup Highlights</h4><h4 class="vs"></h4></a>
</tag>
<tag class="northamerica south international-polo-club games">
  <a href="#133078840" class="link-lightbox" data-videoid="133078840" data-videosite="vimeo">
  	<div class="img-contain">
	<div class="vimeo-133078840" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2015 U.S. Open Final</h4><h4 class="vs">Valiente vs Orchard Hill</h4></a>
</tag>
<tag class="northamerica south international-polo-club games">
  <a href="#121202096" class="link-lightbox" data-videoid="121202096" data-videosite="vimeo">
  	<div class="img-contain">
	<div class="vimeo-121202096" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2015 C.V. Whitney Final</h4><h4 class="vs">Orchard Hill vs. Audi</h4></a>
</tag>
<tag class="northamerica south international-polo-club games">
  <a href="#133684062" class="link-lightbox" data-videoid="133684062" data-videosite="vimeo">
  	<div class="img-contain">
	<div class="vimeo-133684062" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2015 U.S. Gold Cup</h4><h4 class="vs">Audi vs Orchard Hill</h4></a>
</tag>
<tag class="northamerica south international-polo-club featured">
  <a href="#120401367" class="link-lightbox" data-videoid="120401367" data-videosite="vimeo">
  	<div class="img-contain">
	<div class="vimeo-120401367" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2015 Joe Barry Memorial</h4><h4 class="vs">Finals Promo</h4></a>
</tag>
<tag class="northamerica west santa-barbara-polo-club profiles">
  <a href="#142764142" class="link-lightbox" data-videoid="142764142" data-videosite="vimeo">
  	<div class="img-contain">
	<div class="vimeo-142764142" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2015 Santa Barbara</h4><h4 class="vs">Polo Club</h4></a>
</tag>
<tag class="northamerica north eldorado-polo-club events">
  <a href="#158221484" class="link-lightbox" data-videoid="158221484" data-videosite="vimeo">
  	<div class="img-contain">
	<div class="vimeo-158221484" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">Eldorado Polo Club</h4><h4 class="vs"></h4></a>
</tag>
<tag class="northamerica south international-polo-club featured">
  <a href="#K9Kvs0lUJBA" class="link-lightbox" data-videoid="K9Kvs0lUJBA" data-videosite="youtube">
  	<div class="img-contain">
	<div style="background: url(http://img.youtube.com/vi/K9Kvs0lUJBA/mqdefault.jpg) no-repeat center center; background-size: cover;"></div>
	</div>
    <br><h4 class="match-name">DJI Polo</h4><h4 class="vs"></h4></a>
</tag>',
    '[[#7.pagetitle:lcase:replace=` ==`]]' => 'southamerica',
    '[[getResources? &parents=`7`&depth=`1`  &sortby=`{"menuindex":"ASC"}` &limit=`0` &tpl=`item` &includeContent=`1` &includeTVs=`1` &processTVs=`1` &tvPrefix=``]]' => '<tag class="southamerica south ellerstina-polo-club featured">
  <a href="#115073839" class="link-lightbox" data-videoid="115073839" data-videosite="vimeo">
  	<div class="img-contain">
	<div class="vimeo-115073839" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2014 Ellerstina Gold Cup</h4><h4 class="vs"></h4></a>
</tag>
<tag class="southamerica central maragata-polo-club featured">
  <a href="#2AZNJdMUJxc" class="link-lightbox" data-videoid="2AZNJdMUJxc" data-videosite="youtube">
  	<div class="img-contain">
	<div style="background: url(http://img.youtube.com/vi/2AZNJdMUJxc/mqdefault.jpg) no-repeat center center; background-size: cover;"></div>
	</div>
    <br><h4 class="match-name">Maragata Polo Experience</h4><h4 class="vs">Maragata Zapalla vs Urban Arts</h4></a>
</tag>
<tag class="southamerica south ellerstina-polo-club featured">
  <a href="#Y4sIMf2wQLA" class="link-lightbox" data-videoid="Y4sIMf2wQLA" data-videosite="youtube">
  	<div class="img-contain">
	<div style="background: url(http://img.youtube.com/vi/Y4sIMf2wQLA/mqdefault.jpg) no-repeat center center; background-size: cover;"></div>
	</div>
    <br><h4 class="match-name">2014 Ellerstina Polo Team</h4><h4 class="vs"></h4></a>
</tag>
<tag class="southamerica north la-dolfina events">
  <a href="#85665738" class="link-lightbox" data-videoid="85665738" data-videosite="vimeo">
  	<div class="img-contain">
	<div class="vimeo-85665738" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2014 La Dolfina Polo Tips</h4><h4 class="vs"></h4></a>
</tag>
<tag class="southamerica east tanoira-polo-club profiles">
  <a href="#157750810" class="link-lightbox" data-videoid="157750810" data-videosite="vimeo">
  	<div class="img-contain">
	<div class="vimeo-157750810" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">All Pro Polo League</h4><h4 class="vs"></h4></a>
</tag>
<tag class="southamerica west hilario ulloa profiles">
  <a href="#191725822" class="link-lightbox" data-videoid="191725822" data-videosite="vimeo">
  	<div class="img-contain">
	<div class="vimeo-191725822" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">Hilario Ulloa</h4><h4 class="vs">10 Goals</h4></a>
</tag>',
    '[[getResources? &parents=`9`&depth=`1`  &sortby=`{"menuindex":"ASC"}` &limit=`0` &tpl=`item` &includeContent=`1` &includeTVs=`1` &processTVs=`1` &tvPrefix=``]]' => '',
    '[[#8.pagetitle:lcase:replace=` ==`]]' => 'europe',
    '[[getResources? &parents=`8`&depth=`1`  &sortby=`{"menuindex":"ASC"}` &limit=`0` &tpl=`item` &includeContent=`1` &includeTVs=`1` &processTVs=`1` &tvPrefix=``]]' => '<tag class="europe south cowdray-park-polo-club featured">
  <a href="#133498820" class="link-lightbox" data-videoid="133498820" data-videosite="vimeo">
  	<div class="img-contain">
	<div class="vimeo-133498820" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2015 British Open</h4><h4 class="vs">Â Quarterfinals Recap</h4></a>
</tag>
<tag class="europe south cowdray-park-polo-club games">
  <a href="#133476424" class="link-lightbox" data-videoid="133476424" data-videosite="vimeo">
  	<div class="img-contain">
	<div class="vimeo-133476424" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2015 British Open Quarterfinals</h4><h4 class="vs">El Remanso vs Apes Hill</h4></a>
</tag>
<tag class="europe south cowdray-park-polo-club games">
  <a href="#133472900" class="link-lightbox" data-videoid="133472900" data-videosite="vimeo">
  	<div class="img-contain">
	<div class="vimeo-133472900" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2015 British Open Quarterfinals</h4><h4 class="vs">Zacara vs Dubai</h4></a>
</tag>
<tag class="europe south cowdray-park-polo-club games">
  <a href="#173384466" class="link-lightbox" data-videoid="173384466" data-videosite="vimeo">
  	<div class="img-contain">
	<div class="vimeo-173384466" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">07/03 Britannia El Remanso vs Murus Sanctus</h4><h4 class="vs">2016 British Open</h4></a>
</tag>
<tag class="europe south cowdray-park-polo-club games">
  <a href="#174805828" class="link-lightbox" data-videoid="174805828" data-videosite="vimeo">
  	<div class="img-contain">
	<div class="vimeo-174805828" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2016 British Open Quarter Final Recap</h4><h4 class="vs"></h4></a>
</tag>
<tag class="europe south cowdray-park-polo-club games">
  <a href="#174793966" class="link-lightbox" data-videoid="174793966" data-videosite="vimeo">
  	<div class="img-contain">
	<div class="vimeo-174793966" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2016 British Open Pre-Quarter Final Recap</h4><h4 class="vs"></h4></a>
</tag>
<tag class="europe south cowdray-park-polo-club featured">
  <a href="#174733471" class="link-lightbox" data-videoid="174733471" data-videosite="vimeo">
  	<div class="img-contain">
	<div class="vimeo-174733471" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2016 British Open Finals Promo</h4><h4 class="vs"></h4></a>
</tag>
<tag class="europe south cowdray-park-polo-club games">
  <a href="#174722845" class="link-lightbox" data-videoid="174722845" data-videosite="vimeo">
  	<div class="img-contain">
	<div class="vimeo-174722845" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">07/13 King Power vs Valiente</h4><h4 class="vs">2016 British Open Semi-final</h4></a>
</tag>
<tag class="europe south cowdray-park-polo-club featured">
  <a href="#174661628" class="link-lightbox" data-videoid="174661628" data-videosite="vimeo">
  	<div class="img-contain">
	<div class="vimeo-174661628" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2016 British Open Gold Cup</h4><h4 class="vs">Introduction</h4></a>
</tag>
<tag class="europe south cowdray-park-polo-club games">
  <a href="#174440002" class="link-lightbox" data-videoid="174440002" data-videosite="vimeo">
  	<div class="img-contain">
	<div class="vimeo-174440002" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">07/10 King Power vs El Remanso</h4><h4 class="vs">2016 British Open Quarter Finals</h4></a>
</tag>
<tag class="europe south cowdray-park-polo-club games">
  <a href="#174182658" class="link-lightbox" data-videoid="174182658" data-videosite="vimeo">
  	<div class="img-contain">
	<div class="vimeo-174182658" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">07/09 Clarke and Green vs Valiente</h4><h4 class="vs">2016 British Open Quarter Finals</h4></a>
</tag>
<tag class="europe south cowdray-park-polo-club games">
  <a href="#174136115" class="link-lightbox" data-videoid="174136115" data-videosite="vimeo">
  	<div class="img-contain">
	<div class="vimeo-174136115" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">07/09 Zacara vs HB Polo</h4><h4 class="vs">2016 British Open Quarter Final</h4></a>
</tag>
<tag class="europe south cowdray-park-polo-club games">
  <a href="#174081451" class="link-lightbox" data-videoid="174081451" data-videosite="vimeo">
  	<div class="img-contain">
	<div class="vimeo-174081451" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">07/06 La Indiana vs Murus Sanctus</h4><h4 class="vs">2016 British OpenÂ </h4></a>
</tag>
<tag class="europe south cowdray-park-polo-club games">
  <a href="#173996540" class="link-lightbox" data-videoid="173996540" data-videosite="vimeo">
  	<div class="img-contain">
	<div class="vimeo-173996540" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">07/06 Valiente vs El Remanso</h4><h4 class="vs">2016 British Open</h4></a>
</tag>
<tag class="europe south cowdray-park-polo-club games">
  <a href="#173380859" class="link-lightbox" data-videoid="173380859" data-videosite="vimeo">
  	<div class="img-contain">
	<div class="vimeo-173380859" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">07/02 Zacara vs HB Polo</h4><h4 class="vs">2016 British Open</h4></a>
</tag>
<tag class="europe south cowdray-park-polo-club games">
  <a href="#173043122" class="link-lightbox" data-videoid="173043122" data-videosite="vimeo">
  	<div class="img-contain">
	<div class="vimeo-173043122" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">06/30 Valiente vs Murus Sanctus</h4><h4 class="vs">2016 British Open</h4></a>
</tag>
<tag class="europe south cowdray-park-polo-club games">
  <a href="#173040928" class="link-lightbox" data-videoid="173040928" data-videosite="vimeo">
  	<div class="img-contain">
	<div class="vimeo-173040928" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">06/29 Cowdray Vikings vs El Remanso</h4><h4 class="vs">2016 British Open</h4></a>
</tag>
<tag class="europe south cowdray-park-polo-club games">
  <a href="#173038119" class="link-lightbox" data-videoid="173038119" data-videosite="vimeo">
  	<div class="img-contain">
	<div class="vimeo-173038119" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">06/29 HB Polo vs Clarke & Green</h4><h4 class="vs">2016 British Open</h4></a>
</tag>
<tag class="europe south cowdray-park-polo-club games">
  <a href="#172869892" class="link-lightbox" data-videoid="172869892" data-videosite="vimeo">
  	<div class="img-contain">
	<div class="vimeo-172869892" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">06/29 Apes Hill vs La Bamba De Areco</h4><h4 class="vs">2016 British OpenÂ </h4></a>
</tag>
<tag class="europe south cowdray-park-polo-club games">
  <a href="#172338259" class="link-lightbox" data-videoid="172338259" data-videosite="vimeo">
  	<div class="img-contain">
	<div class="vimeo-172338259" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">06/26 Murus Sanctus vs Cowdray Vikings</h4><h4 class="vs">2016 British Open</h4></a>
</tag>
<tag class="europe south cowdray-park-polo-club games">
  <a href="#172697733" class="link-lightbox" data-videoid="172697733" data-videosite="vimeo">
  	<div class="img-contain">
	<div class="vimeo-172697733" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">06/28 King Power Foxes vs Zacara</h4><h4 class="vs">2016 British OpenÂ </h4></a>
</tag>
<tag class="europe south cowdray-park-polo-club games">
  <a href="#171844797" class="link-lightbox" data-videoid="171844797" data-videosite="vimeo">
  	<div class="img-contain">
	<div class="vimeo-171844797" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">Talandracas vs HB Polo</h4><h4 class="vs">2016 British Open</h4></a>
</tag>
<tag class="europe central guards-polo-club games">
  <a href="#170693576" class="link-lightbox" data-videoid="170693576" data-videosite="vimeo">
  	<div class="img-contain">
	<div class="vimeo-170693576" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">Dubai vs La Indiana</h4><h4 class="vs">2016 Queens Cup Finals</h4></a>
</tag>
<tag class="europe central guards-polo-club games">
  <a href="#167953076" class="link-lightbox" data-videoid="167953076" data-videosite="vimeo">
  	<div class="img-contain">
	<div class="vimeo-167953076" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">La Bamba VS La Indiana</h4><h4 class="vs">2016 Cartier Queens Cup</h4></a>
</tag>
<tag class="europe north guards-polo-club events">
  <a href="#167326092" class="link-lightbox" data-videoid="167326092" data-videosite="vimeo">
  	<div class="img-contain">
	<div class="vimeo-167326092" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">El Remanso vs Sifani</h4><h4 class="vs">2016 Cartier Queens Cup</h4></a>
</tag>
<tag class="europe central guards-polo-club games">
  <a href="#167731444" class="link-lightbox" data-videoid="167731444" data-videosite="vimeo">
  	<div class="img-contain">
	<div class="vimeo-167731444" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">King Power Foxes vs Talandracas</h4><h4 class="vs">2016 Cartier Queens Cup</h4></a>
</tag>
<tag class="europe south cowdray-park-polo-club featured">
  <a href="#133150949" class="link-lightbox" data-videoid="133150949" data-videosite="vimeo">
  	<div class="img-contain">
	<div class="vimeo-133150949" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2015 British Open</h4><h4 class="vs">Pre-quarterfinals Recap</h4></a>
</tag>
<tag class="europe central copenhagen-polo-club profiles">
  <a href="#TYMcbXdJa1Q" class="link-lightbox" data-videoid="TYMcbXdJa1Q" data-videosite="youtube">
  	<div class="img-contain">
	<div style="background: url(http://img.youtube.com/vi/TYMcbXdJa1Q/mqdefault.jpg) no-repeat center center; background-size: cover;"></div>
	</div>
    <br><h4 class="match-name">The Player</h4><h4 class="vs">Copenhagen Polo Club</h4></a>
</tag>
<tag class="europe south cowdray-park-polo-club games">
  <a href="#133476170" class="link-lightbox" data-videoid="133476170" data-videosite="vimeo">
  	<div class="img-contain">
	<div class="vimeo-133476170" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2015 British Open Quarterfinals</h4><h4 class="vs">King Power Foxes vs RH Polo</h4></a>
</tag>
<tag class="europe south cowdray-park-polo-club games">
  <a href="#133478289" class="link-lightbox" data-videoid="133478289" data-videosite="vimeo">
  	<div class="img-contain">
	<div class="vimeo-133478289" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2015 British Open Quarterfinals</h4><h4 class="vs">UAE vs Salkeld</h4></a>
</tag>
<tag class="europe central guards-polo-club games">
  <a href="#131505477" class="link-lightbox" data-videoid="131505477" data-videosite="vimeo">
  	<div class="img-contain">
	<div class="vimeo-131505477" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2015 Queens Cup Finals</h4><h4 class="vs">King Power Foxes vs. Dubai</h4></a>
</tag>
<tag class="europe north guards-polo-club featured">
  <a href="#131678798" class="link-lightbox" data-videoid="131678798" data-videosite="vimeo">
  	<div class="img-contain">
	<div class="vimeo-131678798" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2015 Queens Cup</h4><h4 class="vs">Â Finals Highlights</h4></a>
</tag>
<tag class="europe south guards-polo-club games">
  <a href="#s-aGJtbQdp8" class="link-lightbox" data-videoid="s-aGJtbQdp8" data-videosite="youtube">
  	<div class="img-contain">
	<div style="background: url(http://img.youtube.com/vi/s-aGJtbQdp8/mqdefault.jpg) no-repeat center center; background-size: cover;"></div>
	</div>
    <br><h4 class="match-name">2015 Queens Cup Semi-finals</h4><h4 class="vs">King Power Foxes vs Talandracas</h4></a>
</tag>
<tag class="europe west guards-polo-club games">
  <a href="#IDVb3bqhJdw" class="link-lightbox" data-videoid="IDVb3bqhJdw" data-videosite="youtube">
  	<div class="img-contain">
	<div style="background: url(http://img.youtube.com/vi/IDVb3bqhJdw/mqdefault.jpg) no-repeat center center; background-size: cover;"></div>
	</div>
    <br><h4 class="match-name">2015 Queens Cup Semi-finals</h4><h4 class="vs">Dubai vs UAE</h4></a>
</tag>
<tag class="europe south cowdray-park-polo-club games">
  <a href="#133666093" class="link-lightbox" data-videoid="133666093" data-videosite="vimeo">
  	<div class="img-contain">
	<div class="vimeo-133666093" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2015 British Open Semi-finals</h4><h4 class="vs">King Power Foxes vs Zacara</h4></a>
</tag>
<tag class="europe south cowdray-park-polo-club games">
  <a href="#133669382" class="link-lightbox" data-videoid="133669382" data-videosite="vimeo">
  	<div class="img-contain">
	<div class="vimeo-133669382" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2015 British Open Semi-finals</h4><h4 class="vs">UAE vs Apes Hill</h4></a>
</tag>
<tag class="europe south cowdray-park-polo-club games">
  <a href="#134007561" class="link-lightbox" data-videoid="134007561" data-videosite="vimeo">
  	<div class="img-contain">
	<div class="vimeo-134007561" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2015 British Open Finals</h4><h4 class="vs">King Power Foxes vs UAE</h4></a>
</tag>
<tag class="europe south cowdray-park-polo-club featured">
  <a href="#133860509" class="link-lightbox" data-videoid="133860509" data-videosite="vimeo">
  	<div class="img-contain">
	<div class="vimeo-133860509" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2015 British Open</h4><h4 class="vs">Finals Promo</h4></a>
</tag>
<tag class="europe south cowdray-park-polo-club featured">
  <a href="#134500976" class="link-lightbox" data-videoid="134500976" data-videosite="vimeo">
  	<div class="img-contain">
	<div class="vimeo-134500976" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2015 British Open</h4><h4 class="vs">Finals Higlights</h4></a>
</tag>
<tag class="europe central guards-polo-club games">
  <a href="#167011228" class="link-lightbox" data-videoid="167011228" data-videosite="vimeo">
  	<div class="img-contain">
	<div class="vimeo-167011228" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">RH Polo vs Zacara</h4><h4 class="vs">2016 Cartier Queens Cup</h4></a>
</tag>
<tag class="europe central guards-polo-club games">
  <a href="#167139005" class="link-lightbox" data-videoid="167139005" data-videosite="vimeo">
  	<div class="img-contain">
	<div class="vimeo-167139005" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">Apes Hill vs La Bamba</h4><h4 class="vs">2016 Cartier Queens Cup</h4></a>
</tag>
<tag class="europe central guards-polo-club games">
  <a href="#167180358" class="link-lightbox" data-videoid="167180358" data-videosite="vimeo">
  	<div class="img-contain">
	<div class="vimeo-167180358" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">Valiente vs Dubai</h4><h4 class="vs">2016 Cartier Queens Cup</h4></a>
</tag>
<tag class="europe central guards-polo-club games">
  <a href="#167180524" class="link-lightbox" data-videoid="167180524" data-videosite="vimeo">
  	<div class="img-contain">
	<div class="vimeo-167180524" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">Talandracas vs HB Polo</h4><h4 class="vs">2016 Cartier Queens Cup</h4></a>
</tag>
<tag class="europe north guards-polo-club events">
  <a href="#167331563" class="link-lightbox" data-videoid="167331563" data-videosite="vimeo">
  	<div class="img-contain">
	<div class="vimeo-167331563" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">King Power Foxes vs La Indiana</h4><h4 class="vs">2016 Cartier Queens Cup</h4></a>
</tag>
<tag class="europe central guards-polo-club games">
  <a href="#167564233" class="link-lightbox" data-videoid="167564233" data-videosite="vimeo">
  	<div class="img-contain">
	<div class="vimeo-167564233" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">HB Polo vs Apes Hill</h4><h4 class="vs">2016 Cartier Queens Cup</h4></a>
</tag>
<tag class="europe central guards-polo-club games">
  <a href="#167567613" class="link-lightbox" data-videoid="167567613" data-videosite="vimeo">
  	<div class="img-contain">
	<div class="vimeo-167567613" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">Valiente vs RH Polo</h4><h4 class="vs">2016 Cartier Queens Cup</h4></a>
</tag>
<tag class="europe central guards-polo-club games">
  <a href="#167938734" class="link-lightbox" data-videoid="167938734" data-videosite="vimeo">
  	<div class="img-contain">
	<div class="vimeo-167938734" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">Dubai VS El Remanso</h4><h4 class="vs">2016 Cartier Queen\'s Cup</h4></a>
</tag>
<tag class="europe central guards-polo-club games">
  <a href="#167731444" class="link-lightbox" data-videoid="167731444" data-videosite="vimeo">
  	<div class="img-contain">
	<div class="vimeo-167731444" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">22/5 King Power Foxes VS Talandracas</h4><h4 class="vs">2016 Cartier Queens Cup</h4></a>
</tag>
<tag class="europe central guards-polo-club games">
  <a href="#167326092" class="link-lightbox" data-videoid="167326092" data-videosite="vimeo">
  	<div class="img-contain">
	<div class="vimeo-167326092" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">El Remanso VS Sifani</h4><h4 class="vs">Cartier Queen\'s Cup</h4></a>
</tag>
<tag class="europe central guards-polo-club games">
  <a href="#167642622" class="link-lightbox" data-videoid="167642622" data-videosite="vimeo">
  	<div class="img-contain">
	<div class="vimeo-167642622" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">Sifani VS Zacara</h4><h4 class="vs">2016 Cartier Queens Cup</h4></a>
</tag>
<tag class="europe central guards-polo-club games">
  <a href="#167953076" class="link-lightbox" data-videoid="167953076" data-videosite="vimeo">
  	<div class="img-contain">
	<div class="vimeo-167953076" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">La Bamba VS La Indiana 24th</h4><h4 class="vs">2016 Cartier Queen\'s Cup</h4></a>
</tag>
<tag class="europe central guards-polo-club games">
  <a href="#168448946" class="link-lightbox" data-videoid="168448946" data-videosite="vimeo">
  	<div class="img-contain">
	<div class="vimeo-168448946" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">Dubai VS Zacara</h4><h4 class="vs">2016 Cartier Queen\'s Cup</h4></a>
</tag>
<tag class="europe central guards-polo-club games">
  <a href="#169927505" class="link-lightbox" data-videoid="169927505" data-videosite="vimeo">
  	<div class="img-contain">
	<div class="vimeo-169927505" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">Semi-finals Dubai vs Zacara</h4><h4 class="vs">2016 Queens CupÂ </h4></a>
</tag>
<tag class="europe central guards-polo-club games">
  <a href="#169919645" class="link-lightbox" data-videoid="169919645" data-videosite="vimeo">
  	<div class="img-contain">
	<div class="vimeo-169919645" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">King Power Foxes vs La Indiana</h4><h4 class="vs">2016 Queens CupÂ Semi-finalsÂ </h4></a>
</tag>
<tag class="europe central guards-polo-club games">
  <a href="#170098809" class="link-lightbox" data-videoid="170098809" data-videosite="vimeo">
  	<div class="img-contain">
	<div class="vimeo-170098809" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">Quarter-finals Recap</h4><h4 class="vs">2016 Queens Cup</h4></a>
</tag>
<tag class="europe south cowdray-park-polo-club games">
  <a href="#171906979" class="link-lightbox" data-videoid="171906979" data-videosite="vimeo">
  	<div class="img-contain">
	<div class="vimeo-171906979" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">Valiente vs Cowdray Vikings</h4><h4 class="vs">2016 British open</h4></a>
</tag>
<tag class="europe south cowdray-park-polo-club games">
  <a href="#172092615" class="link-lightbox" data-videoid="172092615" data-videosite="vimeo">
  	<div class="img-contain">
	<div class="vimeo-172092615" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">Zacara vs RH Polo</h4><h4 class="vs">2016 British Open</h4></a>
</tag>
<tag class="europe central cowdray-park-polo-club games">
  <a href="#172092603" class="link-lightbox" data-videoid="172092603" data-videosite="vimeo">
  	<div class="img-contain">
	<div class="vimeo-172092603" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">Apes Hill vs Clarke&Greene</h4><h4 class="vs">2016 British Open</h4></a>
</tag>
<tag class="europe south cowdray-park-polo-club games">
  <a href="#172274886" class="link-lightbox" data-videoid="172274886" data-videosite="vimeo">
  	<div class="img-contain">
	<div class="vimeo-172274886" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">06/25 Clarke and Greene vs RH Polo</h4><h4 class="vs">2016 British Open</h4></a>
</tag>
<tag class="europe south cowdray-park-polo-club games">
  <a href="#172318944" class="link-lightbox" data-videoid="172318944" data-videosite="vimeo">
  	<div class="img-contain">
	<div class="vimeo-172318944" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">06/25 Talandracas vs Apes Hill</h4><h4 class="vs">2016 British Open</h4></a>
</tag>
<tag class="europe south cowdray-park-polo-club games">
  <a href="#172334336" class="link-lightbox" data-videoid="172334336" data-videosite="vimeo">
  	<div class="img-contain">
	<div class="vimeo-172334336" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">06/26 La Bamba de Areco vs HB Polo</h4><h4 class="vs">2016 British Open</h4></a>
</tag>
<tag class="europe south cowdray-park-polo-club games">
  <a href="#172465164" class="link-lightbox" data-videoid="172465164" data-videosite="vimeo">
  	<div class="img-contain">
	<div class="vimeo-172465164" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">06/27 La Indiana vs Valiente</h4><h4 class="vs">2016 British Open</h4></a>
</tag>
<tag class="europe south cowdray-park-polo-club games">
  <a href="#173377177" class="link-lightbox" data-videoid="173377177" data-videosite="vimeo">
  	<div class="img-contain">
	<div class="vimeo-173377177" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">07/02 King Power Foxes vs Clarke & Green</h4><h4 class="vs">2016 British OpenÂ </h4></a>
</tag>
<tag class="europe south cowdray-park-polo-club games">
  <a href="#173448941" class="link-lightbox" data-videoid="173448941" data-videosite="vimeo">
  	<div class="img-contain">
	<div class="vimeo-173448941" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">07/03 Cowdray Vikings vs La Indiana</h4><h4 class="vs">2016 British Open</h4></a>
</tag>
<tag class="europe south cowdray-park-polo-club games">
  <a href="#173653268" class="link-lightbox" data-videoid="173653268" data-videosite="vimeo">
  	<div class="img-contain">
	<div class="vimeo-173653268" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">07/05 Zacara vs Apes Hill</h4><h4 class="vs">2016 British OpenÂ </h4></a>
</tag>
<tag class="europe south cowdray-park-polo-club games">
  <a href="#173965653" class="link-lightbox" data-videoid="173965653" data-videosite="vimeo">
  	<div class="img-contain">
	<div class="vimeo-173965653" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">07/05 King Power vs Talandracs</h4><h4 class="vs">2016 British Open</h4></a>
</tag>
<tag class="europe central guards-polo-club featured">
  <a href="#174359658" class="link-lightbox" data-videoid="174359658" data-videosite="vimeo">
  	<div class="img-contain">
	<div class="vimeo-174359658" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2016 Queens Cup</h4><h4 class="vs">Finals Promo</h4></a>
</tag>
<tag class="europe south cowdray-park-polo-club games">
  <a href="#174602074" class="link-lightbox" data-videoid="174602074" data-videosite="vimeo">
  	<div class="img-contain">
	<div class="vimeo-174602074" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">07/10 Talandracas vs La Indiana</h4><h4 class="vs">2016 British Open Quarter Finals</h4></a>
</tag>
<tag class="europe central guards-polo-club featured">
  <a href="#174359809" class="link-lightbox" data-videoid="174359809" data-videosite="vimeo">
  	<div class="img-contain">
	<div class="vimeo-174359809" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2016 Queens Cup Introduction</h4><h4 class="vs"></h4></a>
</tag>
<tag class="europe south cowdray-park-polo-club games">
  <a href="#174669425" class="link-lightbox" data-videoid="174669425" data-videosite="vimeo">
  	<div class="img-contain">
	<div class="vimeo-174669425" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">07/13 La Indiana vs Zacara</h4><h4 class="vs">2016 British Open Semi-final</h4></a>
</tag>
<tag class="europe south cowdray-park-polo-club games">
  <a href="#175311549" class="link-lightbox" data-videoid="175311549" data-videosite="vimeo">
  	<div class="img-contain">
	<div class="vimeo-175311549" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">2016 British Open Gold Cup Finals</h4><h4 class="vs"></h4></a>
</tag>
<tag class="europe north cowdray-park-polo-club profiles">
  <a href="#172747778" class="link-lightbox" data-videoid="172747778" data-videosite="vimeo">
  	<div class="img-contain">
	<div class="vimeo-172747778" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">PoloChannel Stories: Kian Hall</h4><h4 class="vs"></h4></a>
</tag>',
    '[[#11.pagetitle:lcase:replace=` ==`]]' => 'asia',
    '[[getResources? &parents=`11`&depth=`1`  &sortby=`{"menuindex":"ASC"}` &limit=`0` &tpl=`item` &includeContent=`1` &includeTVs=`1` &processTVs=`1` &tvPrefix=``]]' => '<tag class="asia east tang-polo-club events">
  <a href="#CYMd9y9XzVQ" class="link-lightbox" data-videoid="CYMd9y9XzVQ" data-videosite="youtube">
  	<div class="img-contain">
	<div style="background: url(http://img.youtube.com/vi/CYMd9y9XzVQ/mqdefault.jpg) no-repeat center center; background-size: cover;"></div>
	</div>
    <br><h4 class="match-name">British Polo Day</h4><h4 class="vs">China 2014</h4></a>
</tag>',
    '[[#10.pagetitle:lcase:replace=` ==`]]' => 'australia',
    '[[getResources? &parents=`10`&depth=`1`  &sortby=`{"menuindex":"ASC"}` &limit=`0` &tpl=`item` &includeContent=`1` &includeTVs=`1` &processTVs=`1` &tvPrefix=``]]' => '<tag class="australia east ellerston-polo-club featured">
  <a href="#142716292" class="link-lightbox" data-videoid="142716292" data-videosite="vimeo">
  	<div class="img-contain">
	<div class="vimeo-142716292" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">Ellerston 2015 Polo Season</h4><h4 class="vs"></h4></a>
</tag>
<tag class="australia east ellerston-polo-club events">
  <a href="#168184996" class="link-lightbox" data-videoid="168184996" data-videosite="vimeo">
  	<div class="img-contain">
	<div class="vimeo-168184996" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">Ellerston 2016</h4><h4 class="vs">Autumn Polo Season</h4></a>
</tag>',
    '[[#79.pagetitle:lcase:replace=` ==`]]' => 'middleeast',
    '[[getResources? &parents=`79`&depth=`1`  &sortby=`{"menuindex":"ASC"}` &limit=`0` &tpl=`item` &includeContent=`1` &includeTVs=`1` &processTVs=`1` &tvPrefix=``]]' => '<tag class="middleeast east ghantoot-racing-and-polo-club featured">
  <a href="#W_i6DYKFJP4" class="link-lightbox" data-videoid="W_i6DYKFJP4" data-videosite="youtube">
  	<div class="img-contain">
	<div style="background: url(http://img.youtube.com/vi/W_i6DYKFJP4/mqdefault.jpg) no-repeat center center; background-size: cover;"></div>
	</div>
    <br><h4 class="match-name">Duplicate of 2015 President\'s Cup</h4><h4 class="vs">H.H President of the
UAE Polo Cup Highlights</h4></a>
</tag>
<tag class="middleeast east ghantoot-racing-and-polo-club events">
  <a href="#W53lelM2NFM" class="link-lightbox" data-videoid="W53lelM2NFM" data-videosite="youtube">
  	<div class="img-contain">
	<div style="background: url(http://img.youtube.com/vi/W53lelM2NFM/mqdefault.jpg) no-repeat center center; background-size: cover;"></div>
	</div>
    <br><h4 class="match-name">Duplicate of 2015 Emirates Open</h4><h4 class="vs">Finals Highlights</h4></a>
</tag>
<tag class="middleeast east desert-palm-polo-club events">
  <a href="#_35_-rOiPmg" class="link-lightbox" data-videoid="_35_-rOiPmg" data-videosite="youtube">
  	<div class="img-contain">
	<div style="background: url(http://img.youtube.com/vi/_35_-rOiPmg/mqdefault.jpg) no-repeat center center; background-size: cover;"></div>
	</div>
    <br><h4 class="match-name">Duplicate of 2015 Silver Cup Dubai</h4><h4 class="vs">Hildon Cup Final Highlights</h4></a>
</tag>
<tag class="middleeast east desert-palm-polo-club events">
  <a href="#163535831" class="link-lightbox" data-videoid="163535831" data-videosite="vimeo">
  	<div class="img-contain">
	<div class="vimeo-163535831" data-vimeo="vimeo-img"></div>
	</div>
    <br><h4 class="match-name">Gold Cup Experience</h4><h4 class="vs">Dubai 2016</h4></a>
</tag>
<tag class="middleeast east desert-palm-polo-club events">
  <a href="#i2521qkdeJ4" class="link-lightbox" data-videoid="i2521qkdeJ4" data-videosite="youtube">
  	<div class="img-contain">
	<div style="background: url(http://img.youtube.com/vi/i2521qkdeJ4/mqdefault.jpg) no-repeat center center; background-size: cover;"></div>
	</div>
    <br><h4 class="match-name">Duplicate of Cartier Dubai</h4><h4 class="vs">Challenge 2015</h4></a>
</tag>
<tag class="middleeast east ghantoot-racing-and-polo-club events">
  <a href="#UbJGodUUlj0" class="link-lightbox" data-videoid="UbJGodUUlj0" data-videosite="youtube">
  	<div class="img-contain">
	<div style="background: url(http://img.youtube.com/vi/UbJGodUUlj0/mqdefault.jpg) no-repeat center center; background-size: cover;"></div>
	</div>
    <br><h4 class="match-name">Sentebale Polo</h4><h4 class="vs">Cup 2014</h4></a>
</tag>',
  ),
  'sourceCache' => 
  array (
    'modChunk' => 
    array (
      'doc_head' => 
      array (
        'fields' => 
        array (
          'id' => 1,
          'source' => 1,
          'property_preprocess' => false,
          'name' => 'doc_head',
          'description' => '',
          'editor_type' => 0,
          'category' => 3,
          'cache_type' => 0,
          'snippet' => '<!doctype html>
<head>
<meta charset="UTF-8">
<title>Polo Channel</title>
<link href="[[++site_url]]/css/main.css" rel="stylesheet" type="text/css" />
<link rel="icon" type="image/png" href="http://polochannel.com/favicon.png">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script type="text/javascript" src="[[++site_url]]/js/lightbox.min.js"></script>
<script type="text/javascript" src="[[++site_url]]/js/touchSwipe.min.js"></script>
<meta name="description" content="[[*description]]">
        <meta name="viewport" content="width=device-width,initial-scale=1.0">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

        <!-- THIS IS NEEDED FOR FURL -->
        <base href="[[++site_url]]" />
        
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width,initial-scale=1.0">

        <!-- Facebook OpenGraph Data -->
        <meta property="og:title" content="[[++site_name]]" />
        <meta property="og:type" content="website" />
        <!-- If it\'s an article:
        <meta property="og:type" content="article" />
        <meta property="article:published_time" content = "" />
        -->
        <meta property="og:description" content="[[*description]]" />        
        <meta property="og:url" content="[[++site_url]]"/>
        <meta property="og:image" content="[[++site_url]][[++social_image]]" />
        <meta property="og:site_name" content="[[++site_name]]" />


<script src="js/respond.min.js"></script>

<script src="[[++site_url]]/js/mapdata.js"></script>
<script src="[[++site_url]]/js/worldmap.js"></script>
<script src="[[++site_url]]/js/jquery.fastLiveFilter.js"></script>
<script src="[[++site_url]]/js/jquery.countdown.min.js"></script>
<script src="[[++site_url]]/js/modernizr.custom.js"></script>
<link rel="stylesheet" type="text/css" href="css/lightbox.min.css">
<meta name="google-site-verification" content="lyzPX5GCX9Yi9NxiLhZGlEnTRGSsqA6xKj1sbBaz_Yo" />
<link href="[[++site_url]]/css/normalize.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<script>
  (function(i,s,o,g,r,a,m){i[\'GoogleAnalyticsObject\']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,\'script\',\'https://www.google-analytics.com/analytics.js\',\'ga\');

  ga(\'create\', \'UA-82621934-1\', \'auto\');
  ga(\'send\', \'pageview\');

</script>
 <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
 <![endif]-->
<script>
    $(function() {
        $(\'#search_input\').fastLiveFilter(\'.search_list\');
        $(\'#search_input_mobile\').fastLiveFilter(\'.search_list\');
    });

       $(window).load(function() {
      $(\'#loading\').fadeOut("slow");
      hideEmpty();
    });

    function hideEmpty() {
$(".vids").each(function(){

 if($(this).children(\'tag:visible\').length == 0) {
   $(this.parentNode.parentNode).hide(); 
}
else {
 $(this.parentNode.parentNode).show();
}
});

}
</script>



<!--[if lt IE 9]>
<script>
document.createElement(\'video\');
</script>
<![endif]-->

</head>',
          'locked' => false,
          'properties' => 
          array (
          ),
          'static' => true,
          'static_file' => 'assets/chunks/layout/doc_head.html',
          'content' => '<!doctype html>
<head>
<meta charset="UTF-8">
<title>Polo Channel</title>
<link href="[[++site_url]]/css/main.css" rel="stylesheet" type="text/css" />
<link rel="icon" type="image/png" href="http://polochannel.com/favicon.png">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script type="text/javascript" src="[[++site_url]]/js/lightbox.min.js"></script>
<script type="text/javascript" src="[[++site_url]]/js/touchSwipe.min.js"></script>
<meta name="description" content="[[*description]]">
        <meta name="viewport" content="width=device-width,initial-scale=1.0">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

        <!-- THIS IS NEEDED FOR FURL -->
        <base href="[[++site_url]]" />
        
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width,initial-scale=1.0">

        <!-- Facebook OpenGraph Data -->
        <meta property="og:title" content="[[++site_name]]" />
        <meta property="og:type" content="website" />
        <!-- If it\'s an article:
        <meta property="og:type" content="article" />
        <meta property="article:published_time" content = "" />
        -->
        <meta property="og:description" content="[[*description]]" />        
        <meta property="og:url" content="[[++site_url]]"/>
        <meta property="og:image" content="[[++site_url]][[++social_image]]" />
        <meta property="og:site_name" content="[[++site_name]]" />


<script src="js/respond.min.js"></script>

<script src="[[++site_url]]/js/mapdata.js"></script>
<script src="[[++site_url]]/js/worldmap.js"></script>
<script src="[[++site_url]]/js/jquery.fastLiveFilter.js"></script>
<script src="[[++site_url]]/js/jquery.countdown.min.js"></script>
<script src="[[++site_url]]/js/modernizr.custom.js"></script>
<link rel="stylesheet" type="text/css" href="css/lightbox.min.css">
<meta name="google-site-verification" content="lyzPX5GCX9Yi9NxiLhZGlEnTRGSsqA6xKj1sbBaz_Yo" />
<link href="[[++site_url]]/css/normalize.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<script>
  (function(i,s,o,g,r,a,m){i[\'GoogleAnalyticsObject\']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,\'script\',\'https://www.google-analytics.com/analytics.js\',\'ga\');

  ga(\'create\', \'UA-82621934-1\', \'auto\');
  ga(\'send\', \'pageview\');

</script>
 <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
 <![endif]-->
<script>
    $(function() {
        $(\'#search_input\').fastLiveFilter(\'.search_list\');
        $(\'#search_input_mobile\').fastLiveFilter(\'.search_list\');
    });

       $(window).load(function() {
      $(\'#loading\').fadeOut("slow");
      hideEmpty();
    });

    function hideEmpty() {
$(".vids").each(function(){

 if($(this).children(\'tag:visible\').length == 0) {
   $(this.parentNode.parentNode).hide(); 
}
else {
 $(this.parentNode.parentNode).show();
}
});

}
</script>



<!--[if lt IE 9]>
<script>
document.createElement(\'video\');
</script>
<![endif]-->

</head>',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'subscribe' => 
      array (
        'fields' => 
        array (
          'id' => 34,
          'source' => 2,
          'property_preprocess' => false,
          'name' => 'subscribe',
          'description' => '',
          'editor_type' => 0,
          'category' => 0,
          'cache_type' => 0,
          'snippet' => '

<div class="modal-body" id="subscribe">

     [[!FormIt?
   &hooks=`email, FormItSaveForm, redirect`
   &emailTpl=`email`
   &emailFrom=`no-reply@polochannel.com`
   &emailTo=`michael@ten25designs.com`
   &validate=`name:required,
      email:required`
   &redirectTo=`1`
]]

<form class="contact" action="[[~[[*id]]]]" method="post">
<i style="position: absolute; right:0; top: 0; color: gray; text-align: right; float: right; margin-right: 10px; margin-top: 10px; font-size: 20px; z-index: 5000;" class="fa fa-close"></i>
    <fieldset>
         <h3>Subscribe to our news</h3>
        <input type="text" name="name" value="[[!+fi.first_name]]" placeholder="First Name">
        <input type="text" name="email" value="[[!+fi.email]]" placeholder="email">
       <input class="send" type="submit" value="Send"> <!-- Send button-->
   </fieldset>
</form>

</div>',
          'locked' => false,
          'properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '

<div class="modal-body" id="subscribe">

     [[!FormIt?
   &hooks=`email, FormItSaveForm, redirect`
   &emailTpl=`email`
   &emailFrom=`no-reply@polochannel.com`
   &emailTo=`michael@ten25designs.com`
   &validate=`name:required,
      email:required`
   &redirectTo=`1`
]]

<form class="contact" action="[[~[[*id]]]]" method="post">
<i style="position: absolute; right:0; top: 0; color: gray; text-align: right; float: right; margin-right: 10px; margin-top: 10px; font-size: 20px; z-index: 5000;" class="fa fa-close"></i>
    <fieldset>
         <h3>Subscribe to our news</h3>
        <input type="text" name="name" value="[[!+fi.first_name]]" placeholder="First Name">
        <input type="text" name="email" value="[[!+fi.email]]" placeholder="email">
       <input class="send" type="submit" value="Send"> <!-- Send button-->
   </fieldset>
</form>

</div>',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
          'id' => 2,
          'name' => 'Assets',
          'description' => 'SubAdmin Media Access',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
            'basePath' => 
            array (
              'name' => 'basePath',
              'desc' => 'prop_file.basePath_desc',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'assets/media/',
              'lexicon' => 'core:source',
            ),
            'baseUrl' => 
            array (
              'name' => 'baseUrl',
              'desc' => 'prop_file.baseUrl_desc',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'assets/media/',
              'lexicon' => 'core:source',
            ),
          ),
          'is_stream' => true,
        ),
      ),
      'header' => 
      array (
        'fields' => 
        array (
          'id' => 3,
          'source' => 1,
          'property_preprocess' => false,
          'name' => 'header',
          'description' => '',
          'editor_type' => 0,
          'category' => 3,
          'cache_type' => 0,
          'snippet' => '<div class="fixed-header">
  <div class="header">

[[$header3]]

      [[$buttons]]
    <a href="/"><img src="images/mainlogo.png" alt="" height="70" class="headerimage"></a>
  </div>

  
</div>
<div class="spacer"></div>
<div class="not-so-fixie">',
          'locked' => false,
          'properties' => 
          array (
          ),
          'static' => true,
          'static_file' => 'assets/chunks/layout/header.html',
          'content' => '<div class="fixed-header">
  <div class="header">

[[$header3]]

      [[$buttons]]
    <a href="/"><img src="images/mainlogo.png" alt="" height="70" class="headerimage"></a>
  </div>

  
</div>
<div class="spacer"></div>
<div class="not-so-fixie">',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'header3' => 
      array (
        'fields' => 
        array (
          'id' => 31,
          'source' => 2,
          'property_preprocess' => false,
          'name' => 'header3',
          'description' => '',
          'editor_type' => 0,
          'category' => 0,
          'cache_type' => 0,
          'snippet' => '<div class="header3">
	  <ul class="nav">
		<li><a href="/">Home</a></li>
  
		<li><a class="videos" href="/">Videos</a></li>
  
		<li><a href="/polo-blog">News</a></li>
  

		<li><div><a href="/" class="subscribe">Subscribe</a></div></li>
	  </ul>
	</div>',
          'locked' => false,
          'properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '<div class="header3">
	  <ul class="nav">
		<li><a href="/">Home</a></li>
  
		<li><a class="videos" href="/">Videos</a></li>
  
		<li><a href="/polo-blog">News</a></li>
  

		<li><div><a href="/" class="subscribe">Subscribe</a></div></li>
	  </ul>
	</div>',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
          'id' => 2,
          'name' => 'Assets',
          'description' => 'SubAdmin Media Access',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
            'basePath' => 
            array (
              'name' => 'basePath',
              'desc' => 'prop_file.basePath_desc',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'assets/media/',
              'lexicon' => 'core:source',
            ),
            'baseUrl' => 
            array (
              'name' => 'baseUrl',
              'desc' => 'prop_file.baseUrl_desc',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'assets/media/',
              'lexicon' => 'core:source',
            ),
          ),
          'is_stream' => true,
        ),
      ),
      'buttons' => 
      array (
        'fields' => 
        array (
          'id' => 15,
          'source' => 1,
          'property_preprocess' => false,
          'name' => 'buttons',
          'description' => '',
          'editor_type' => 0,
          'category' => 3,
          'cache_type' => 0,
          'snippet' => '<ul class="headerlinks">
	      [[*live-stream:notempty=`<li><a href=""><button class="show-stream" type="button">live stream </button></a></li>`]]
[[*link-1-text:notempty=`<li><a href="[[*link-1]]" class="append"><button type="button">[[*link-1-text]]</button></a><span id="append-back"></span></li>`]]
      [[*link-2-text:notempty=`<li><a href="[[*link-2]]"><button type="button">[[*link-2-text]]</button></a></li>`]]
          <li><a href="https://www.facebook.com/polochannelnetwork" target="_blank"><div class="fa fa-facebook-official  fa-lg"></div></a></li>
      <li><a href="https://instagram.com/thepolochannel" target="_blank"><div class="fa fa-instagram  fa-lg"></div></a></li>
      <li><a href="mailto:contact@horseplay.tv"><div class="fa fa-envelope  fa-lg"></div></a></li>
  </ul>',
          'locked' => false,
          'properties' => 
          array (
          ),
          'static' => true,
          'static_file' => 'assets/chunks/layout/buttons.html',
          'content' => '<ul class="headerlinks">
	      [[*live-stream:notempty=`<li><a href=""><button class="show-stream" type="button">live stream </button></a></li>`]]
[[*link-1-text:notempty=`<li><a href="[[*link-1]]" class="append"><button type="button">[[*link-1-text]]</button></a><span id="append-back"></span></li>`]]
      [[*link-2-text:notempty=`<li><a href="[[*link-2]]"><button type="button">[[*link-2-text]]</button></a></li>`]]
          <li><a href="https://www.facebook.com/polochannelnetwork" target="_blank"><div class="fa fa-facebook-official  fa-lg"></div></a></li>
      <li><a href="https://instagram.com/thepolochannel" target="_blank"><div class="fa fa-instagram  fa-lg"></div></a></li>
      <li><a href="mailto:contact@horseplay.tv"><div class="fa fa-envelope  fa-lg"></div></a></li>
  </ul>',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'feed' => 
      array (
        'fields' => 
        array (
          'id' => 21,
          'source' => 2,
          'property_preprocess' => false,
          'name' => 'feed',
          'description' => '',
          'editor_type' => 0,
          'category' => 0,
          'cache_type' => 0,
          'snippet' => '<div class="polo-feed">
      <div class="three-col small-col">
			[[!getResources? &parents=`187` &limit=`999` &tpl=`blog_post` &includeTVs=`1` &processTVs=`1`]]
      </div>
      <div class="three-col large-col">

      </div>
	  <div class="append-back">
      <div class="three-col small-col" id="score-list">
			[[!getResources? &parents=`190` &limit=`999` &tpl=`score_post` &includeTVs=`1` &processTVs=`1`]]
      </div>
	  </div>
</div>',
          'locked' => false,
          'properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '<div class="polo-feed">
      <div class="three-col small-col">
			[[!getResources? &parents=`187` &limit=`999` &tpl=`blog_post` &includeTVs=`1` &processTVs=`1`]]
      </div>
      <div class="three-col large-col">

      </div>
	  <div class="append-back">
      <div class="three-col small-col" id="score-list">
			[[!getResources? &parents=`190` &limit=`999` &tpl=`score_post` &includeTVs=`1` &processTVs=`1`]]
      </div>
	  </div>
</div>',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
          'id' => 2,
          'name' => 'Assets',
          'description' => 'SubAdmin Media Access',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
            'basePath' => 
            array (
              'name' => 'basePath',
              'desc' => 'prop_file.basePath_desc',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'assets/media/',
              'lexicon' => 'core:source',
            ),
            'baseUrl' => 
            array (
              'name' => 'baseUrl',
              'desc' => 'prop_file.baseUrl_desc',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'assets/media/',
              'lexicon' => 'core:source',
            ),
          ),
          'is_stream' => true,
        ),
      ),
      'header2' => 
      array (
        'fields' => 
        array (
          'id' => 22,
          'source' => 2,
          'property_preprocess' => false,
          'name' => 'header2',
          'description' => '',
          'editor_type' => 0,
          'category' => 0,
          'cache_type' => 0,
          'snippet' => '<div class="header2">
    <ul class="nav">
      <li class="group_toggle" id="group1">games&nbsp;<img src="map_images/blue.png" height="20"></li>
      <li><div class="v-bar"></div></li>
      <li class="group_toggle" id="group3">featured&nbsp;<img src="map_images/yellow.png" height="20"></li>
      <li><div class="v-bar"></div></li>
      <li class="group_toggle" id="group2">profiles&nbsp;<img src="map_images/green.png" height="20"></li>
      <li><div class="v-bar"></div></li>
      <li class="group_toggle" id="group0">events&nbsp;<img src="map_images/orange.png" height="20"></li>
      <li><div class="v-bar" height="20"></div></li>
      <li><div class="showall">show all</div></li>
      <li><input type="text" id="search_input" placeholder="search..."></li>


	  <li><div class="map-btn">Map</div></li>
       
    </ul>

  </div>',
          'locked' => false,
          'properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '<div class="header2">
    <ul class="nav">
      <li class="group_toggle" id="group1">games&nbsp;<img src="map_images/blue.png" height="20"></li>
      <li><div class="v-bar"></div></li>
      <li class="group_toggle" id="group3">featured&nbsp;<img src="map_images/yellow.png" height="20"></li>
      <li><div class="v-bar"></div></li>
      <li class="group_toggle" id="group2">profiles&nbsp;<img src="map_images/green.png" height="20"></li>
      <li><div class="v-bar"></div></li>
      <li class="group_toggle" id="group0">events&nbsp;<img src="map_images/orange.png" height="20"></li>
      <li><div class="v-bar" height="20"></div></li>
      <li><div class="showall">show all</div></li>
      <li><input type="text" id="search_input" placeholder="search..."></li>


	  <li><div class="map-btn">Map</div></li>
       
    </ul>

  </div>',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
          'id' => 2,
          'name' => 'Assets',
          'description' => 'SubAdmin Media Access',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
            'basePath' => 
            array (
              'name' => 'basePath',
              'desc' => 'prop_file.basePath_desc',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'assets/media/',
              'lexicon' => 'core:source',
            ),
            'baseUrl' => 
            array (
              'name' => 'baseUrl',
              'desc' => 'prop_file.baseUrl_desc',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'assets/media/',
              'lexicon' => 'core:source',
            ),
          ),
          'is_stream' => true,
        ),
      ),
      'clubs-nav' => 
      array (
        'fields' => 
        array (
          'id' => 16,
          'source' => 1,
          'property_preprocess' => false,
          'name' => 'clubs-nav',
          'description' => '',
          'editor_type' => 0,
          'category' => 3,
          'cache_type' => 0,
          'snippet' => '[[getResources? &parents=`32` &sortby=`{"menuindex":"ASC"}` &limit=`0` &tpl=`clubs` &includeTVs=`1` &processTVs=`1` &showUnpublished=`1` &showHidded=`1` &tvPrefix=``]]                        ',
          'locked' => false,
          'properties' => 
          array (
          ),
          'static' => true,
          'static_file' => 'assets/chunks/layout/clubs-nav',
          'content' => '[[getResources? &parents=`32` &sortby=`{"menuindex":"ASC"}` &limit=`0` &tpl=`clubs` &includeTVs=`1` &processTVs=`1` &showUnpublished=`1` &showHidded=`1` &tvPrefix=``]]                        ',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'feed-mobile' => 
      array (
        'fields' => 
        array (
          'id' => 36,
          'source' => 2,
          'property_preprocess' => false,
          'name' => 'feed-mobile',
          'description' => '',
          'editor_type' => 0,
          'category' => 0,
          'cache_type' => 0,
          'snippet' => '<div class="polo-feed-mobile">
		<h3>News</h3>
		<div class="scroller">
         [[!getResources? &parents=`187` &limit=`10` &tpl=`blog-list` &includeTVs=`1` &processTVs=`1`]]
		</div>
		<div style="text-align: center; font-size: 10px; color: gray;"><i class="fa fa-arrow-left"></i> swipe to scroll <i class="fa fa-arrow-right"></i></div>
</div>',
          'locked' => false,
          'properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '<div class="polo-feed-mobile">
		<h3>News</h3>
		<div class="scroller">
         [[!getResources? &parents=`187` &limit=`10` &tpl=`blog-list` &includeTVs=`1` &processTVs=`1`]]
		</div>
		<div style="text-align: center; font-size: 10px; color: gray;"><i class="fa fa-arrow-left"></i> swipe to scroll <i class="fa fa-arrow-right"></i></div>
</div>',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
          'id' => 2,
          'name' => 'Assets',
          'description' => 'SubAdmin Media Access',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
            'basePath' => 
            array (
              'name' => 'basePath',
              'desc' => 'prop_file.basePath_desc',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'assets/media/',
              'lexicon' => 'core:source',
            ),
            'baseUrl' => 
            array (
              'name' => 'baseUrl',
              'desc' => 'prop_file.baseUrl_desc',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'assets/media/',
              'lexicon' => 'core:source',
            ),
          ),
          'is_stream' => true,
        ),
      ),
      'doc_footer' => 
      array (
        'fields' => 
        array (
          'id' => 2,
          'source' => 1,
          'property_preprocess' => false,
          'name' => 'doc_footer',
          'description' => '',
          'editor_type' => 0,
          'category' => 3,
          'cache_type' => 0,
          'snippet' => '
<!-- javascript !-->

<script type="text/javascript"> 
$(\'.vids a\').simpleLightboxVideo();
</script>
<script src="js/custom.js"></script>

</body>
</html>',
          'locked' => false,
          'properties' => 
          array (
          ),
          'static' => true,
          'static_file' => 'assets/chunks/layout/doc_footer.html',
          'content' => '
<!-- javascript !-->

<script type="text/javascript"> 
$(\'.vids a\').simpleLightboxVideo();
</script>
<script src="js/custom.js"></script>

</body>
</html>',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
    ),
    'modSnippet' => 
    array (
      'getResources' => 
      array (
        'fields' => 
        array (
          'id' => 33,
          'source' => 0,
          'property_preprocess' => false,
          'name' => 'getResources',
          'description' => '<strong>1.6.1-pl</strong> A general purpose Resource listing and summarization snippet for MODX Revolution',
          'editor_type' => 0,
          'category' => 0,
          'cache_type' => 0,
          'snippet' => '/**
 * getResources
 *
 * A general purpose Resource listing and summarization snippet for MODX 2.x.
 *
 * @author Jason Coward
 * @copyright Copyright 2010-2013, Jason Coward
 *
 * TEMPLATES
 *
 * tpl - Name of a chunk serving as a resource template
 * [NOTE: if not provided, properties are dumped to output for each resource]
 *
 * tplOdd - (Opt) Name of a chunk serving as resource template for resources with an odd idx value
 * (see idx property)
 * tplFirst - (Opt) Name of a chunk serving as resource template for the first resource (see first
 * property)
 * tplLast - (Opt) Name of a chunk serving as resource template for the last resource (see last
 * property)
 * tpl_{n} - (Opt) Name of a chunk serving as resource template for the nth resource
 *
 * tplCondition - (Opt) Defines a field of the resource to evaluate against keys defined in the
 * conditionalTpls property. Must be a resource field; does not work with Template Variables.
 * conditionalTpls - (Opt) A JSON object defining a map of field values and the associated tpl to
 * use when the field defined by tplCondition matches the value. [NOTE: tplOdd, tplFirst, tplLast,
 * and tpl_{n} will take precedence over any defined conditionalTpls]
 *
 * tplWrapper - (Opt) Name of a chunk serving as a wrapper template for the output
 * [NOTE: Does not work with toSeparatePlaceholders]
 *
 * SELECTION
 *
 * parents - Comma-delimited list of ids serving as parents
 *
 * context - (Opt) Comma-delimited list of context keys to limit results by; if empty, contexts for all specified
 * parents will be used (all contexts if 0 is specified) [default=]
 *
 * depth - (Opt) Integer value indicating depth to search for resources from each parent [default=10]
 *
 * tvFilters - (Opt) Delimited-list of TemplateVar values to filter resources by. Supports two
 * delimiters and two value search formats. The first delimiter || represents a logical OR and the
 * primary grouping mechanism.  Within each group you can provide a comma-delimited list of values.
 * These values can be either tied to a specific TemplateVar by name, e.g. myTV==value, or just the
 * value, indicating you are searching for the value in any TemplateVar tied to the Resource. An
 * example would be &tvFilters=`filter2==one,filter1==bar%||filter1==foo`
 * [NOTE: filtering by values uses a LIKE query and % is considered a wildcard.]
 * [NOTE: this only looks at the raw value set for specific Resource, i. e. there must be a value
 * specifically set for the Resource and it is not evaluated.]
 *
 * tvFiltersAndDelimiter - (Opt) Custom delimiter for logical AND, default \',\', in case you want to
 * match a literal comma in the tvFilters. E.g. &tvFiltersAndDelimiter=`&&`
 * &tvFilters=`filter1==foo,bar&&filter2==baz` [default=,]
 *
 * tvFiltersOrDelimiter - (Opt) Custom delimiter for logical OR, default \'||\', in case you want to
 * match a literal \'||\' in the tvFilters. E.g. &tvFiltersOrDelimiter=`|OR|`
 * &tvFilters=`filter1==foo||bar|OR|filter2==baz` [default=||]
 *
 * where - (Opt) A JSON expression of criteria to build any additional where clauses from. An example would be
 * &where=`{{"alias:LIKE":"foo%", "OR:alias:LIKE":"%bar"},{"OR:pagetitle:=":"foobar", "AND:description:=":"raboof"}}`
 *
 * sortby - (Opt) Field to sort by or a JSON array, e.g. {"publishedon":"ASC","createdon":"DESC"} [default=publishedon]
 * sortbyTV - (opt) A Template Variable name to sort by (if supplied, this precedes the sortby value) [default=]
 * sortbyTVType - (Opt) A data type to CAST a TV Value to in order to sort on it properly [default=string]
 * sortbyAlias - (Opt) Query alias for sortby field [default=]
 * sortbyEscaped - (Opt) Escapes the field name(s) specified in sortby [default=0]
 * sortdir - (Opt) Order which to sort by [default=DESC]
 * sortdirTV - (Opt) Order which to sort by a TV [default=DESC]
 * limit - (Opt) Limits the number of resources returned [default=5]
 * offset - (Opt) An offset of resources returned by the criteria to skip [default=0]
 * dbCacheFlag - (Opt) Controls caching of db queries; 0|false = do not cache result set; 1 = cache result set
 * according to cache settings, any other integer value = number of seconds to cache result set [default=0]
 *
 * OPTIONS
 *
 * includeContent - (Opt) Indicates if the content of each resource should be returned in the
 * results [default=0]
 * includeTVs - (Opt) Indicates if TemplateVar values should be included in the properties available
 * to each resource template [default=0]
 * includeTVList - (Opt) Limits the TemplateVars that are included if includeTVs is true to those specified
 * by name in a comma-delimited list [default=]
 * prepareTVs - (Opt) Prepares media-source dependent TemplateVar values [default=1]
 * prepareTVList - (Opt) Limits the TVs that are prepared to those specified by name in a comma-delimited
 * list [default=]
 * processTVs - (Opt) Indicates if TemplateVar values should be rendered as they would on the
 * resource being summarized [default=0]
 * processTVList - (opt) Limits the TemplateVars that are processed if included to those specified
 * by name in a comma-delimited list [default=]
 * tvPrefix - (Opt) The prefix for TemplateVar properties [default=tv.]
 * idx - (Opt) You can define the starting idx of the resources, which is an property that is
 * incremented as each resource is rendered [default=1]
 * first - (Opt) Define the idx which represents the first resource (see tplFirst) [default=1]
 * last - (Opt) Define the idx which represents the last resource (see tplLast) [default=# of
 * resources being summarized + first - 1]
 * outputSeparator - (Opt) An optional string to separate each tpl instance [default="\\n"]
 * wrapIfEmpty - (Opt) Indicates if the tplWrapper should be applied if the output is empty [default=0]
 *
 */
$output = array();
$outputSeparator = isset($outputSeparator) ? $outputSeparator : "\\n";

/* set default properties */
$tpl = !empty($tpl) ? $tpl : \'\';
$includeContent = !empty($includeContent) ? true : false;
$includeTVs = !empty($includeTVs) ? true : false;
$includeTVList = !empty($includeTVList) ? explode(\',\', $includeTVList) : array();
$processTVs = !empty($processTVs) ? true : false;
$processTVList = !empty($processTVList) ? explode(\',\', $processTVList) : array();
$prepareTVs = !empty($prepareTVs) ? true : false;
$prepareTVList = !empty($prepareTVList) ? explode(\',\', $prepareTVList) : array();
$tvPrefix = isset($tvPrefix) ? $tvPrefix : \'tv.\';
$parents = (!empty($parents) || $parents === \'0\') ? explode(\',\', $parents) : array($modx->resource->get(\'id\'));
array_walk($parents, \'trim\');
$parents = array_unique($parents);
$depth = isset($depth) ? (integer) $depth : 10;

$tvFiltersOrDelimiter = isset($tvFiltersOrDelimiter) ? $tvFiltersOrDelimiter : \'||\';
$tvFiltersAndDelimiter = isset($tvFiltersAndDelimiter) ? $tvFiltersAndDelimiter : \',\';
$tvFilters = !empty($tvFilters) ? explode($tvFiltersOrDelimiter, $tvFilters) : array();

$where = !empty($where) ? $modx->fromJSON($where) : array();
$showUnpublished = !empty($showUnpublished) ? true : false;
$showDeleted = !empty($showDeleted) ? true : false;

$sortby = isset($sortby) ? $sortby : \'publishedon\';
$sortbyTV = isset($sortbyTV) ? $sortbyTV : \'\';
$sortbyAlias = isset($sortbyAlias) ? $sortbyAlias : \'modResource\';
$sortbyEscaped = !empty($sortbyEscaped) ? true : false;
$sortdir = isset($sortdir) ? $sortdir : \'DESC\';
$sortdirTV = isset($sortdirTV) ? $sortdirTV : \'DESC\';
$limit = isset($limit) ? (integer) $limit : 5;
$offset = isset($offset) ? (integer) $offset : 0;
$totalVar = !empty($totalVar) ? $totalVar : \'total\';

$dbCacheFlag = !isset($dbCacheFlag) ? false : $dbCacheFlag;
if (is_string($dbCacheFlag) || is_numeric($dbCacheFlag)) {
    if ($dbCacheFlag == \'0\') {
        $dbCacheFlag = false;
    } elseif ($dbCacheFlag == \'1\') {
        $dbCacheFlag = true;
    } else {
        $dbCacheFlag = (integer) $dbCacheFlag;
    }
}

/* multiple context support */
$contextArray = array();
$contextSpecified = false;
if (!empty($context)) {
    $contextArray = explode(\',\',$context);
    array_walk($contextArray, \'trim\');
    $contexts = array();
    foreach ($contextArray as $ctx) {
        $contexts[] = $modx->quote($ctx);
    }
    $context = implode(\',\',$contexts);
    $contextSpecified = true;
    unset($contexts,$ctx);
} else {
    $context = $modx->quote($modx->context->get(\'key\'));
}

$pcMap = array();
$pcQuery = $modx->newQuery(\'modResource\', array(\'id:IN\' => $parents), $dbCacheFlag);
$pcQuery->select(array(\'id\', \'context_key\'));
if ($pcQuery->prepare() && $pcQuery->stmt->execute()) {
    foreach ($pcQuery->stmt->fetchAll(PDO::FETCH_ASSOC) as $pcRow) {
        $pcMap[(integer) $pcRow[\'id\']] = $pcRow[\'context_key\'];
    }
}

$children = array();
$parentArray = array();
foreach ($parents as $parent) {
    $parent = (integer) $parent;
    if ($parent === 0) {
        $pchildren = array();
        if ($contextSpecified) {
            foreach ($contextArray as $pCtx) {
                if (!in_array($pCtx, $contextArray)) {
                    continue;
                }
                $options = $pCtx !== $modx->context->get(\'key\') ? array(\'context\' => $pCtx) : array();
                $pcchildren = $modx->getChildIds($parent, $depth, $options);
                if (!empty($pcchildren)) $pchildren = array_merge($pchildren, $pcchildren);
            }
        } else {
            $cQuery = $modx->newQuery(\'modContext\', array(\'key:!=\' => \'mgr\'));
            $cQuery->select(array(\'key\'));
            if ($cQuery->prepare() && $cQuery->stmt->execute()) {
                foreach ($cQuery->stmt->fetchAll(PDO::FETCH_COLUMN) as $pCtx) {
                    $options = $pCtx !== $modx->context->get(\'key\') ? array(\'context\' => $pCtx) : array();
                    $pcchildren = $modx->getChildIds($parent, $depth, $options);
                    if (!empty($pcchildren)) $pchildren = array_merge($pchildren, $pcchildren);
                }
            }
        }
        $parentArray[] = $parent;
    } else {
        $pContext = array_key_exists($parent, $pcMap) ? $pcMap[$parent] : false;
        if ($debug) $modx->log(modX::LOG_LEVEL_ERROR, "context for {$parent} is {$pContext}");
        if ($pContext && $contextSpecified && !in_array($pContext, $contextArray, true)) {
            $parent = next($parents);
            continue;
        }
        $parentArray[] = $parent;
        $options = !empty($pContext) && $pContext !== $modx->context->get(\'key\') ? array(\'context\' => $pContext) : array();
        $pchildren = $modx->getChildIds($parent, $depth, $options);
    }
    if (!empty($pchildren)) $children = array_merge($children, $pchildren);
    $parent = next($parents);
}
$parents = array_merge($parentArray, $children);

/* build query */
$criteria = array("modResource.parent IN (" . implode(\',\', $parents) . ")");
if ($contextSpecified) {
    $contextResourceTbl = $modx->getTableName(\'modContextResource\');
    $criteria[] = "(modResource.context_key IN ({$context}) OR EXISTS(SELECT 1 FROM {$contextResourceTbl} ctx WHERE ctx.resource = modResource.id AND ctx.context_key IN ({$context})))";
}
if (empty($showDeleted)) {
    $criteria[\'deleted\'] = \'0\';
}
if (empty($showUnpublished)) {
    $criteria[\'published\'] = \'1\';
}
if (empty($showHidden)) {
    $criteria[\'hidemenu\'] = \'0\';
}
if (!empty($hideContainers)) {
    $criteria[\'isfolder\'] = \'0\';
}
$criteria = $modx->newQuery(\'modResource\', $criteria);
if (!empty($tvFilters)) {
    $tmplVarTbl = $modx->getTableName(\'modTemplateVar\');
    $tmplVarResourceTbl = $modx->getTableName(\'modTemplateVarResource\');
    $conditions = array();
    $operators = array(
        \'<=>\' => \'<=>\',
        \'===\' => \'=\',
        \'!==\' => \'!=\',
        \'<>\' => \'<>\',
        \'==\' => \'LIKE\',
        \'!=\' => \'NOT LIKE\',
        \'<<\' => \'<\',
        \'<=\' => \'<=\',
        \'=<\' => \'=<\',
        \'>>\' => \'>\',
        \'>=\' => \'>=\',
        \'=>\' => \'=>\'
    );
    foreach ($tvFilters as $fGroup => $tvFilter) {
        $filterGroup = array();
        $filters = explode($tvFiltersAndDelimiter, $tvFilter);
        $multiple = count($filters) > 0;
        foreach ($filters as $filter) {
            $operator = \'==\';
            $sqlOperator = \'LIKE\';
            foreach ($operators as $op => $opSymbol) {
                if (strpos($filter, $op, 1) !== false) {
                    $operator = $op;
                    $sqlOperator = $opSymbol;
                    break;
                }
            }
            $tvValueField = \'tvr.value\';
            $tvDefaultField = \'tv.default_text\';
            $f = explode($operator, $filter);
            if (count($f) >= 2) {
                if (count($f) > 2) {
                    $k = array_shift($f);
                    $b = join($operator, $f);
                    $f = array($k, $b);
                }
                $tvName = $modx->quote($f[0]);
                if (is_numeric($f[1]) && !in_array($sqlOperator, array(\'LIKE\', \'NOT LIKE\'))) {
                    $tvValue = $f[1];
                    if ($f[1] == (integer)$f[1]) {
                        $tvValueField = "CAST({$tvValueField} AS SIGNED INTEGER)";
                        $tvDefaultField = "CAST({$tvDefaultField} AS SIGNED INTEGER)";
                    } else {
                        $tvValueField = "CAST({$tvValueField} AS DECIMAL)";
                        $tvDefaultField = "CAST({$tvDefaultField} AS DECIMAL)";
                    }
                } else {
                    $tvValue = $modx->quote($f[1]);
                }
                if ($multiple) {
                    $filterGroup[] =
                        "(EXISTS (SELECT 1 FROM {$tmplVarResourceTbl} tvr JOIN {$tmplVarTbl} tv ON {$tvValueField} {$sqlOperator} {$tvValue} AND tv.name = {$tvName} AND tv.id = tvr.tmplvarid WHERE tvr.contentid = modResource.id) " .
                        "OR EXISTS (SELECT 1 FROM {$tmplVarTbl} tv WHERE tv.name = {$tvName} AND {$tvDefaultField} {$sqlOperator} {$tvValue} AND tv.id NOT IN (SELECT tmplvarid FROM {$tmplVarResourceTbl} WHERE contentid = modResource.id)) " .
                        ")";
                } else {
                    $filterGroup =
                        "(EXISTS (SELECT 1 FROM {$tmplVarResourceTbl} tvr JOIN {$tmplVarTbl} tv ON {$tvValueField} {$sqlOperator} {$tvValue} AND tv.name = {$tvName} AND tv.id = tvr.tmplvarid WHERE tvr.contentid = modResource.id) " .
                        "OR EXISTS (SELECT 1 FROM {$tmplVarTbl} tv WHERE tv.name = {$tvName} AND {$tvDefaultField} {$sqlOperator} {$tvValue} AND tv.id NOT IN (SELECT tmplvarid FROM {$tmplVarResourceTbl} WHERE contentid = modResource.id)) " .
                        ")";
                }
            } elseif (count($f) == 1) {
                $tvValue = $modx->quote($f[0]);
                if ($multiple) {
                    $filterGroup[] = "EXISTS (SELECT 1 FROM {$tmplVarResourceTbl} tvr JOIN {$tmplVarTbl} tv ON {$tvValueField} {$sqlOperator} {$tvValue} AND tv.id = tvr.tmplvarid WHERE tvr.contentid = modResource.id)";
                } else {
                    $filterGroup = "EXISTS (SELECT 1 FROM {$tmplVarResourceTbl} tvr JOIN {$tmplVarTbl} tv ON {$tvValueField} {$sqlOperator} {$tvValue} AND tv.id = tvr.tmplvarid WHERE tvr.contentid = modResource.id)";
                }
            }
        }
        $conditions[] = $filterGroup;
    }
    if (!empty($conditions)) {
        $firstGroup = true;
        foreach ($conditions as $cGroup => $c) {
            if (is_array($c)) {
                $first = true;
                foreach ($c as $cond) {
                    if ($first && !$firstGroup) {
                        $criteria->condition($criteria->query[\'where\'][0][1], $cond, xPDOQuery::SQL_OR, null, $cGroup);
                    } else {
                        $criteria->condition($criteria->query[\'where\'][0][1], $cond, xPDOQuery::SQL_AND, null, $cGroup);
                    }
                    $first = false;
                }
            } else {
                $criteria->condition($criteria->query[\'where\'][0][1], $c, $firstGroup ? xPDOQuery::SQL_AND : xPDOQuery::SQL_OR, null, $cGroup);
            }
            $firstGroup = false;
        }
    }
}
/* include/exclude resources, via &resources=`123,-456` prop */
if (!empty($resources)) {
    $resourceConditions = array();
    $resources = explode(\',\',$resources);
    $include = array();
    $exclude = array();
    foreach ($resources as $resource) {
        $resource = (int)$resource;
        if ($resource == 0) continue;
        if ($resource < 0) {
            $exclude[] = abs($resource);
        } else {
            $include[] = $resource;
        }
    }
    if (!empty($include)) {
        $criteria->where(array(\'OR:modResource.id:IN\' => $include), xPDOQuery::SQL_OR);
    }
    if (!empty($exclude)) {
        $criteria->where(array(\'modResource.id:NOT IN\' => $exclude), xPDOQuery::SQL_AND, null, 1);
    }
}
if (!empty($where)) {
    $criteria->where($where);
}

$total = $modx->getCount(\'modResource\', $criteria);
$modx->setPlaceholder($totalVar, $total);

$fields = array_keys($modx->getFields(\'modResource\'));
if (empty($includeContent)) {
    $fields = array_diff($fields, array(\'content\'));
}
$columns = $includeContent ? $modx->getSelectColumns(\'modResource\', \'modResource\') : $modx->getSelectColumns(\'modResource\', \'modResource\', \'\', array(\'content\'), true);
$criteria->select($columns);
if (!empty($sortbyTV)) {
    $criteria->leftJoin(\'modTemplateVar\', \'tvDefault\', array(
        "tvDefault.name" => $sortbyTV
    ));
    $criteria->leftJoin(\'modTemplateVarResource\', \'tvSort\', array(
        "tvSort.contentid = modResource.id",
        "tvSort.tmplvarid = tvDefault.id"
    ));
    if (empty($sortbyTVType)) $sortbyTVType = \'string\';
    if ($modx->getOption(\'dbtype\') === \'mysql\') {
        switch ($sortbyTVType) {
            case \'integer\':
                $criteria->select("CAST(IFNULL(tvSort.value, tvDefault.default_text) AS SIGNED INTEGER) AS sortTV");
                break;
            case \'decimal\':
                $criteria->select("CAST(IFNULL(tvSort.value, tvDefault.default_text) AS DECIMAL) AS sortTV");
                break;
            case \'datetime\':
                $criteria->select("CAST(IFNULL(tvSort.value, tvDefault.default_text) AS DATETIME) AS sortTV");
                break;
            case \'string\':
            default:
                $criteria->select("IFNULL(tvSort.value, tvDefault.default_text) AS sortTV");
                break;
        }
    } elseif ($modx->getOption(\'dbtype\') === \'sqlsrv\') {
        switch ($sortbyTVType) {
            case \'integer\':
                $criteria->select("CAST(ISNULL(tvSort.value, tvDefault.default_text) AS BIGINT) AS sortTV");
                break;
            case \'decimal\':
                $criteria->select("CAST(ISNULL(tvSort.value, tvDefault.default_text) AS DECIMAL) AS sortTV");
                break;
            case \'datetime\':
                $criteria->select("CAST(ISNULL(tvSort.value, tvDefault.default_text) AS DATETIME) AS sortTV");
                break;
            case \'string\':
            default:
                $criteria->select("ISNULL(tvSort.value, tvDefault.default_text) AS sortTV");
                break;
        }
    }
    $criteria->sortby("sortTV", $sortdirTV);
}
if (!empty($sortby)) {
    if (strpos($sortby, \'{\') === 0) {
        $sorts = $modx->fromJSON($sortby);
    } else {
        $sorts = array($sortby => $sortdir);
    }
    if (is_array($sorts)) {
        while (list($sort, $dir) = each($sorts)) {
            if ($sortbyEscaped) $sort = $modx->escape($sort);
            if (!empty($sortbyAlias)) $sort = $modx->escape($sortbyAlias) . ".{$sort}";
            $criteria->sortby($sort, $dir);
        }
    }
}
if (!empty($limit)) $criteria->limit($limit, $offset);

if (!empty($debug)) {
    $criteria->prepare();
    $modx->log(modX::LOG_LEVEL_ERROR, $criteria->toSQL());
}
$collection = $modx->getCollection(\'modResource\', $criteria, $dbCacheFlag);

$idx = !empty($idx) || $idx === \'0\' ? (integer) $idx : 1;
$first = empty($first) && $first !== \'0\' ? 1 : (integer) $first;
$last = empty($last) ? (count($collection) + $idx - 1) : (integer) $last;

/* include parseTpl */
include_once $modx->getOption(\'getresources.core_path\',null,$modx->getOption(\'core_path\').\'components/getresources/\').\'include.parsetpl.php\';

$templateVars = array();
if (!empty($includeTVs) && !empty($includeTVList)) {
    $templateVars = $modx->getCollection(\'modTemplateVar\', array(\'name:IN\' => $includeTVList));
}
/** @var modResource $resource */
foreach ($collection as $resourceId => $resource) {
    $tvs = array();
    if (!empty($includeTVs)) {
        if (empty($includeTVList)) {
            $templateVars = $resource->getMany(\'TemplateVars\');
        }
        /** @var modTemplateVar $templateVar */
        foreach ($templateVars as $tvId => $templateVar) {
            if (!empty($includeTVList) && !in_array($templateVar->get(\'name\'), $includeTVList)) continue;
            if ($processTVs && (empty($processTVList) || in_array($templateVar->get(\'name\'), $processTVList))) {
                $tvs[$tvPrefix . $templateVar->get(\'name\')] = $templateVar->renderOutput($resource->get(\'id\'));
            } else {
                $value = $templateVar->getValue($resource->get(\'id\'));
                if ($prepareTVs && method_exists($templateVar, \'prepareOutput\') && (empty($prepareTVList) || in_array($templateVar->get(\'name\'), $prepareTVList))) {
                    $value = $templateVar->prepareOutput($value);
                }
                $tvs[$tvPrefix . $templateVar->get(\'name\')] = $value;
            }
        }
    }
    $odd = ($idx & 1);
    $properties = array_merge(
        $scriptProperties
        ,array(
            \'idx\' => $idx
            ,\'first\' => $first
            ,\'last\' => $last
            ,\'odd\' => $odd
        )
        ,$includeContent ? $resource->toArray() : $resource->get($fields)
        ,$tvs
    );
    $resourceTpl = false;
    if ($idx == $first && !empty($tplFirst)) {
        $resourceTpl = parseTpl($tplFirst, $properties);
    }
    if ($idx == $last && empty($resourceTpl) && !empty($tplLast)) {
        $resourceTpl = parseTpl($tplLast, $properties);
    }
    $tplidx = \'tpl_\' . $idx;
    if (empty($resourceTpl) && !empty($$tplidx)) {
        $resourceTpl = parseTpl($$tplidx, $properties);
    }
    if ($idx > 1 && empty($resourceTpl)) {
        $divisors = getDivisors($idx);
        if (!empty($divisors)) {
            foreach ($divisors as $divisor) {
                $tplnth = \'tpl_n\' . $divisor;
                if (!empty($$tplnth)) {
                    $resourceTpl = parseTpl($$tplnth, $properties);
                    if (!empty($resourceTpl)) {
                        break;
                    }
                }
            }
        }
    }
    if ($odd && empty($resourceTpl) && !empty($tplOdd)) {
        $resourceTpl = parseTpl($tplOdd, $properties);
    }
    if (!empty($tplCondition) && !empty($conditionalTpls) && empty($resourceTpl)) {
        $conTpls = $modx->fromJSON($conditionalTpls);
        $subject = $properties[$tplCondition];
        $tplOperator = !empty($tplOperator) ? $tplOperator : \'=\';
        $tplOperator = strtolower($tplOperator);
        $tplCon = \'\';
        foreach ($conTpls as $operand => $conditionalTpl) {
            switch ($tplOperator) {
                case \'!=\':
                case \'neq\':
                case \'not\':
                case \'isnot\':
                case \'isnt\':
                case \'unequal\':
                case \'notequal\':
                    $tplCon = (($subject != $operand) ? $conditionalTpl : $tplCon);
                    break;
                case \'<\':
                case \'lt\':
                case \'less\':
                case \'lessthan\':
                    $tplCon = (($subject < $operand) ? $conditionalTpl : $tplCon);
                    break;
                case \'>\':
                case \'gt\':
                case \'greater\':
                case \'greaterthan\':
                    $tplCon = (($subject > $operand) ? $conditionalTpl : $tplCon);
                    break;
                case \'<=\':
                case \'lte\':
                case \'lessthanequals\':
                case \'lessthanorequalto\':
                    $tplCon = (($subject <= $operand) ? $conditionalTpl : $tplCon);
                    break;
                case \'>=\':
                case \'gte\':
                case \'greaterthanequals\':
                case \'greaterthanequalto\':
                    $tplCon = (($subject >= $operand) ? $conditionalTpl : $tplCon);
                    break;
                case \'isempty\':
                case \'empty\':
                    $tplCon = empty($subject) ? $conditionalTpl : $tplCon;
                    break;
                case \'!empty\':
                case \'notempty\':
                case \'isnotempty\':
                    $tplCon = !empty($subject) && $subject != \'\' ? $conditionalTpl : $tplCon;
                    break;
                case \'isnull\':
                case \'null\':
                    $tplCon = $subject == null || strtolower($subject) == \'null\' ? $conditionalTpl : $tplCon;
                    break;
                case \'inarray\':
                case \'in_array\':
                case \'ia\':
                    $operand = explode(\',\', $operand);
                    $tplCon = in_array($subject, $operand) ? $conditionalTpl : $tplCon;
                    break;
                case \'between\':
                case \'range\':
                case \'>=<\':
                case \'><\':
                    $operand = explode(\',\', $operand);
                    $tplCon = ($subject >= min($operand) && $subject <= max($operand)) ? $conditionalTpl : $tplCon;
                    break;
                case \'==\':
                case \'=\':
                case \'eq\':
                case \'is\':
                case \'equal\':
                case \'equals\':
                case \'equalto\':
                default:
                    $tplCon = (($subject == $operand) ? $conditionalTpl : $tplCon);
                    break;
            }
        }
        if (!empty($tplCon)) {
            $resourceTpl = parseTpl($tplCon, $properties);
        }
    }
    if (!empty($tpl) && empty($resourceTpl)) {
        $resourceTpl = parseTpl($tpl, $properties);
    }
    if ($resourceTpl === false && !empty($debug)) {
        $chunk = $modx->newObject(\'modChunk\');
        $chunk->setCacheable(false);
        $output[]= $chunk->process(array(), \'<pre>\' . print_r($properties, true) .\'</pre>\');
    } else {
        $output[]= $resourceTpl;
    }
    $idx++;
}

/* output */
$toSeparatePlaceholders = $modx->getOption(\'toSeparatePlaceholders\', $scriptProperties, false);
if (!empty($toSeparatePlaceholders)) {
    $modx->setPlaceholders($output, $toSeparatePlaceholders);
    return \'\';
}

$output = implode($outputSeparator, $output);

$tplWrapper = $modx->getOption(\'tplWrapper\', $scriptProperties, false);
$wrapIfEmpty = $modx->getOption(\'wrapIfEmpty\', $scriptProperties, false);
if (!empty($tplWrapper) && ($wrapIfEmpty || !empty($output))) {
    $output = parseTpl($tplWrapper, array_merge($scriptProperties, array(\'output\' => $output)));
}

$toPlaceholder = $modx->getOption(\'toPlaceholder\', $scriptProperties, false);
if (!empty($toPlaceholder)) {
    $modx->setPlaceholder($toPlaceholder, $output);
    return \'\';
}
return $output;',
          'locked' => false,
          'properties' => 
          array (
            'tpl' => 
            array (
              'name' => 'tpl',
              'desc' => 'Name of a chunk serving as a resource template. NOTE: if not provided, properties are dumped to output for each resource.',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'Name of a chunk serving as a resource template. NOTE: if not provided, properties are dumped to output for each resource.',
              'area_trans' => '',
            ),
            'tplOdd' => 
            array (
              'name' => 'tplOdd',
              'desc' => 'Name of a chunk serving as resource template for resources with an odd idx value (see idx property).',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'Name of a chunk serving as resource template for resources with an odd idx value (see idx property).',
              'area_trans' => '',
            ),
            'tplFirst' => 
            array (
              'name' => 'tplFirst',
              'desc' => 'Name of a chunk serving as resource template for the first resource (see first property).',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'Name of a chunk serving as resource template for the first resource (see first property).',
              'area_trans' => '',
            ),
            'tplLast' => 
            array (
              'name' => 'tplLast',
              'desc' => 'Name of a chunk serving as resource template for the last resource (see last property).',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'Name of a chunk serving as resource template for the last resource (see last property).',
              'area_trans' => '',
            ),
            'tplWrapper' => 
            array (
              'name' => 'tplWrapper',
              'desc' => 'Name of a chunk serving as wrapper template for the Snippet output. This does not work with toSeparatePlaceholders.',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'Name of a chunk serving as wrapper template for the Snippet output. This does not work with toSeparatePlaceholders.',
              'area_trans' => '',
            ),
            'wrapIfEmpty' => 
            array (
              'name' => 'wrapIfEmpty',
              'desc' => 'Indicates if empty output should be wrapped by the tplWrapper, if specified. Defaults to false.',
              'type' => 'combo-boolean',
              'options' => '',
              'value' => false,
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'Indicates if empty output should be wrapped by the tplWrapper, if specified. Defaults to false.',
              'area_trans' => '',
            ),
            'sortby' => 
            array (
              'name' => 'sortby',
              'desc' => 'A field name to sort by or JSON object of field names and sortdir for each field, e.g. {"publishedon":"ASC","createdon":"DESC"}. Defaults to publishedon.',
              'type' => 'textfield',
              'options' => '',
              'value' => 'publishedon',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'A field name to sort by or JSON object of field names and sortdir for each field, e.g. {"publishedon":"ASC","createdon":"DESC"}. Defaults to publishedon.',
              'area_trans' => '',
            ),
            'sortbyTV' => 
            array (
              'name' => 'sortbyTV',
              'desc' => 'Name of a Template Variable to sort by. Defaults to empty string.',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'Name of a Template Variable to sort by. Defaults to empty string.',
              'area_trans' => '',
            ),
            'sortbyTVType' => 
            array (
              'name' => 'sortbyTVType',
              'desc' => 'An optional type to indicate how to sort on the Template Variable value.',
              'type' => 'list',
              'options' => 
              array (
                0 => 
                array (
                  'text' => 'string',
                  'value' => 'string',
                  'name' => 'String',
                ),
                1 => 
                array (
                  'text' => 'integer',
                  'value' => 'integer',
                  'name' => 'integer',
                ),
                2 => 
                array (
                  'text' => 'decimal',
                  'value' => 'decimal',
                  'name' => 'decimal',
                ),
                3 => 
                array (
                  'text' => 'datetime',
                  'value' => 'datetime',
                  'name' => 'datetime',
                ),
              ),
              'value' => 'string',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'An optional type to indicate how to sort on the Template Variable value.',
              'area_trans' => '',
            ),
            'sortbyAlias' => 
            array (
              'name' => 'sortbyAlias',
              'desc' => 'Query alias for sortby field. Defaults to an empty string.',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'Query alias for sortby field. Defaults to an empty string.',
              'area_trans' => '',
            ),
            'sortbyEscaped' => 
            array (
              'name' => 'sortbyEscaped',
              'desc' => 'Determines if the field name specified in sortby should be escaped. Defaults to 0.',
              'type' => 'textfield',
              'options' => '',
              'value' => '0',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'Determines if the field name specified in sortby should be escaped. Defaults to 0.',
              'area_trans' => '',
            ),
            'sortdir' => 
            array (
              'name' => 'sortdir',
              'desc' => 'Order which to sort by. Defaults to DESC.',
              'type' => 'list',
              'options' => 
              array (
                0 => 
                array (
                  'text' => 'ASC',
                  'value' => 'ASC',
                  'name' => 'ASC',
                ),
                1 => 
                array (
                  'text' => 'DESC',
                  'value' => 'DESC',
                  'name' => 'DESC',
                ),
              ),
              'value' => 'DESC',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'Order which to sort by. Defaults to DESC.',
              'area_trans' => '',
            ),
            'sortdirTV' => 
            array (
              'name' => 'sortdirTV',
              'desc' => 'Order which to sort a Template Variable by. Defaults to DESC.',
              'type' => 'list',
              'options' => 
              array (
                0 => 
                array (
                  'text' => 'ASC',
                  'value' => 'ASC',
                  'name' => 'ASC',
                ),
                1 => 
                array (
                  'text' => 'DESC',
                  'value' => 'DESC',
                  'name' => 'DESC',
                ),
              ),
              'value' => 'DESC',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'Order which to sort a Template Variable by. Defaults to DESC.',
              'area_trans' => '',
            ),
            'limit' => 
            array (
              'name' => 'limit',
              'desc' => 'Limits the number of resources returned. Defaults to 5.',
              'type' => 'textfield',
              'options' => '',
              'value' => '5',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'Limits the number of resources returned. Defaults to 5.',
              'area_trans' => '',
            ),
            'offset' => 
            array (
              'name' => 'offset',
              'desc' => 'An offset of resources returned by the criteria to skip.',
              'type' => 'textfield',
              'options' => '',
              'value' => '0',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'An offset of resources returned by the criteria to skip.',
              'area_trans' => '',
            ),
            'tvFilters' => 
            array (
              'name' => 'tvFilters',
              'desc' => 'Delimited-list of TemplateVar values to filter resources by. Supports two delimiters and two value search formats. THe first delimiter || represents a logical OR and the primary grouping mechanism.  Within each group you can provide a comma-delimited list of values. These values can be either tied to a specific TemplateVar by name, e.g. myTV==value, or just the value, indicating you are searching for the value in any TemplateVar tied to the Resource. An example would be &tvFilters=`filter2==one,filter1==bar%||filter1==foo`. <br />NOTE: filtering by values uses a LIKE query and % is considered a wildcard. <br />ANOTHER NOTE: This only looks at the raw value set for specific Resource, i. e. there must be a value specifically set for the Resource and it is not evaluated.',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'Delimited-list of TemplateVar values to filter resources by. Supports two delimiters and two value search formats. THe first delimiter || represents a logical OR and the primary grouping mechanism.  Within each group you can provide a comma-delimited list of values. These values can be either tied to a specific TemplateVar by name, e.g. myTV==value, or just the value, indicating you are searching for the value in any TemplateVar tied to the Resource. An example would be &tvFilters=`filter2==one,filter1==bar%||filter1==foo`. <br />NOTE: filtering by values uses a LIKE query and % is considered a wildcard. <br />ANOTHER NOTE: This only looks at the raw value set for specific Resource, i. e. there must be a value specifically set for the Resource and it is not evaluated.',
              'area_trans' => '',
            ),
            'tvFiltersAndDelimiter' => 
            array (
              'name' => 'tvFiltersAndDelimiter',
              'desc' => 'The delimiter to use to separate logical AND expressions in tvFilters. Default is ,',
              'type' => 'textfield',
              'options' => '',
              'value' => ',',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'The delimiter to use to separate logical AND expressions in tvFilters. Default is ,',
              'area_trans' => '',
            ),
            'tvFiltersOrDelimiter' => 
            array (
              'name' => 'tvFiltersOrDelimiter',
              'desc' => 'The delimiter to use to separate logical OR expressions in tvFilters. Default is ||',
              'type' => 'textfield',
              'options' => '',
              'value' => '||',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'The delimiter to use to separate logical OR expressions in tvFilters. Default is ||',
              'area_trans' => '',
            ),
            'depth' => 
            array (
              'name' => 'depth',
              'desc' => 'Integer value indicating depth to search for resources from each parent. Defaults to 10.',
              'type' => 'textfield',
              'options' => '',
              'value' => '10',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'Integer value indicating depth to search for resources from each parent. Defaults to 10.',
              'area_trans' => '',
            ),
            'parents' => 
            array (
              'name' => 'parents',
              'desc' => 'Optional. Comma-delimited list of ids serving as parents.',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'Optional. Comma-delimited list of ids serving as parents.',
              'area_trans' => '',
            ),
            'includeContent' => 
            array (
              'name' => 'includeContent',
              'desc' => 'Indicates if the content of each resource should be returned in the results. Defaults to false.',
              'type' => 'combo-boolean',
              'options' => '',
              'value' => false,
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'Indicates if the content of each resource should be returned in the results. Defaults to false.',
              'area_trans' => '',
            ),
            'includeTVs' => 
            array (
              'name' => 'includeTVs',
              'desc' => 'Indicates if TemplateVar values should be included in the properties available to each resource template. Defaults to false.',
              'type' => 'combo-boolean',
              'options' => '',
              'value' => false,
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'Indicates if TemplateVar values should be included in the properties available to each resource template. Defaults to false.',
              'area_trans' => '',
            ),
            'includeTVList' => 
            array (
              'name' => 'includeTVList',
              'desc' => 'Limits included TVs to those specified as a comma-delimited list of TV names. Defaults to empty.',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'Limits included TVs to those specified as a comma-delimited list of TV names. Defaults to empty.',
              'area_trans' => '',
            ),
            'showHidden' => 
            array (
              'name' => 'showHidden',
              'desc' => 'Indicates if Resources that are hidden from menus should be shown. Defaults to false.',
              'type' => 'combo-boolean',
              'options' => '',
              'value' => false,
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'Indicates if Resources that are hidden from menus should be shown. Defaults to false.',
              'area_trans' => '',
            ),
            'showUnpublished' => 
            array (
              'name' => 'showUnpublished',
              'desc' => 'Indicates if Resources that are unpublished should be shown. Defaults to false.',
              'type' => 'combo-boolean',
              'options' => '',
              'value' => false,
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'Indicates if Resources that are unpublished should be shown. Defaults to false.',
              'area_trans' => '',
            ),
            'showDeleted' => 
            array (
              'name' => 'showDeleted',
              'desc' => 'Indicates if Resources that are deleted should be shown. Defaults to false.',
              'type' => 'combo-boolean',
              'options' => '',
              'value' => false,
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'Indicates if Resources that are deleted should be shown. Defaults to false.',
              'area_trans' => '',
            ),
            'resources' => 
            array (
              'name' => 'resources',
              'desc' => 'A comma-separated list of resource IDs to exclude or include. IDs with a - in front mean to exclude. Ex: 123,-456 means to include Resource 123, but always exclude Resource 456.',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'A comma-separated list of resource IDs to exclude or include. IDs with a - in front mean to exclude. Ex: 123,-456 means to include Resource 123, but always exclude Resource 456.',
              'area_trans' => '',
            ),
            'processTVs' => 
            array (
              'name' => 'processTVs',
              'desc' => 'Indicates if TemplateVar values should be rendered as they would on the resource being summarized. Defaults to false.',
              'type' => 'combo-boolean',
              'options' => '',
              'value' => false,
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'Indicates if TemplateVar values should be rendered as they would on the resource being summarized. Defaults to false.',
              'area_trans' => '',
            ),
            'processTVList' => 
            array (
              'name' => 'processTVList',
              'desc' => 'Limits processed TVs to those specified as a comma-delimited list of TV names; note only includedTVs will be available for processing if specified. Defaults to empty.',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'Limits processed TVs to those specified as a comma-delimited list of TV names; note only includedTVs will be available for processing if specified. Defaults to empty.',
              'area_trans' => '',
            ),
            'prepareTVs' => 
            array (
              'name' => 'prepareTVs',
              'desc' => 'Indicates if TemplateVar values that are not processed fully should be prepared before being returned. Defaults to true.',
              'type' => 'combo-boolean',
              'options' => '',
              'value' => true,
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'Indicates if TemplateVar values that are not processed fully should be prepared before being returned. Defaults to true.',
              'area_trans' => '',
            ),
            'prepareTVList' => 
            array (
              'name' => 'prepareTVList',
              'desc' => 'Limits prepared TVs to those specified as a comma-delimited list of TV names; note only includedTVs will be available for preparing if specified. Defaults to empty.',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'Limits prepared TVs to those specified as a comma-delimited list of TV names; note only includedTVs will be available for preparing if specified. Defaults to empty.',
              'area_trans' => '',
            ),
            'tvPrefix' => 
            array (
              'name' => 'tvPrefix',
              'desc' => 'The prefix for TemplateVar properties. Defaults to: tv.',
              'type' => 'textfield',
              'options' => '',
              'value' => 'tv.',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'The prefix for TemplateVar properties. Defaults to: tv.',
              'area_trans' => '',
            ),
            'idx' => 
            array (
              'name' => 'idx',
              'desc' => 'You can define the starting idx of the resources, which is an property that is incremented as each resource is rendered.',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'You can define the starting idx of the resources, which is an property that is incremented as each resource is rendered.',
              'area_trans' => '',
            ),
            'first' => 
            array (
              'name' => 'first',
              'desc' => 'Define the idx which represents the first resource (see tplFirst). Defaults to 1.',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'Define the idx which represents the first resource (see tplFirst). Defaults to 1.',
              'area_trans' => '',
            ),
            'last' => 
            array (
              'name' => 'last',
              'desc' => 'Define the idx which represents the last resource (see tplLast). Defaults to the number of resources being summarized + first - 1',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'Define the idx which represents the last resource (see tplLast). Defaults to the number of resources being summarized + first - 1',
              'area_trans' => '',
            ),
            'toPlaceholder' => 
            array (
              'name' => 'toPlaceholder',
              'desc' => 'If set, will assign the result to this placeholder instead of outputting it directly.',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'If set, will assign the result to this placeholder instead of outputting it directly.',
              'area_trans' => '',
            ),
            'toSeparatePlaceholders' => 
            array (
              'name' => 'toSeparatePlaceholders',
              'desc' => 'If set, will assign EACH result to a separate placeholder named by this param suffixed with a sequential number (starting from 0).',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'If set, will assign EACH result to a separate placeholder named by this param suffixed with a sequential number (starting from 0).',
              'area_trans' => '',
            ),
            'debug' => 
            array (
              'name' => 'debug',
              'desc' => 'If true, will send the SQL query to the MODX log. Defaults to false.',
              'type' => 'combo-boolean',
              'options' => '',
              'value' => false,
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'If true, will send the SQL query to the MODX log. Defaults to false.',
              'area_trans' => '',
            ),
            'where' => 
            array (
              'name' => 'where',
              'desc' => 'A JSON expression of criteria to build any additional where clauses from, e.g. &where=`{{"alias:LIKE":"foo%", "OR:alias:LIKE":"%bar"},{"OR:pagetitle:=":"foobar", "AND:description:=":"raboof"}}`',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'A JSON expression of criteria to build any additional where clauses from, e.g. &where=`{{"alias:LIKE":"foo%", "OR:alias:LIKE":"%bar"},{"OR:pagetitle:=":"foobar", "AND:description:=":"raboof"}}`',
              'area_trans' => '',
            ),
            'dbCacheFlag' => 
            array (
              'name' => 'dbCacheFlag',
              'desc' => 'Determines how result sets are cached if cache_db is enabled in MODX. 0|false = do not cache result set; 1 = cache result set according to cache settings, any other integer value = number of seconds to cache result set',
              'type' => 'textfield',
              'options' => '',
              'value' => '0',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'Determines how result sets are cached if cache_db is enabled in MODX. 0|false = do not cache result set; 1 = cache result set according to cache settings, any other integer value = number of seconds to cache result set',
              'area_trans' => '',
            ),
            'context' => 
            array (
              'name' => 'context',
              'desc' => 'A comma-delimited list of context keys for limiting results. Default is empty, i.e. do not limit results by context.',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'A comma-delimited list of context keys for limiting results. Default is empty, i.e. do not limit results by context.',
              'area_trans' => '',
            ),
            'tplCondition' => 
            array (
              'name' => 'tplCondition',
              'desc' => 'A condition to compare against the conditionalTpls property to map Resources to different tpls based on custom conditional logic.',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'A condition to compare against the conditionalTpls property to map Resources to different tpls based on custom conditional logic.',
              'area_trans' => '',
            ),
            'tplOperator' => 
            array (
              'name' => 'tplOperator',
              'desc' => 'An optional operator to use for the tplCondition when comparing against the conditionalTpls operands. Default is == (equals).',
              'type' => 'list',
              'options' => 
              array (
                0 => 
                array (
                  'text' => 'is equal to',
                  'value' => '==',
                  'name' => 'is equal to',
                ),
                1 => 
                array (
                  'text' => 'is not equal to',
                  'value' => '!=',
                  'name' => 'is not equal to',
                ),
                2 => 
                array (
                  'text' => 'less than',
                  'value' => '<',
                  'name' => 'less than',
                ),
                3 => 
                array (
                  'text' => 'less than or equal to',
                  'value' => '<=',
                  'name' => 'less than or equal to',
                ),
                4 => 
                array (
                  'text' => 'greater than or equal to',
                  'value' => '>=',
                  'name' => 'greater than or equal to',
                ),
                5 => 
                array (
                  'text' => 'is empty',
                  'value' => 'empty',
                  'name' => 'is empty',
                ),
                6 => 
                array (
                  'text' => 'is not empty',
                  'value' => '!empty',
                  'name' => 'is not empty',
                ),
                7 => 
                array (
                  'text' => 'is null',
                  'value' => 'null',
                  'name' => 'is null',
                ),
                8 => 
                array (
                  'text' => 'is in array',
                  'value' => 'inarray',
                  'name' => 'is in array',
                ),
                9 => 
                array (
                  'text' => 'is between',
                  'value' => 'between',
                  'name' => 'is between',
                ),
              ),
              'value' => '==',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'An optional operator to use for the tplCondition when comparing against the conditionalTpls operands. Default is == (equals).',
              'area_trans' => '',
            ),
            'conditionalTpls' => 
            array (
              'name' => 'conditionalTpls',
              'desc' => 'A JSON map of conditional operands and tpls to compare against the tplCondition property using the specified tplOperator.',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'A JSON map of conditional operands and tpls to compare against the tplCondition property using the specified tplOperator.',
              'area_trans' => '',
            ),
          ),
          'moduleguid' => '',
          'static' => false,
          'static_file' => '',
          'content' => '/**
 * getResources
 *
 * A general purpose Resource listing and summarization snippet for MODX 2.x.
 *
 * @author Jason Coward
 * @copyright Copyright 2010-2013, Jason Coward
 *
 * TEMPLATES
 *
 * tpl - Name of a chunk serving as a resource template
 * [NOTE: if not provided, properties are dumped to output for each resource]
 *
 * tplOdd - (Opt) Name of a chunk serving as resource template for resources with an odd idx value
 * (see idx property)
 * tplFirst - (Opt) Name of a chunk serving as resource template for the first resource (see first
 * property)
 * tplLast - (Opt) Name of a chunk serving as resource template for the last resource (see last
 * property)
 * tpl_{n} - (Opt) Name of a chunk serving as resource template for the nth resource
 *
 * tplCondition - (Opt) Defines a field of the resource to evaluate against keys defined in the
 * conditionalTpls property. Must be a resource field; does not work with Template Variables.
 * conditionalTpls - (Opt) A JSON object defining a map of field values and the associated tpl to
 * use when the field defined by tplCondition matches the value. [NOTE: tplOdd, tplFirst, tplLast,
 * and tpl_{n} will take precedence over any defined conditionalTpls]
 *
 * tplWrapper - (Opt) Name of a chunk serving as a wrapper template for the output
 * [NOTE: Does not work with toSeparatePlaceholders]
 *
 * SELECTION
 *
 * parents - Comma-delimited list of ids serving as parents
 *
 * context - (Opt) Comma-delimited list of context keys to limit results by; if empty, contexts for all specified
 * parents will be used (all contexts if 0 is specified) [default=]
 *
 * depth - (Opt) Integer value indicating depth to search for resources from each parent [default=10]
 *
 * tvFilters - (Opt) Delimited-list of TemplateVar values to filter resources by. Supports two
 * delimiters and two value search formats. The first delimiter || represents a logical OR and the
 * primary grouping mechanism.  Within each group you can provide a comma-delimited list of values.
 * These values can be either tied to a specific TemplateVar by name, e.g. myTV==value, or just the
 * value, indicating you are searching for the value in any TemplateVar tied to the Resource. An
 * example would be &tvFilters=`filter2==one,filter1==bar%||filter1==foo`
 * [NOTE: filtering by values uses a LIKE query and % is considered a wildcard.]
 * [NOTE: this only looks at the raw value set for specific Resource, i. e. there must be a value
 * specifically set for the Resource and it is not evaluated.]
 *
 * tvFiltersAndDelimiter - (Opt) Custom delimiter for logical AND, default \',\', in case you want to
 * match a literal comma in the tvFilters. E.g. &tvFiltersAndDelimiter=`&&`
 * &tvFilters=`filter1==foo,bar&&filter2==baz` [default=,]
 *
 * tvFiltersOrDelimiter - (Opt) Custom delimiter for logical OR, default \'||\', in case you want to
 * match a literal \'||\' in the tvFilters. E.g. &tvFiltersOrDelimiter=`|OR|`
 * &tvFilters=`filter1==foo||bar|OR|filter2==baz` [default=||]
 *
 * where - (Opt) A JSON expression of criteria to build any additional where clauses from. An example would be
 * &where=`{{"alias:LIKE":"foo%", "OR:alias:LIKE":"%bar"},{"OR:pagetitle:=":"foobar", "AND:description:=":"raboof"}}`
 *
 * sortby - (Opt) Field to sort by or a JSON array, e.g. {"publishedon":"ASC","createdon":"DESC"} [default=publishedon]
 * sortbyTV - (opt) A Template Variable name to sort by (if supplied, this precedes the sortby value) [default=]
 * sortbyTVType - (Opt) A data type to CAST a TV Value to in order to sort on it properly [default=string]
 * sortbyAlias - (Opt) Query alias for sortby field [default=]
 * sortbyEscaped - (Opt) Escapes the field name(s) specified in sortby [default=0]
 * sortdir - (Opt) Order which to sort by [default=DESC]
 * sortdirTV - (Opt) Order which to sort by a TV [default=DESC]
 * limit - (Opt) Limits the number of resources returned [default=5]
 * offset - (Opt) An offset of resources returned by the criteria to skip [default=0]
 * dbCacheFlag - (Opt) Controls caching of db queries; 0|false = do not cache result set; 1 = cache result set
 * according to cache settings, any other integer value = number of seconds to cache result set [default=0]
 *
 * OPTIONS
 *
 * includeContent - (Opt) Indicates if the content of each resource should be returned in the
 * results [default=0]
 * includeTVs - (Opt) Indicates if TemplateVar values should be included in the properties available
 * to each resource template [default=0]
 * includeTVList - (Opt) Limits the TemplateVars that are included if includeTVs is true to those specified
 * by name in a comma-delimited list [default=]
 * prepareTVs - (Opt) Prepares media-source dependent TemplateVar values [default=1]
 * prepareTVList - (Opt) Limits the TVs that are prepared to those specified by name in a comma-delimited
 * list [default=]
 * processTVs - (Opt) Indicates if TemplateVar values should be rendered as they would on the
 * resource being summarized [default=0]
 * processTVList - (opt) Limits the TemplateVars that are processed if included to those specified
 * by name in a comma-delimited list [default=]
 * tvPrefix - (Opt) The prefix for TemplateVar properties [default=tv.]
 * idx - (Opt) You can define the starting idx of the resources, which is an property that is
 * incremented as each resource is rendered [default=1]
 * first - (Opt) Define the idx which represents the first resource (see tplFirst) [default=1]
 * last - (Opt) Define the idx which represents the last resource (see tplLast) [default=# of
 * resources being summarized + first - 1]
 * outputSeparator - (Opt) An optional string to separate each tpl instance [default="\\n"]
 * wrapIfEmpty - (Opt) Indicates if the tplWrapper should be applied if the output is empty [default=0]
 *
 */
$output = array();
$outputSeparator = isset($outputSeparator) ? $outputSeparator : "\\n";

/* set default properties */
$tpl = !empty($tpl) ? $tpl : \'\';
$includeContent = !empty($includeContent) ? true : false;
$includeTVs = !empty($includeTVs) ? true : false;
$includeTVList = !empty($includeTVList) ? explode(\',\', $includeTVList) : array();
$processTVs = !empty($processTVs) ? true : false;
$processTVList = !empty($processTVList) ? explode(\',\', $processTVList) : array();
$prepareTVs = !empty($prepareTVs) ? true : false;
$prepareTVList = !empty($prepareTVList) ? explode(\',\', $prepareTVList) : array();
$tvPrefix = isset($tvPrefix) ? $tvPrefix : \'tv.\';
$parents = (!empty($parents) || $parents === \'0\') ? explode(\',\', $parents) : array($modx->resource->get(\'id\'));
array_walk($parents, \'trim\');
$parents = array_unique($parents);
$depth = isset($depth) ? (integer) $depth : 10;

$tvFiltersOrDelimiter = isset($tvFiltersOrDelimiter) ? $tvFiltersOrDelimiter : \'||\';
$tvFiltersAndDelimiter = isset($tvFiltersAndDelimiter) ? $tvFiltersAndDelimiter : \',\';
$tvFilters = !empty($tvFilters) ? explode($tvFiltersOrDelimiter, $tvFilters) : array();

$where = !empty($where) ? $modx->fromJSON($where) : array();
$showUnpublished = !empty($showUnpublished) ? true : false;
$showDeleted = !empty($showDeleted) ? true : false;

$sortby = isset($sortby) ? $sortby : \'publishedon\';
$sortbyTV = isset($sortbyTV) ? $sortbyTV : \'\';
$sortbyAlias = isset($sortbyAlias) ? $sortbyAlias : \'modResource\';
$sortbyEscaped = !empty($sortbyEscaped) ? true : false;
$sortdir = isset($sortdir) ? $sortdir : \'DESC\';
$sortdirTV = isset($sortdirTV) ? $sortdirTV : \'DESC\';
$limit = isset($limit) ? (integer) $limit : 5;
$offset = isset($offset) ? (integer) $offset : 0;
$totalVar = !empty($totalVar) ? $totalVar : \'total\';

$dbCacheFlag = !isset($dbCacheFlag) ? false : $dbCacheFlag;
if (is_string($dbCacheFlag) || is_numeric($dbCacheFlag)) {
    if ($dbCacheFlag == \'0\') {
        $dbCacheFlag = false;
    } elseif ($dbCacheFlag == \'1\') {
        $dbCacheFlag = true;
    } else {
        $dbCacheFlag = (integer) $dbCacheFlag;
    }
}

/* multiple context support */
$contextArray = array();
$contextSpecified = false;
if (!empty($context)) {
    $contextArray = explode(\',\',$context);
    array_walk($contextArray, \'trim\');
    $contexts = array();
    foreach ($contextArray as $ctx) {
        $contexts[] = $modx->quote($ctx);
    }
    $context = implode(\',\',$contexts);
    $contextSpecified = true;
    unset($contexts,$ctx);
} else {
    $context = $modx->quote($modx->context->get(\'key\'));
}

$pcMap = array();
$pcQuery = $modx->newQuery(\'modResource\', array(\'id:IN\' => $parents), $dbCacheFlag);
$pcQuery->select(array(\'id\', \'context_key\'));
if ($pcQuery->prepare() && $pcQuery->stmt->execute()) {
    foreach ($pcQuery->stmt->fetchAll(PDO::FETCH_ASSOC) as $pcRow) {
        $pcMap[(integer) $pcRow[\'id\']] = $pcRow[\'context_key\'];
    }
}

$children = array();
$parentArray = array();
foreach ($parents as $parent) {
    $parent = (integer) $parent;
    if ($parent === 0) {
        $pchildren = array();
        if ($contextSpecified) {
            foreach ($contextArray as $pCtx) {
                if (!in_array($pCtx, $contextArray)) {
                    continue;
                }
                $options = $pCtx !== $modx->context->get(\'key\') ? array(\'context\' => $pCtx) : array();
                $pcchildren = $modx->getChildIds($parent, $depth, $options);
                if (!empty($pcchildren)) $pchildren = array_merge($pchildren, $pcchildren);
            }
        } else {
            $cQuery = $modx->newQuery(\'modContext\', array(\'key:!=\' => \'mgr\'));
            $cQuery->select(array(\'key\'));
            if ($cQuery->prepare() && $cQuery->stmt->execute()) {
                foreach ($cQuery->stmt->fetchAll(PDO::FETCH_COLUMN) as $pCtx) {
                    $options = $pCtx !== $modx->context->get(\'key\') ? array(\'context\' => $pCtx) : array();
                    $pcchildren = $modx->getChildIds($parent, $depth, $options);
                    if (!empty($pcchildren)) $pchildren = array_merge($pchildren, $pcchildren);
                }
            }
        }
        $parentArray[] = $parent;
    } else {
        $pContext = array_key_exists($parent, $pcMap) ? $pcMap[$parent] : false;
        if ($debug) $modx->log(modX::LOG_LEVEL_ERROR, "context for {$parent} is {$pContext}");
        if ($pContext && $contextSpecified && !in_array($pContext, $contextArray, true)) {
            $parent = next($parents);
            continue;
        }
        $parentArray[] = $parent;
        $options = !empty($pContext) && $pContext !== $modx->context->get(\'key\') ? array(\'context\' => $pContext) : array();
        $pchildren = $modx->getChildIds($parent, $depth, $options);
    }
    if (!empty($pchildren)) $children = array_merge($children, $pchildren);
    $parent = next($parents);
}
$parents = array_merge($parentArray, $children);

/* build query */
$criteria = array("modResource.parent IN (" . implode(\',\', $parents) . ")");
if ($contextSpecified) {
    $contextResourceTbl = $modx->getTableName(\'modContextResource\');
    $criteria[] = "(modResource.context_key IN ({$context}) OR EXISTS(SELECT 1 FROM {$contextResourceTbl} ctx WHERE ctx.resource = modResource.id AND ctx.context_key IN ({$context})))";
}
if (empty($showDeleted)) {
    $criteria[\'deleted\'] = \'0\';
}
if (empty($showUnpublished)) {
    $criteria[\'published\'] = \'1\';
}
if (empty($showHidden)) {
    $criteria[\'hidemenu\'] = \'0\';
}
if (!empty($hideContainers)) {
    $criteria[\'isfolder\'] = \'0\';
}
$criteria = $modx->newQuery(\'modResource\', $criteria);
if (!empty($tvFilters)) {
    $tmplVarTbl = $modx->getTableName(\'modTemplateVar\');
    $tmplVarResourceTbl = $modx->getTableName(\'modTemplateVarResource\');
    $conditions = array();
    $operators = array(
        \'<=>\' => \'<=>\',
        \'===\' => \'=\',
        \'!==\' => \'!=\',
        \'<>\' => \'<>\',
        \'==\' => \'LIKE\',
        \'!=\' => \'NOT LIKE\',
        \'<<\' => \'<\',
        \'<=\' => \'<=\',
        \'=<\' => \'=<\',
        \'>>\' => \'>\',
        \'>=\' => \'>=\',
        \'=>\' => \'=>\'
    );
    foreach ($tvFilters as $fGroup => $tvFilter) {
        $filterGroup = array();
        $filters = explode($tvFiltersAndDelimiter, $tvFilter);
        $multiple = count($filters) > 0;
        foreach ($filters as $filter) {
            $operator = \'==\';
            $sqlOperator = \'LIKE\';
            foreach ($operators as $op => $opSymbol) {
                if (strpos($filter, $op, 1) !== false) {
                    $operator = $op;
                    $sqlOperator = $opSymbol;
                    break;
                }
            }
            $tvValueField = \'tvr.value\';
            $tvDefaultField = \'tv.default_text\';
            $f = explode($operator, $filter);
            if (count($f) >= 2) {
                if (count($f) > 2) {
                    $k = array_shift($f);
                    $b = join($operator, $f);
                    $f = array($k, $b);
                }
                $tvName = $modx->quote($f[0]);
                if (is_numeric($f[1]) && !in_array($sqlOperator, array(\'LIKE\', \'NOT LIKE\'))) {
                    $tvValue = $f[1];
                    if ($f[1] == (integer)$f[1]) {
                        $tvValueField = "CAST({$tvValueField} AS SIGNED INTEGER)";
                        $tvDefaultField = "CAST({$tvDefaultField} AS SIGNED INTEGER)";
                    } else {
                        $tvValueField = "CAST({$tvValueField} AS DECIMAL)";
                        $tvDefaultField = "CAST({$tvDefaultField} AS DECIMAL)";
                    }
                } else {
                    $tvValue = $modx->quote($f[1]);
                }
                if ($multiple) {
                    $filterGroup[] =
                        "(EXISTS (SELECT 1 FROM {$tmplVarResourceTbl} tvr JOIN {$tmplVarTbl} tv ON {$tvValueField} {$sqlOperator} {$tvValue} AND tv.name = {$tvName} AND tv.id = tvr.tmplvarid WHERE tvr.contentid = modResource.id) " .
                        "OR EXISTS (SELECT 1 FROM {$tmplVarTbl} tv WHERE tv.name = {$tvName} AND {$tvDefaultField} {$sqlOperator} {$tvValue} AND tv.id NOT IN (SELECT tmplvarid FROM {$tmplVarResourceTbl} WHERE contentid = modResource.id)) " .
                        ")";
                } else {
                    $filterGroup =
                        "(EXISTS (SELECT 1 FROM {$tmplVarResourceTbl} tvr JOIN {$tmplVarTbl} tv ON {$tvValueField} {$sqlOperator} {$tvValue} AND tv.name = {$tvName} AND tv.id = tvr.tmplvarid WHERE tvr.contentid = modResource.id) " .
                        "OR EXISTS (SELECT 1 FROM {$tmplVarTbl} tv WHERE tv.name = {$tvName} AND {$tvDefaultField} {$sqlOperator} {$tvValue} AND tv.id NOT IN (SELECT tmplvarid FROM {$tmplVarResourceTbl} WHERE contentid = modResource.id)) " .
                        ")";
                }
            } elseif (count($f) == 1) {
                $tvValue = $modx->quote($f[0]);
                if ($multiple) {
                    $filterGroup[] = "EXISTS (SELECT 1 FROM {$tmplVarResourceTbl} tvr JOIN {$tmplVarTbl} tv ON {$tvValueField} {$sqlOperator} {$tvValue} AND tv.id = tvr.tmplvarid WHERE tvr.contentid = modResource.id)";
                } else {
                    $filterGroup = "EXISTS (SELECT 1 FROM {$tmplVarResourceTbl} tvr JOIN {$tmplVarTbl} tv ON {$tvValueField} {$sqlOperator} {$tvValue} AND tv.id = tvr.tmplvarid WHERE tvr.contentid = modResource.id)";
                }
            }
        }
        $conditions[] = $filterGroup;
    }
    if (!empty($conditions)) {
        $firstGroup = true;
        foreach ($conditions as $cGroup => $c) {
            if (is_array($c)) {
                $first = true;
                foreach ($c as $cond) {
                    if ($first && !$firstGroup) {
                        $criteria->condition($criteria->query[\'where\'][0][1], $cond, xPDOQuery::SQL_OR, null, $cGroup);
                    } else {
                        $criteria->condition($criteria->query[\'where\'][0][1], $cond, xPDOQuery::SQL_AND, null, $cGroup);
                    }
                    $first = false;
                }
            } else {
                $criteria->condition($criteria->query[\'where\'][0][1], $c, $firstGroup ? xPDOQuery::SQL_AND : xPDOQuery::SQL_OR, null, $cGroup);
            }
            $firstGroup = false;
        }
    }
}
/* include/exclude resources, via &resources=`123,-456` prop */
if (!empty($resources)) {
    $resourceConditions = array();
    $resources = explode(\',\',$resources);
    $include = array();
    $exclude = array();
    foreach ($resources as $resource) {
        $resource = (int)$resource;
        if ($resource == 0) continue;
        if ($resource < 0) {
            $exclude[] = abs($resource);
        } else {
            $include[] = $resource;
        }
    }
    if (!empty($include)) {
        $criteria->where(array(\'OR:modResource.id:IN\' => $include), xPDOQuery::SQL_OR);
    }
    if (!empty($exclude)) {
        $criteria->where(array(\'modResource.id:NOT IN\' => $exclude), xPDOQuery::SQL_AND, null, 1);
    }
}
if (!empty($where)) {
    $criteria->where($where);
}

$total = $modx->getCount(\'modResource\', $criteria);
$modx->setPlaceholder($totalVar, $total);

$fields = array_keys($modx->getFields(\'modResource\'));
if (empty($includeContent)) {
    $fields = array_diff($fields, array(\'content\'));
}
$columns = $includeContent ? $modx->getSelectColumns(\'modResource\', \'modResource\') : $modx->getSelectColumns(\'modResource\', \'modResource\', \'\', array(\'content\'), true);
$criteria->select($columns);
if (!empty($sortbyTV)) {
    $criteria->leftJoin(\'modTemplateVar\', \'tvDefault\', array(
        "tvDefault.name" => $sortbyTV
    ));
    $criteria->leftJoin(\'modTemplateVarResource\', \'tvSort\', array(
        "tvSort.contentid = modResource.id",
        "tvSort.tmplvarid = tvDefault.id"
    ));
    if (empty($sortbyTVType)) $sortbyTVType = \'string\';
    if ($modx->getOption(\'dbtype\') === \'mysql\') {
        switch ($sortbyTVType) {
            case \'integer\':
                $criteria->select("CAST(IFNULL(tvSort.value, tvDefault.default_text) AS SIGNED INTEGER) AS sortTV");
                break;
            case \'decimal\':
                $criteria->select("CAST(IFNULL(tvSort.value, tvDefault.default_text) AS DECIMAL) AS sortTV");
                break;
            case \'datetime\':
                $criteria->select("CAST(IFNULL(tvSort.value, tvDefault.default_text) AS DATETIME) AS sortTV");
                break;
            case \'string\':
            default:
                $criteria->select("IFNULL(tvSort.value, tvDefault.default_text) AS sortTV");
                break;
        }
    } elseif ($modx->getOption(\'dbtype\') === \'sqlsrv\') {
        switch ($sortbyTVType) {
            case \'integer\':
                $criteria->select("CAST(ISNULL(tvSort.value, tvDefault.default_text) AS BIGINT) AS sortTV");
                break;
            case \'decimal\':
                $criteria->select("CAST(ISNULL(tvSort.value, tvDefault.default_text) AS DECIMAL) AS sortTV");
                break;
            case \'datetime\':
                $criteria->select("CAST(ISNULL(tvSort.value, tvDefault.default_text) AS DATETIME) AS sortTV");
                break;
            case \'string\':
            default:
                $criteria->select("ISNULL(tvSort.value, tvDefault.default_text) AS sortTV");
                break;
        }
    }
    $criteria->sortby("sortTV", $sortdirTV);
}
if (!empty($sortby)) {
    if (strpos($sortby, \'{\') === 0) {
        $sorts = $modx->fromJSON($sortby);
    } else {
        $sorts = array($sortby => $sortdir);
    }
    if (is_array($sorts)) {
        while (list($sort, $dir) = each($sorts)) {
            if ($sortbyEscaped) $sort = $modx->escape($sort);
            if (!empty($sortbyAlias)) $sort = $modx->escape($sortbyAlias) . ".{$sort}";
            $criteria->sortby($sort, $dir);
        }
    }
}
if (!empty($limit)) $criteria->limit($limit, $offset);

if (!empty($debug)) {
    $criteria->prepare();
    $modx->log(modX::LOG_LEVEL_ERROR, $criteria->toSQL());
}
$collection = $modx->getCollection(\'modResource\', $criteria, $dbCacheFlag);

$idx = !empty($idx) || $idx === \'0\' ? (integer) $idx : 1;
$first = empty($first) && $first !== \'0\' ? 1 : (integer) $first;
$last = empty($last) ? (count($collection) + $idx - 1) : (integer) $last;

/* include parseTpl */
include_once $modx->getOption(\'getresources.core_path\',null,$modx->getOption(\'core_path\').\'components/getresources/\').\'include.parsetpl.php\';

$templateVars = array();
if (!empty($includeTVs) && !empty($includeTVList)) {
    $templateVars = $modx->getCollection(\'modTemplateVar\', array(\'name:IN\' => $includeTVList));
}
/** @var modResource $resource */
foreach ($collection as $resourceId => $resource) {
    $tvs = array();
    if (!empty($includeTVs)) {
        if (empty($includeTVList)) {
            $templateVars = $resource->getMany(\'TemplateVars\');
        }
        /** @var modTemplateVar $templateVar */
        foreach ($templateVars as $tvId => $templateVar) {
            if (!empty($includeTVList) && !in_array($templateVar->get(\'name\'), $includeTVList)) continue;
            if ($processTVs && (empty($processTVList) || in_array($templateVar->get(\'name\'), $processTVList))) {
                $tvs[$tvPrefix . $templateVar->get(\'name\')] = $templateVar->renderOutput($resource->get(\'id\'));
            } else {
                $value = $templateVar->getValue($resource->get(\'id\'));
                if ($prepareTVs && method_exists($templateVar, \'prepareOutput\') && (empty($prepareTVList) || in_array($templateVar->get(\'name\'), $prepareTVList))) {
                    $value = $templateVar->prepareOutput($value);
                }
                $tvs[$tvPrefix . $templateVar->get(\'name\')] = $value;
            }
        }
    }
    $odd = ($idx & 1);
    $properties = array_merge(
        $scriptProperties
        ,array(
            \'idx\' => $idx
            ,\'first\' => $first
            ,\'last\' => $last
            ,\'odd\' => $odd
        )
        ,$includeContent ? $resource->toArray() : $resource->get($fields)
        ,$tvs
    );
    $resourceTpl = false;
    if ($idx == $first && !empty($tplFirst)) {
        $resourceTpl = parseTpl($tplFirst, $properties);
    }
    if ($idx == $last && empty($resourceTpl) && !empty($tplLast)) {
        $resourceTpl = parseTpl($tplLast, $properties);
    }
    $tplidx = \'tpl_\' . $idx;
    if (empty($resourceTpl) && !empty($$tplidx)) {
        $resourceTpl = parseTpl($$tplidx, $properties);
    }
    if ($idx > 1 && empty($resourceTpl)) {
        $divisors = getDivisors($idx);
        if (!empty($divisors)) {
            foreach ($divisors as $divisor) {
                $tplnth = \'tpl_n\' . $divisor;
                if (!empty($$tplnth)) {
                    $resourceTpl = parseTpl($$tplnth, $properties);
                    if (!empty($resourceTpl)) {
                        break;
                    }
                }
            }
        }
    }
    if ($odd && empty($resourceTpl) && !empty($tplOdd)) {
        $resourceTpl = parseTpl($tplOdd, $properties);
    }
    if (!empty($tplCondition) && !empty($conditionalTpls) && empty($resourceTpl)) {
        $conTpls = $modx->fromJSON($conditionalTpls);
        $subject = $properties[$tplCondition];
        $tplOperator = !empty($tplOperator) ? $tplOperator : \'=\';
        $tplOperator = strtolower($tplOperator);
        $tplCon = \'\';
        foreach ($conTpls as $operand => $conditionalTpl) {
            switch ($tplOperator) {
                case \'!=\':
                case \'neq\':
                case \'not\':
                case \'isnot\':
                case \'isnt\':
                case \'unequal\':
                case \'notequal\':
                    $tplCon = (($subject != $operand) ? $conditionalTpl : $tplCon);
                    break;
                case \'<\':
                case \'lt\':
                case \'less\':
                case \'lessthan\':
                    $tplCon = (($subject < $operand) ? $conditionalTpl : $tplCon);
                    break;
                case \'>\':
                case \'gt\':
                case \'greater\':
                case \'greaterthan\':
                    $tplCon = (($subject > $operand) ? $conditionalTpl : $tplCon);
                    break;
                case \'<=\':
                case \'lte\':
                case \'lessthanequals\':
                case \'lessthanorequalto\':
                    $tplCon = (($subject <= $operand) ? $conditionalTpl : $tplCon);
                    break;
                case \'>=\':
                case \'gte\':
                case \'greaterthanequals\':
                case \'greaterthanequalto\':
                    $tplCon = (($subject >= $operand) ? $conditionalTpl : $tplCon);
                    break;
                case \'isempty\':
                case \'empty\':
                    $tplCon = empty($subject) ? $conditionalTpl : $tplCon;
                    break;
                case \'!empty\':
                case \'notempty\':
                case \'isnotempty\':
                    $tplCon = !empty($subject) && $subject != \'\' ? $conditionalTpl : $tplCon;
                    break;
                case \'isnull\':
                case \'null\':
                    $tplCon = $subject == null || strtolower($subject) == \'null\' ? $conditionalTpl : $tplCon;
                    break;
                case \'inarray\':
                case \'in_array\':
                case \'ia\':
                    $operand = explode(\',\', $operand);
                    $tplCon = in_array($subject, $operand) ? $conditionalTpl : $tplCon;
                    break;
                case \'between\':
                case \'range\':
                case \'>=<\':
                case \'><\':
                    $operand = explode(\',\', $operand);
                    $tplCon = ($subject >= min($operand) && $subject <= max($operand)) ? $conditionalTpl : $tplCon;
                    break;
                case \'==\':
                case \'=\':
                case \'eq\':
                case \'is\':
                case \'equal\':
                case \'equals\':
                case \'equalto\':
                default:
                    $tplCon = (($subject == $operand) ? $conditionalTpl : $tplCon);
                    break;
            }
        }
        if (!empty($tplCon)) {
            $resourceTpl = parseTpl($tplCon, $properties);
        }
    }
    if (!empty($tpl) && empty($resourceTpl)) {
        $resourceTpl = parseTpl($tpl, $properties);
    }
    if ($resourceTpl === false && !empty($debug)) {
        $chunk = $modx->newObject(\'modChunk\');
        $chunk->setCacheable(false);
        $output[]= $chunk->process(array(), \'<pre>\' . print_r($properties, true) .\'</pre>\');
    } else {
        $output[]= $resourceTpl;
    }
    $idx++;
}

/* output */
$toSeparatePlaceholders = $modx->getOption(\'toSeparatePlaceholders\', $scriptProperties, false);
if (!empty($toSeparatePlaceholders)) {
    $modx->setPlaceholders($output, $toSeparatePlaceholders);
    return \'\';
}

$output = implode($outputSeparator, $output);

$tplWrapper = $modx->getOption(\'tplWrapper\', $scriptProperties, false);
$wrapIfEmpty = $modx->getOption(\'wrapIfEmpty\', $scriptProperties, false);
if (!empty($tplWrapper) && ($wrapIfEmpty || !empty($output))) {
    $output = parseTpl($tplWrapper, array_merge($scriptProperties, array(\'output\' => $output)));
}

$toPlaceholder = $modx->getOption(\'toPlaceholder\', $scriptProperties, false);
if (!empty($toPlaceholder)) {
    $modx->setPlaceholder($toPlaceholder, $output);
    return \'\';
}
return $output;',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
        ),
      ),
      'FormIt' => 
      array (
        'fields' => 
        array (
          'id' => 5,
          'source' => 0,
          'property_preprocess' => false,
          'name' => 'FormIt',
          'description' => 'A dynamic form processing snippet.',
          'editor_type' => 0,
          'category' => 4,
          'cache_type' => 0,
          'snippet' => '/**
 * FormIt
 *
 * Copyright 2009-2012 by Shaun McCormick <shaun@modx.com>
 *
 * FormIt is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option) any
 * later version.
 *
 * FormIt is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * FormIt; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
 * Suite 330, Boston, MA 02111-1307 USA
 *
 * @package formit
 */
/**
 * FormIt
 *
 * A dynamic form processing Snippet for MODx Revolution.
 *
 * @var modX $modx
 * @var array $scriptProperties
 *
 * @package formit
 */

$modelPath = $modx->getOption(\'formit.core_path\', null, $modx->getOption(\'core_path\', null, MODX_CORE_PATH) . \'components/formit/\') . \'model/formit/\';
$modx->loadClass(\'FormIt\', $modelPath, true, true);

$fi = new FormIt($modx,$scriptProperties);
$fi->initialize($modx->context->get(\'key\'));
$fi->loadRequest();

$fields = $fi->request->prepare();
return $fi->request->handle($fields);',
          'locked' => false,
          'properties' => 
          array (
            'hooks' => 
            array (
              'name' => 'hooks',
              'desc' => 'prop_formit.hooks_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'What scripts to fire, if any, after the form passes validation. This can be a comma-separated list of hooks, and if the first fails, the proceeding ones will not fire. A hook can also be a Snippet name that will execute that Snippet.',
              'area_trans' => '',
            ),
            'preHooks' => 
            array (
              'name' => 'preHooks',
              'desc' => 'prop_formit.prehooks_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'What scripts to fire, if any, once the form loads. You can pre-set form fields via $scriptProperties[`hook`]->fields[`fieldname`]. This can be a comma-separated list of hooks, and if the first fails, the proceeding ones will not fire. A hook can also be a Snippet name that will execute that Snippet.',
              'area_trans' => '',
            ),
            'submitVar' => 
            array (
              'name' => 'submitVar',
              'desc' => 'prop_formit.submitvar_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'If set, will not begin form processing if this POST variable is not passed.',
              'area_trans' => '',
            ),
            'validate' => 
            array (
              'name' => 'validate',
              'desc' => 'prop_formit.validate_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'A comma-separated list of fields to validate, with each field name as name:validator (eg: username:required,email:required). Validators can also be chained, like email:email:required. This property can be specified on multiple lines.',
              'area_trans' => '',
            ),
            'errTpl' => 
            array (
              'name' => 'errTpl',
              'desc' => 'prop_formit.errtpl_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '<span class="error">[[+error]]</span>',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'The wrapper template for error messages.',
              'area_trans' => '',
            ),
            'validationErrorMessage' => 
            array (
              'name' => 'validationErrorMessage',
              'desc' => 'prop_formit.validationerrormessage_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '<p class="error">A form validation error occurred. Please check the values you have entered.</p>',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'A general error message to set to a placeholder if validation fails. Can contain [[+errors]] if you want to display a list of all errors at the top.',
              'area_trans' => '',
            ),
            'validationErrorBulkTpl' => 
            array (
              'name' => 'validationErrorBulkTpl',
              'desc' => 'prop_formit.validationerrorbulktpl_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '<li>[[+error]]</li>',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'HTML tpl that is used for each individual error in the generic validation error message value.',
              'area_trans' => '',
            ),
            'trimValuesBeforeValidation' => 
            array (
              'name' => 'trimValuesBeforeValidation',
              'desc' => 'prop_formit.trimvaluesdeforevalidation_desc',
              'type' => 'combo-boolean',
              'options' => '',
              'value' => true,
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Whether or not to trim spaces from the beginning and end of values before attempting validation. Defaults to true.',
              'area_trans' => '',
            ),
            'customValidators' => 
            array (
              'name' => 'customValidators',
              'desc' => 'prop_formit.customvalidators_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'A comma-separated list of custom validator names (snippets) you plan to use in this form. They must be explicitly stated here, or they will not be run.',
              'area_trans' => '',
            ),
            'clearFieldsOnSuccess' => 
            array (
              'name' => 'clearFieldsOnSuccess',
              'desc' => 'prop_formit.clearfieldsonsuccess_desc',
              'type' => 'combo-boolean',
              'options' => '',
              'value' => true,
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'If true, will clear the fields on a successful form submission that does not redirect.',
              'area_trans' => '',
            ),
            'successMessage' => 
            array (
              'name' => 'successMessage',
              'desc' => 'prop_formit.successmessage_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'If set, will set this a placeholder with the name of the value of the property &successMessagePlaceholder, which defaults to `fi.successMessage`.',
              'area_trans' => '',
            ),
            'successMessagePlaceholder' => 
            array (
              'name' => 'successMessagePlaceholder',
              'desc' => 'prop_formit.successmessageplaceholder_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => 'fi.successMessage',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'The placeholder to set the success message to.',
              'area_trans' => '',
            ),
            'store' => 
            array (
              'name' => 'store',
              'desc' => 'prop_formit.store_desc',
              'type' => 'combo-boolean',
              'options' => '',
              'value' => false,
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'If true, will store the data in the cache for retrieval using the FormItRetriever snippet.',
              'area_trans' => '',
            ),
            'placeholderPrefix' => 
            array (
              'name' => 'placeholderPrefix',
              'desc' => 'prop_formit.placeholderprefix_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => 'fi.',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'The prefix to use for all placeholders set by FormIt for fields. Defaults to `fi.`',
              'area_trans' => '',
            ),
            'storeTime' => 
            array (
              'name' => 'storeTime',
              'desc' => 'prop_formit.storetime_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '300',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'If `store` is set to true, this specifies the number of seconds to store the data from the form submission. Defaults to five minutes.',
              'area_trans' => '',
            ),
            'storeLocation' => 
            array (
              'name' => 'storeLocation',
              'desc' => 'prop_formit.storelocation_desc',
              'type' => 'list',
              'options' => 
              array (
                0 => 
                array (
                  'value' => 'cache',
                  'text' => 'formit.opt_cache',
                  'name' => 'MODX Cache',
                ),
                1 => 
                array (
                  'value' => 'session',
                  'text' => 'formit.opt_session',
                  'name' => 'Session',
                ),
              ),
              'value' => 'cache',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'If `store` is set to true, this specifies the cache location of the data from the form submission. Defaults to MODX cache.',
              'area_trans' => '',
            ),
            'allowFiles' => 
            array (
              'name' => 'allowFiles',
              'desc' => 'prop_formit.allowfiles_desc',
              'type' => 'combo-boolean',
              'options' => '',
              'value' => true,
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'If set to 0, will prevent files from being submitted on the form.',
              'area_trans' => '',
            ),
            'spamEmailFields' => 
            array (
              'name' => 'spamEmailFields',
              'desc' => 'prop_formit.spamemailfields_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => 'email',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'If `spam` is set as a hook, a comma-separated list of fields containing emails to check spam against.',
              'area_trans' => '',
            ),
            'spamCheckIp' => 
            array (
              'name' => 'spamCheckIp',
              'desc' => 'prop_formit.spamcheckip_desc',
              'type' => 'combo-boolean',
              'options' => '',
              'value' => false,
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'If `spam` is set as a hook, and this is true, will check the IP as well.',
              'area_trans' => '',
            ),
            'recaptchaJs' => 
            array (
              'name' => 'recaptchaJs',
              'desc' => 'prop_formit.recaptchajs_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '{}',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'If `recaptcha` is set as a hook, this can be a JSON object that will be set to the JS RecaptchaOptions variable, which configures options for reCaptcha.',
              'area_trans' => '',
            ),
            'recaptchaTheme' => 
            array (
              'name' => 'recaptchaTheme',
              'desc' => 'prop_formit.recaptchatheme_desc',
              'type' => 'list',
              'options' => 
              array (
                0 => 
                array (
                  'value' => 'red',
                  'text' => 'formit.opt_red',
                  'name' => 'Red',
                ),
                1 => 
                array (
                  'value' => 'white',
                  'text' => 'formit.opt_white',
                  'name' => 'White',
                ),
                2 => 
                array (
                  'value' => 'clean',
                  'text' => 'formit.opt_clean',
                  'name' => 'Clean',
                ),
                3 => 
                array (
                  'value' => 'blackglass',
                  'text' => 'formit.opt_blackglass',
                  'name' => 'Black Glass',
                ),
              ),
              'value' => 'clean',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'If `recaptcha` is set as a hook, this will select a theme for the reCaptcha widget.',
              'area_trans' => '',
            ),
            'redirectTo' => 
            array (
              'name' => 'redirectTo',
              'desc' => 'prop_formit.redirectto_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'If `redirect` is set as a hook, this must specify the Resource ID to redirect to.',
              'area_trans' => '',
            ),
            'redirectParams' => 
            array (
              'name' => 'redirectParams',
              'desc' => 'prop_formit.redirectparams_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'A JSON array of parameters to pass to the redirect hook that will be passed when redirecting.',
              'area_trans' => '',
            ),
            'emailTo' => 
            array (
              'name' => 'emailTo',
              'desc' => 'prop_formit.emailto_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'If `email` is set as a hook, then this specifies the email(s) to send the email to. Can be a comma-separated list of email addresses.',
              'area_trans' => '',
            ),
            'emailToName' => 
            array (
              'name' => 'emailToName',
              'desc' => 'prop_formit.emailtoname_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Optional. If `email` is set as a hook, then this must be a parallel list of comma-separated names for the email addresses specified in the `emailTo` property.',
              'area_trans' => '',
            ),
            'emailFrom' => 
            array (
              'name' => 'emailFrom',
              'desc' => 'prop_formit.emailfrom_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Optional. If `email` is set as a hook, and this is set, will specify the From: address for the email. If not set, will first look for an `email` form field. If none is found, will default to the `emailsender` system setting.',
              'area_trans' => '',
            ),
            'emailFromName' => 
            array (
              'name' => 'emailFromName',
              'desc' => 'prop_formit.emailfromname_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Optional. If `email` is set as a hook, and this is set, will specify the From: name for the email.',
              'area_trans' => '',
            ),
            'emailReplyTo' => 
            array (
              'name' => 'emailReplyTo',
              'desc' => 'prop_formit.emailreplyto_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Optional. If `email` is set as a hook, and this is set, will specify the Reply-To: address for the email.',
              'area_trans' => '',
            ),
            'emailReplyToName' => 
            array (
              'name' => 'emailReplyToName',
              'desc' => 'prop_formit.emailreplytoname_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Optional. If `email` is set as a hook, and this is set, will specify the Reply-To: name for the email.',
              'area_trans' => '',
            ),
            'emailCC' => 
            array (
              'name' => 'emailCC',
              'desc' => 'prop_formit.emailcc_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'If `email` is set as a hook, then this specifies the email(s) to send the email to as a CC. Can be a comma-separated list of email addresses.',
              'area_trans' => '',
            ),
            'emailCCName' => 
            array (
              'name' => 'emailCCName',
              'desc' => 'prop_formit.emailccname_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Optional. If `email` is set as a hook, then this must be a parallel list of comma-separated names for the email addresses specified in the `emailCC` property.',
              'area_trans' => '',
            ),
            'emailBCC' => 
            array (
              'name' => 'emailBCC',
              'desc' => 'prop_formit.emailbcc_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'If `email` is set as a hook, then this specifies the email(s) to send the email to as a BCC. Can be a comma-separated list of email addresses.',
              'area_trans' => '',
            ),
            'emailBCCName' => 
            array (
              'name' => 'emailBCCName',
              'desc' => 'prop_formit.emailbccname_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Optional. If `email` is set as a hook, then this must be a parallel list of comma-separated names for the email addresses specified in the `emailBCC` property.',
              'area_trans' => '',
            ),
            'emailReturnPath' => 
            array (
              'name' => 'emailReturnPath',
              'desc' => 'prop_formit.emailreturnpath_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Optional. If `email` is set as a hook, and this is set, will specify the Return-path: address for the email. If not set, will take the value of `emailFrom` property.',
              'area_trans' => '',
            ),
            'emailSubject' => 
            array (
              'name' => 'emailSubject',
              'desc' => 'prop_formit.emailsubject_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'If `email` is set as a hook, this is required as a subject line for the email.',
              'area_trans' => '',
            ),
            'emailUseFieldForSubject' => 
            array (
              'name' => 'emailUseFieldForSubject',
              'desc' => 'prop_formit.emailusefieldforsubject_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'If the field `subject` is passed into the form, if this is true, it will use the field content for the subject line of the email.',
              'area_trans' => '',
            ),
            'emailHtml' => 
            array (
              'name' => 'emailHtml',
              'desc' => 'prop_formit.emailhtml_desc',
              'type' => 'combo-boolean',
              'options' => '',
              'value' => true,
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Optional. If `email` is set as a hook, this toggles HTML emails or not. Defaults to true.',
              'area_trans' => '',
            ),
            'emailConvertNewlines' => 
            array (
              'name' => 'emailConvertNewlines',
              'desc' => 'prop_formit.emailconvertnewlines_desc',
              'type' => 'combo-boolean',
              'options' => '',
              'value' => false,
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'If true and emailHtml is set to 1, will convert newlines to BR tags in the email.',
              'area_trans' => '',
            ),
            'emailMultiWrapper' => 
            array (
              'name' => 'emailMultiWrapper',
              'desc' => 'prop_formit.emailmultiwrapper_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '[[+value]]',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Will wrap each item in a collection of fields sent via checkboxes/multi-selects. Defaults to just the value.',
              'area_trans' => '',
            ),
            'emailMultiSeparator' => 
            array (
              'name' => 'emailMultiSeparator',
              'desc' => 'prop_formit.emailmultiseparator_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'The default separator for collections of items sent through checkboxes/multi-selects. Defaults to a newline.',
              'area_trans' => '',
            ),
            'fiarTpl' => 
            array (
              'name' => 'fiarTpl',
              'desc' => 'prop_formit.fiartpl_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'prop_formit.fiartpl_desc',
              'area_trans' => '',
            ),
            'fiarToField' => 
            array (
              'name' => 'fiarToField',
              'desc' => 'prop_formit.fiartofield_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => 'email',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'prop_formit.fiartofield_desc',
              'area_trans' => '',
            ),
            'fiarSubject' => 
            array (
              'name' => 'fiarSubject',
              'desc' => 'prop_formit.fiarsubject_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '[[++site_name]] Auto-Responder',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'prop_formit.fiarsubject_desc',
              'area_trans' => '',
            ),
            'fiarFrom' => 
            array (
              'name' => 'fiarFrom',
              'desc' => 'prop_formit.fiarfrom_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'prop_formit.fiarfrom_desc',
              'area_trans' => '',
            ),
            'fiarFromName' => 
            array (
              'name' => 'fiarFromName',
              'desc' => 'prop_formit.fiarfromname_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'prop_formit.fiarfromname_desc',
              'area_trans' => '',
            ),
            'fiarReplyTo' => 
            array (
              'name' => 'fiarReplyTo',
              'desc' => 'prop_formit.fiarreplyto_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'prop_formit.fiarreplyto_desc',
              'area_trans' => '',
            ),
            'fiarReplyToName' => 
            array (
              'name' => 'fiarReplyToName',
              'desc' => 'prop_formit.fiarreplytoname_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'prop_formit.fiarreplytoname_desc',
              'area_trans' => '',
            ),
            'fiarCC' => 
            array (
              'name' => 'fiarCC',
              'desc' => 'prop_formit.fiarcc_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'prop_formit.fiarcc_desc',
              'area_trans' => '',
            ),
            'fiarCCName' => 
            array (
              'name' => 'fiarCCName',
              'desc' => 'prop_fiar.fiarccname_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Optional. If `FormItAutoResponder` is set as a hook, then this must be a parallel list of comma-separated names for the email addresses specified in the `emailCC` property.',
              'area_trans' => '',
            ),
            'fiarBCC' => 
            array (
              'name' => 'fiarBCC',
              'desc' => 'prop_formit.fiarbcc_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'prop_formit.fiarbcc_desc',
              'area_trans' => '',
            ),
            'fiarBCCName' => 
            array (
              'name' => 'fiarBCCName',
              'desc' => 'prop_formit.fiarbccname_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'prop_formit.fiarbccname_desc',
              'area_trans' => '',
            ),
            'fiarHtml' => 
            array (
              'name' => 'fiarHtml',
              'desc' => 'prop_formit.fiarhtml_desc',
              'type' => 'combo-boolean',
              'options' => '',
              'value' => true,
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'prop_formit.fiarhtml_desc',
              'area_trans' => '',
            ),
            'mathMinRange' => 
            array (
              'name' => 'mathMinRange',
              'desc' => 'prop_formit.mathminrange_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '10',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'prop_formit.mathminrange_desc',
              'area_trans' => '',
            ),
            'mathMaxRange' => 
            array (
              'name' => 'mathMaxRange',
              'desc' => 'prop_formit.mathmaxrange_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '100',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'prop_formit.mathmaxrange_desc',
              'area_trans' => '',
            ),
            'mathField' => 
            array (
              'name' => 'mathField',
              'desc' => 'prop_formit.mathfield_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => 'math',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'prop_formit.mathfield_desc',
              'area_trans' => '',
            ),
            'mathOp1Field' => 
            array (
              'name' => 'mathOp1Field',
              'desc' => 'prop_formit.mathop1field_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => 'op1',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'prop_formit.mathop1field_desc',
              'area_trans' => '',
            ),
            'mathOp2Field' => 
            array (
              'name' => 'mathOp2Field',
              'desc' => 'prop_formit.mathop2field_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => 'op2',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'prop_formit.mathop2field_desc',
              'area_trans' => '',
            ),
            'mathOperatorField' => 
            array (
              'name' => 'mathOperatorField',
              'desc' => 'prop_formit.mathoperatorfield_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => 'operator',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'prop_formit.mathoperatorfield_desc',
              'area_trans' => '',
            ),
          ),
          'moduleguid' => '',
          'static' => false,
          'static_file' => '',
          'content' => '/**
 * FormIt
 *
 * Copyright 2009-2012 by Shaun McCormick <shaun@modx.com>
 *
 * FormIt is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option) any
 * later version.
 *
 * FormIt is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * FormIt; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
 * Suite 330, Boston, MA 02111-1307 USA
 *
 * @package formit
 */
/**
 * FormIt
 *
 * A dynamic form processing Snippet for MODx Revolution.
 *
 * @var modX $modx
 * @var array $scriptProperties
 *
 * @package formit
 */

$modelPath = $modx->getOption(\'formit.core_path\', null, $modx->getOption(\'core_path\', null, MODX_CORE_PATH) . \'components/formit/\') . \'model/formit/\';
$modx->loadClass(\'FormIt\', $modelPath, true, true);

$fi = new FormIt($modx,$scriptProperties);
$fi->initialize($modx->context->get(\'key\'));
$fi->loadRequest();

$fields = $fi->request->prepare();
return $fi->request->handle($fields);',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
        ),
      ),
      'polo_locations' => 
      array (
        'fields' => 
        array (
          'id' => 17,
          'source' => 1,
          'property_preprocess' => false,
          'name' => 'polo_locations',
          'description' => '',
          'editor_type' => 0,
          'category' => 0,
          'cache_type' => 0,
          'snippet' => 'if(!isset($modx)){
	    require_once $_SERVER[\'DOCUMENT_ROOT\'].\'/config.core.php\';
	    require_once MODX_CORE_PATH.\'model/modx/modx.class.php\';
	    $modx = new modX();
	    $modx->initialize(\'web\');
	}

$results = array();
$sortBy = (isset($sortBy)) ? $sortBy : "menuindex";
$sortOrder = (isset($sortOrder)) ? $sortOrder : "ASC";
// $pageType = (isset($pageType)) ? $pageType : "Page";
// $count = (isset($count)) ? $count : 1000;
$parentId = (isset($parentId)) ? $parentId : 1;
$templateId = (isset($templateId)) ? $templateId : 1;
//echo $parentId;
$c = $modx->newQuery(\'modResource\');
// $c->where(array(\'id\'=> $parentId ));
$c->where(array(\'template\'=> $templateId ));
$c->sortby($sortBy, $sortOrder);

$resources = $modx->getCollection( \'modResource\', $c );

$results = array();

if(!empty($resources)){
	foreach ( $resources as $resource ) {
		$t = array();
	    
	    $t["show_on_map"] = $resource->getTVValue( \'show_on_map\' );
	    if($t["show_on_map"] == 1){
	    	$parent = $modx->getObject(\'modResource\', $resource->parent);
		    $t["region"] = strtolower($resource->getTVValue(\'region\'));
	    	$club = $modx->getObject(\'modResource\', $resource->getTVValue(\'club\'));
			$t["club"] = $club->get(\'pagetitle\');
			$t["club_no_dash"] = str_replace("-", " ", $club->get(\'pagetitle\'));
		    $t["tags"] = strtolower($resource->getTVValue(\'tags\'));
		    $t["map_description"] = $resource->getTVValue(\'map_description\');
		    $t["club_image"] = $resource->getTVValue(\'club_image\');
		    $t["latitude"] = $resource->getTVValue(\'latitude\');
		    $t["longitude"] = $resource->getTVValue(\'longitude\');

		    $t["key"] = str_replace(" ", "", $parent->get(\'pagetitle\')) . "_" . $t["region"] . "_" . $t["club"] . "_" . $t["tags"];
		    
		    switch($t["tags"]){

		    	case \'featured\':
		    		$t["color"] = \'yellow\';
		    	break;
		    	case \'events\':
		    		$t["color"] = \'orange\';
		    	break;
		    	case \'games\':
		    		$t["color"] = \'blue\';
		    	break;
		    	case \'profiles\':
		    	default:
		    		$t["color"] = \'green\';
		    	break;

		    }

		    switch($t["tags"]){

		    	case \'featured\':
		    		$t["group"] = \'3\';
		    	break;
		    	case \'events\':
		    		$t["group"] = \'0\';
		    	break;
		    	case \'games\':
		    		$t["group"] = \'1\';
		    	break;
		    	case \'profiles\':
		    	default:
		    		$t["group"] = \'2\';
		    	break;

		    }

		    $data = array(
				"region_id" => $t["region"],
				"name" => $t["club_no_dash"],
				"description" => \'<img width="auto" height="50" style="float:left; margin-right: 10px;" src="\'  . $t["club_image"] . \'"/>\' . $t["map_description"],
				"lat" => $t["latitude"],
				"lng" => $t["longitude"],
				"url" => \'default\',
				"size" => \'80\',
				"type" => \'image\',
				"image_source" => \'location(\' . $t["color"]  . \').png\',
				"group" => $t["group"]		
			);

		    if(!empty($t["key"])){
		    	$results[makeKey($t["key"])] = $data;
		    }
		}
	}
}


echo json_encode($results);

function makeKey($text){

	return strtolower(str_replace(" ", "_", $text));
}',
          'locked' => false,
          'properties' => 
          array (
          ),
          'moduleguid' => '',
          'static' => true,
          'static_file' => 'polo_locations.php',
          'content' => 'if(!isset($modx)){
	    require_once $_SERVER[\'DOCUMENT_ROOT\'].\'/config.core.php\';
	    require_once MODX_CORE_PATH.\'model/modx/modx.class.php\';
	    $modx = new modX();
	    $modx->initialize(\'web\');
	}

$results = array();
$sortBy = (isset($sortBy)) ? $sortBy : "menuindex";
$sortOrder = (isset($sortOrder)) ? $sortOrder : "ASC";
// $pageType = (isset($pageType)) ? $pageType : "Page";
// $count = (isset($count)) ? $count : 1000;
$parentId = (isset($parentId)) ? $parentId : 1;
$templateId = (isset($templateId)) ? $templateId : 1;
//echo $parentId;
$c = $modx->newQuery(\'modResource\');
// $c->where(array(\'id\'=> $parentId ));
$c->where(array(\'template\'=> $templateId ));
$c->sortby($sortBy, $sortOrder);

$resources = $modx->getCollection( \'modResource\', $c );

$results = array();

if(!empty($resources)){
	foreach ( $resources as $resource ) {
		$t = array();
	    
	    $t["show_on_map"] = $resource->getTVValue( \'show_on_map\' );
	    if($t["show_on_map"] == 1){
	    	$parent = $modx->getObject(\'modResource\', $resource->parent);
		    $t["region"] = strtolower($resource->getTVValue(\'region\'));
	    	$club = $modx->getObject(\'modResource\', $resource->getTVValue(\'club\'));
			$t["club"] = $club->get(\'pagetitle\');
			$t["club_no_dash"] = str_replace("-", " ", $club->get(\'pagetitle\'));
		    $t["tags"] = strtolower($resource->getTVValue(\'tags\'));
		    $t["map_description"] = $resource->getTVValue(\'map_description\');
		    $t["club_image"] = $resource->getTVValue(\'club_image\');
		    $t["latitude"] = $resource->getTVValue(\'latitude\');
		    $t["longitude"] = $resource->getTVValue(\'longitude\');

		    $t["key"] = str_replace(" ", "", $parent->get(\'pagetitle\')) . "_" . $t["region"] . "_" . $t["club"] . "_" . $t["tags"];
		    
		    switch($t["tags"]){

		    	case \'featured\':
		    		$t["color"] = \'yellow\';
		    	break;
		    	case \'events\':
		    		$t["color"] = \'orange\';
		    	break;
		    	case \'games\':
		    		$t["color"] = \'blue\';
		    	break;
		    	case \'profiles\':
		    	default:
		    		$t["color"] = \'green\';
		    	break;

		    }

		    switch($t["tags"]){

		    	case \'featured\':
		    		$t["group"] = \'3\';
		    	break;
		    	case \'events\':
		    		$t["group"] = \'0\';
		    	break;
		    	case \'games\':
		    		$t["group"] = \'1\';
		    	break;
		    	case \'profiles\':
		    	default:
		    		$t["group"] = \'2\';
		    	break;

		    }

		    $data = array(
				"region_id" => $t["region"],
				"name" => $t["club_no_dash"],
				"description" => \'<img width="auto" height="50" style="float:left; margin-right: 10px;" src="\'  . $t["club_image"] . \'"/>\' . $t["map_description"],
				"lat" => $t["latitude"],
				"lng" => $t["longitude"],
				"url" => \'default\',
				"size" => \'80\',
				"type" => \'image\',
				"image_source" => \'location(\' . $t["color"]  . \').png\',
				"group" => $t["group"]		
			);

		    if(!empty($t["key"])){
		    	$results[makeKey($t["key"])] = $data;
		    }
		}
	}
}


echo json_encode($results);

function makeKey($text){

	return strtolower(str_replace(" ", "_", $text));
}',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
    ),
    'modTemplateVar' => 
    array (
      'live-stream' => 
      array (
        'fields' => 
        array (
          'id' => 23,
          'source' => 2,
          'property_preprocess' => false,
          'type' => 'text',
          'name' => 'live-stream',
          'caption' => 'Live stream link',
          'description' => '',
          'editor_type' => 0,
          'category' => 12,
          'locked' => false,
          'elements' => '',
          'rank' => 0,
          'display' => 'default',
          'default_text' => '',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
            'allowBlank' => 'true',
            'maxLength' => '',
            'minLength' => '',
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
          'id' => 2,
          'name' => 'Assets',
          'description' => 'SubAdmin Media Access',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
            'basePath' => 
            array (
              'name' => 'basePath',
              'desc' => 'prop_file.basePath_desc',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'assets/media/',
              'lexicon' => 'core:source',
            ),
            'baseUrl' => 
            array (
              'name' => 'baseUrl',
              'desc' => 'prop_file.baseUrl_desc',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'assets/media/',
              'lexicon' => 'core:source',
            ),
          ),
          'is_stream' => true,
        ),
      ),
      'link-1' => 
      array (
        'fields' => 
        array (
          'id' => 20,
          'source' => 2,
          'property_preprocess' => false,
          'type' => 'text',
          'name' => 'link-1',
          'caption' => 'Button 1 Link',
          'description' => 'use this for external streaming (espn, etc.) ',
          'editor_type' => 0,
          'category' => 9,
          'locked' => false,
          'elements' => '',
          'rank' => 0,
          'display' => 'default',
          'default_text' => '',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
            'allowBlank' => 'true',
            'maxLength' => '',
            'minLength' => '',
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
          'id' => 2,
          'name' => 'Assets',
          'description' => 'SubAdmin Media Access',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
            'basePath' => 
            array (
              'name' => 'basePath',
              'desc' => 'prop_file.basePath_desc',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'assets/media/',
              'lexicon' => 'core:source',
            ),
            'baseUrl' => 
            array (
              'name' => 'baseUrl',
              'desc' => 'prop_file.baseUrl_desc',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'assets/media/',
              'lexicon' => 'core:source',
            ),
          ),
          'is_stream' => true,
        ),
      ),
      'link-1-text' => 
      array (
        'fields' => 
        array (
          'id' => 19,
          'source' => 2,
          'property_preprocess' => false,
          'type' => 'text',
          'name' => 'link-1-text',
          'caption' => 'Button 1 Text',
          'description' => 'use this for external streaming (espn, etc.)',
          'editor_type' => 0,
          'category' => 9,
          'locked' => false,
          'elements' => '',
          'rank' => 0,
          'display' => 'default',
          'default_text' => '',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
            'allowBlank' => 'true',
            'maxLength' => '',
            'minLength' => '',
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
          'id' => 2,
          'name' => 'Assets',
          'description' => 'SubAdmin Media Access',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
            'basePath' => 
            array (
              'name' => 'basePath',
              'desc' => 'prop_file.basePath_desc',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'assets/media/',
              'lexicon' => 'core:source',
            ),
            'baseUrl' => 
            array (
              'name' => 'baseUrl',
              'desc' => 'prop_file.baseUrl_desc',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'assets/media/',
              'lexicon' => 'core:source',
            ),
          ),
          'is_stream' => true,
        ),
      ),
      'link-2' => 
      array (
        'fields' => 
        array (
          'id' => 22,
          'source' => 2,
          'property_preprocess' => false,
          'type' => 'text',
          'name' => 'link-2',
          'caption' => 'Button 2 Link',
          'description' => '',
          'editor_type' => 0,
          'category' => 9,
          'locked' => false,
          'elements' => '',
          'rank' => 0,
          'display' => 'default',
          'default_text' => '',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
            'allowBlank' => 'true',
            'maxLength' => '',
            'minLength' => '',
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
          'id' => 2,
          'name' => 'Assets',
          'description' => 'SubAdmin Media Access',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
            'basePath' => 
            array (
              'name' => 'basePath',
              'desc' => 'prop_file.basePath_desc',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'assets/media/',
              'lexicon' => 'core:source',
            ),
            'baseUrl' => 
            array (
              'name' => 'baseUrl',
              'desc' => 'prop_file.baseUrl_desc',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'assets/media/',
              'lexicon' => 'core:source',
            ),
          ),
          'is_stream' => true,
        ),
      ),
      'link-2-text' => 
      array (
        'fields' => 
        array (
          'id' => 21,
          'source' => 2,
          'property_preprocess' => false,
          'type' => 'text',
          'name' => 'link-2-text',
          'caption' => 'Button 2 Text',
          'description' => '',
          'editor_type' => 0,
          'category' => 9,
          'locked' => false,
          'elements' => '',
          'rank' => 0,
          'display' => 'default',
          'default_text' => '',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
            'allowBlank' => 'true',
            'maxLength' => '',
            'minLength' => '',
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
          'id' => 2,
          'name' => 'Assets',
          'description' => 'SubAdmin Media Access',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
            'basePath' => 
            array (
              'name' => 'basePath',
              'desc' => 'prop_file.basePath_desc',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'assets/media/',
              'lexicon' => 'core:source',
            ),
            'baseUrl' => 
            array (
              'name' => 'baseUrl',
              'desc' => 'prop_file.baseUrl_desc',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'assets/media/',
              'lexicon' => 'core:source',
            ),
          ),
          'is_stream' => true,
        ),
      ),
      'mp4-video' => 
      array (
        'fields' => 
        array (
          'id' => 6,
          'source' => 2,
          'property_preprocess' => false,
          'type' => 'image',
          'name' => 'mp4-video',
          'caption' => 'Mp4 Video',
          'description' => 'Both video types are required for browser compatibility',
          'editor_type' => 0,
          'category' => 9,
          'locked' => false,
          'elements' => '',
          'rank' => 0,
          'display' => 'default',
          'default_text' => '',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
          'id' => 2,
          'name' => 'Assets',
          'description' => 'SubAdmin Media Access',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
            'basePath' => 
            array (
              'name' => 'basePath',
              'desc' => 'prop_file.basePath_desc',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'assets/media/',
              'lexicon' => 'core:source',
            ),
            'baseUrl' => 
            array (
              'name' => 'baseUrl',
              'desc' => 'prop_file.baseUrl_desc',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'assets/media/',
              'lexicon' => 'core:source',
            ),
          ),
          'is_stream' => true,
        ),
      ),
      'show_on_map' => 
      array (
        'fields' => 
        array (
          'id' => 10,
          'source' => 2,
          'property_preprocess' => false,
          'type' => 'checkbox',
          'name' => 'show_on_map',
          'caption' => 'Show this match on map',
          'description' => '',
          'editor_type' => 0,
          'category' => 10,
          'locked' => false,
          'elements' => '1',
          'rank' => 0,
          'display' => 'default',
          'default_text' => '',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
            'allowBlank' => 'true',
            'columns' => '1',
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
          'id' => 2,
          'name' => 'Assets',
          'description' => 'SubAdmin Media Access',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
            'basePath' => 
            array (
              'name' => 'basePath',
              'desc' => 'prop_file.basePath_desc',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'assets/media/',
              'lexicon' => 'core:source',
            ),
            'baseUrl' => 
            array (
              'name' => 'baseUrl',
              'desc' => 'prop_file.baseUrl_desc',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'assets/media/',
              'lexicon' => 'core:source',
            ),
          ),
          'is_stream' => true,
        ),
      ),
      'region' => 
      array (
        'fields' => 
        array (
          'id' => 2,
          'source' => 2,
          'property_preprocess' => false,
          'type' => 'listbox',
          'name' => 'region',
          'caption' => 'Region',
          'description' => '',
          'editor_type' => 0,
          'category' => 8,
          'locked' => false,
          'elements' => 'North||South||East||West||Central',
          'rank' => 0,
          'display' => 'default',
          'default_text' => '',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
            'allowBlank' => 'true',
            'listWidth' => '',
            'title' => '',
            'typeAhead' => 'false',
            'typeAheadDelay' => '250',
            'forceSelection' => 'false',
            'listEmptyText' => '',
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
          'id' => 2,
          'name' => 'Assets',
          'description' => 'SubAdmin Media Access',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
            'basePath' => 
            array (
              'name' => 'basePath',
              'desc' => 'prop_file.basePath_desc',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'assets/media/',
              'lexicon' => 'core:source',
            ),
            'baseUrl' => 
            array (
              'name' => 'baseUrl',
              'desc' => 'prop_file.baseUrl_desc',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'assets/media/',
              'lexicon' => 'core:source',
            ),
          ),
          'is_stream' => true,
        ),
      ),
      'club' => 
      array (
        'fields' => 
        array (
          'id' => 11,
          'source' => 2,
          'property_preprocess' => false,
          'type' => 'resourcelist',
          'name' => 'club',
          'caption' => 'Club',
          'description' => '',
          'editor_type' => 0,
          'category' => 8,
          'locked' => false,
          'elements' => '',
          'rank' => 0,
          'display' => 'default',
          'default_text' => '',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
            'allowBlank' => '0',
            'showNone' => '1',
            'parents' => '32',
            'depth' => '10',
            'includeParent' => 'false',
            'limitRelatedContext' => '0',
            'where' => '',
            'limit' => '0',
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
          'id' => 2,
          'name' => 'Assets',
          'description' => 'SubAdmin Media Access',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
            'basePath' => 
            array (
              'name' => 'basePath',
              'desc' => 'prop_file.basePath_desc',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'assets/media/',
              'lexicon' => 'core:source',
            ),
            'baseUrl' => 
            array (
              'name' => 'baseUrl',
              'desc' => 'prop_file.baseUrl_desc',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'assets/media/',
              'lexicon' => 'core:source',
            ),
          ),
          'is_stream' => true,
        ),
      ),
      'tags' => 
      array (
        'fields' => 
        array (
          'id' => 3,
          'source' => 2,
          'property_preprocess' => false,
          'type' => 'listbox',
          'name' => 'tags',
          'caption' => 'Categories',
          'description' => 'Select the category this match applies to.',
          'editor_type' => 0,
          'category' => 8,
          'locked' => false,
          'elements' => 'Events||Games||Profiles||Featured',
          'rank' => 0,
          'display' => 'default',
          'default_text' => '',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
            'allowBlank' => 'true',
            'listWidth' => '',
            'title' => '',
            'typeAhead' => 'false',
            'typeAheadDelay' => '250',
            'forceSelection' => 'false',
            'listEmptyText' => '',
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
          'id' => 2,
          'name' => 'Assets',
          'description' => 'SubAdmin Media Access',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
            'basePath' => 
            array (
              'name' => 'basePath',
              'desc' => 'prop_file.basePath_desc',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'assets/media/',
              'lexicon' => 'core:source',
            ),
            'baseUrl' => 
            array (
              'name' => 'baseUrl',
              'desc' => 'prop_file.baseUrl_desc',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'assets/media/',
              'lexicon' => 'core:source',
            ),
          ),
          'is_stream' => true,
        ),
      ),
      'map_description' => 
      array (
        'fields' => 
        array (
          'id' => 12,
          'source' => 2,
          'property_preprocess' => false,
          'type' => 'text',
          'name' => 'map_description',
          'caption' => 'Map description',
          'description' => 'Brief text for popup',
          'editor_type' => 0,
          'category' => 10,
          'locked' => false,
          'elements' => '',
          'rank' => 0,
          'display' => 'default',
          'default_text' => '',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
            'allowBlank' => 'true',
            'maxLength' => '',
            'minLength' => '',
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
          'id' => 2,
          'name' => 'Assets',
          'description' => 'SubAdmin Media Access',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
            'basePath' => 
            array (
              'name' => 'basePath',
              'desc' => 'prop_file.basePath_desc',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'assets/media/',
              'lexicon' => 'core:source',
            ),
            'baseUrl' => 
            array (
              'name' => 'baseUrl',
              'desc' => 'prop_file.baseUrl_desc',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'assets/media/',
              'lexicon' => 'core:source',
            ),
          ),
          'is_stream' => true,
        ),
      ),
      'club_image' => 
      array (
        'fields' => 
        array (
          'id' => 13,
          'source' => 2,
          'property_preprocess' => false,
          'type' => 'image',
          'name' => 'club_image',
          'caption' => 'Club Logo',
          'description' => 'logo to be displayed on popup',
          'editor_type' => 0,
          'category' => 10,
          'locked' => false,
          'elements' => '',
          'rank' => 0,
          'display' => 'default',
          'default_text' => '',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
          'id' => 2,
          'name' => 'Assets',
          'description' => 'SubAdmin Media Access',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
            'basePath' => 
            array (
              'name' => 'basePath',
              'desc' => 'prop_file.basePath_desc',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'assets/media/',
              'lexicon' => 'core:source',
            ),
            'baseUrl' => 
            array (
              'name' => 'baseUrl',
              'desc' => 'prop_file.baseUrl_desc',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'assets/media/',
              'lexicon' => 'core:source',
            ),
          ),
          'is_stream' => true,
        ),
      ),
      'latitude' => 
      array (
        'fields' => 
        array (
          'id' => 14,
          'source' => 2,
          'property_preprocess' => false,
          'type' => 'text',
          'name' => 'latitude',
          'caption' => 'Latitude',
          'description' => 'Latitude cords',
          'editor_type' => 0,
          'category' => 10,
          'locked' => false,
          'elements' => '',
          'rank' => 0,
          'display' => 'default',
          'default_text' => '',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
            'allowBlank' => 'true',
            'maxLength' => '',
            'minLength' => '',
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
          'id' => 2,
          'name' => 'Assets',
          'description' => 'SubAdmin Media Access',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
            'basePath' => 
            array (
              'name' => 'basePath',
              'desc' => 'prop_file.basePath_desc',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'assets/media/',
              'lexicon' => 'core:source',
            ),
            'baseUrl' => 
            array (
              'name' => 'baseUrl',
              'desc' => 'prop_file.baseUrl_desc',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'assets/media/',
              'lexicon' => 'core:source',
            ),
          ),
          'is_stream' => true,
        ),
      ),
      'longitude' => 
      array (
        'fields' => 
        array (
          'id' => 15,
          'source' => 2,
          'property_preprocess' => false,
          'type' => 'text',
          'name' => 'longitude',
          'caption' => 'Longitude',
          'description' => 'Longitude cords',
          'editor_type' => 0,
          'category' => 10,
          'locked' => false,
          'elements' => '',
          'rank' => 0,
          'display' => 'default',
          'default_text' => '',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
            'allowBlank' => 'true',
            'maxLength' => '',
            'minLength' => '',
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
          'id' => 2,
          'name' => 'Assets',
          'description' => 'SubAdmin Media Access',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
            'basePath' => 
            array (
              'name' => 'basePath',
              'desc' => 'prop_file.basePath_desc',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'assets/media/',
              'lexicon' => 'core:source',
            ),
            'baseUrl' => 
            array (
              'name' => 'baseUrl',
              'desc' => 'prop_file.baseUrl_desc',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'assets/media/',
              'lexicon' => 'core:source',
            ),
          ),
          'is_stream' => true,
        ),
      ),
    ),
  ),
);