<?php  return array (
  'resourceClass' => 'modDocument',
  'resource' => 
  array (
    'id' => 188,
    'type' => 'document',
    'contentType' => 'text/html',
    'pagetitle' => 'Example Blog Post 1',
    'longtitle' => '',
    'description' => '',
    'alias' => 'post',
    'link_attributes' => '',
    'published' => 1,
    'pub_date' => 0,
    'unpub_date' => 0,
    'parent' => 187,
    'isfolder' => 0,
    'introtext' => '',
    'content' => '',
    'richtext' => 1,
    'template' => 11,
    'menuindex' => 0,
    'searchable' => 1,
    'cacheable' => 1,
    'createdby' => 2,
    'createdon' => 1481232078,
    'editedby' => 2,
    'editedon' => 1486740938,
    'deleted' => 0,
    'deletedon' => 0,
    'deletedby' => 0,
    'publishedon' => 1481298300,
    'publishedby' => 2,
    'menutitle' => '',
    'donthit' => 0,
    'privateweb' => 0,
    'privatemgr' => 0,
    'content_dispo' => 0,
    'hidemenu' => 0,
    'class_key' => 'modDocument',
    'context_key' => 'web',
    'content_type' => 1,
    'uri' => 'polo-blog/blogposts/post',
    'uri_override' => 0,
    'hide_children_in_tree' => 0,
    'show_in_tree' => 1,
    'properties' => NULL,
    'publish-date' => 
    array (
      0 => 'publish-date',
      1 => '2016-12-21 16:00:00',
      2 => 'default',
      3 => NULL,
      4 => 'date',
    ),
    'author' => 
    array (
      0 => 'author',
      1 => 'Ty',
      2 => 'default',
      3 => NULL,
      4 => 'text',
    ),
    'blog-title' => 
    array (
      0 => 'blog-title',
      1 => 'Example Blog Post 1',
      2 => 'default',
      3 => NULL,
      4 => 'text',
    ),
    'blog-img' => 
    array (
      0 => 'blog-img',
      1 => 'Screen Shot 2015-07-16 at 6.22.22 PM.png',
      2 => 'default',
      3 => NULL,
      4 => 'image',
    ),
    'blog-des' => 
    array (
      0 => 'blog-des',
      1 => '<p><img style="float: left;" src="assets/media/images/Screen%20Shot%202015-07-14%20at%2011.11.52%20PM.png" alt="" width="482" height="308" />Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>',
      2 => 'default',
      3 => NULL,
      4 => 'richtext',
    ),
    '_content' => '<!doctype html>
<head>
<meta charset="UTF-8">
<title>Polo Channel</title>
<link href="http://local.polo.com//css/main.css" rel="stylesheet" type="text/css" />
<link rel="icon" type="image/png" href="http://polochannel.com/favicon.png">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script type="text/javascript" src="http://local.polo.com//js/lightbox.min.js"></script>
<script type="text/javascript" src="http://local.polo.com//js/touchSwipe.min.js"></script>
<meta name="description" content="">
        <meta name="viewport" content="width=device-width,initial-scale=1.0">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

        <!-- THIS IS NEEDED FOR FURL -->
        <base href="http://local.polo.com/" />
        
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width,initial-scale=1.0">

        <!-- Facebook OpenGraph Data -->
        <meta property="og:title" content="Polo Channel" />
        <meta property="og:type" content="website" />
        <!-- If it\'s an article:
        <meta property="og:type" content="article" />
        <meta property="article:published_time" content = "" />
        -->
        <meta property="og:description" content="" />        
        <meta property="og:url" content="http://local.polo.com/"/>
        <meta property="og:image" content="http://local.polo.com/images/ogimage.jpg" />
        <meta property="og:site_name" content="Polo Channel" />


<script src="js/respond.min.js"></script>

<script src="http://local.polo.com//js/mapdata.js"></script>
<script src="http://local.polo.com//js/worldmap.js"></script>
<script src="http://local.polo.com//js/jquery.fastLiveFilter.js"></script>
<script src="http://local.polo.com//js/jquery.countdown.min.js"></script>
<script src="http://local.polo.com//js/modernizr.custom.js"></script>
<link rel="stylesheet" type="text/css" href="css/lightbox.min.css">
<meta name="google-site-verification" content="lyzPX5GCX9Yi9NxiLhZGlEnTRGSsqA6xKj1sbBaz_Yo" />
<link href="http://local.polo.com//css/normalize.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<script>
  (function(i,s,o,g,r,a,m){i[\'GoogleAnalyticsObject\']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,\'script\',\'https://www.google-analytics.com/analytics.js\',\'ga\');

  ga(\'create\', \'UA-82621934-1\', \'auto\');
  ga(\'send\', \'pageview\');

</script>
 <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
 <![endif]-->
<script>
    $(function() {
        $(\'#search_input\').fastLiveFilter(\'.search_list\');
        $(\'#search_input_mobile\').fastLiveFilter(\'.search_list\');
    });

       $(window).load(function() {
      $(\'#loading\').fadeOut("slow");
      hideEmpty();
    });

    function hideEmpty() {
$(".vids").each(function(){

 if($(this).children(\'tag:visible\').length == 0) {
   $(this.parentNode.parentNode).hide(); 
}
else {
 $(this.parentNode.parentNode).show();
}
});

}
</script>



<!--[if lt IE 9]>
<script>
document.createElement(\'video\');
</script>
<![endif]-->

</head>

<body>

<div id="loading">
  <img class="loading-image main-logo"src="images/mainlogo_black.png" alt="">
  <img class="loading-image" src="images/loading.gif" alt="Loading..." />
</div>

<div class="fixed-header">
  <div class="header">
    <div class="header3">
    <ul class="nav">
      <li><a href="/">Home</a></li>

      <li><a href="/">Videos</a></li>

      <li><a href="/polo-blog">News</a></li>


      <li><div><a href="/" class="subscribe">Subscribe</a></div></li>
    </ul>
</div>
      <ul class="headerlinks">
	      

      
          <li><a href="https://www.facebook.com/polochannelnetwork" target="_blank"><div class="fa fa-facebook-official  fa-lg"></div></a></li>
      <li><a href="https://instagram.com/thepolochannel" target="_blank"><div class="fa fa-instagram  fa-lg"></div></a></li>
      <li><a href="mailto:contact@horseplay.tv"><div class="fa fa-envelope  fa-lg"></div></a></li>
  </ul>
    <a href="/"><img src="images/mainlogo.png" alt="" height="70" class="headerimage"></a>
  </div>

</div>
<div class="spacer"></div>







<!-- _____________________________________________________________________________________ mobile menu !-->


<div class="mobile-nav">
  
  <div class="header-mobile">
    <ul class="mobile-right-box">

      <li><a href="https://www.facebook.com/polochannelnetwork" target="_blank"><div class="fa fa-facebook-official"></div></a></li>
      <li><a href="https://instagram.com/thepolochannel" target="_blank"><div class="fa fa-instagram"></div></a></li>
      <li><a href="mailto:contact@horseplay.tv"><div class="fa fa-envelope"></div></a></li>
       <li> 
          <ul class="menu-mobile">
              <li></li>
              <li></li>
              <li></li>
          </ul>
       </li>
    </ul>
      <a href="/"><img src="images/mainlogo.png" alt="" height="40" ></a>
  </div>
  

    <div class="nav-mobile">
      <div class="checkboxes mobile">
        <div class="x-mobile"><img src="images/x.png" width="20" alt=""></div>
         <div class="background-mobile"></div>
		    <div class="header3">
    <ul class="nav">
      <li><a href="/">Home</a></li>

      <li><a href="/">Videos</a></li>

      <li><a href="/polo-blog">News</a></li>


      <li><div><a href="/" class="subscribe">Subscribe</a></div></li>
    </ul>
</div>
      	 	<ul class="headerlinks">
	      

      
          <li><a href="https://www.facebook.com/polochannelnetwork" target="_blank"><div class="fa fa-facebook-official  fa-lg"></div></a></li>
      <li><a href="https://instagram.com/thepolochannel" target="_blank"><div class="fa fa-instagram  fa-lg"></div></a></li>
      <li><a href="mailto:contact@horseplay.tv"><div class="fa fa-envelope  fa-lg"></div></a></li>
  </ul>
    	</div>
    </div>
</div>

<br>


<div class="blog_page">
	<div class="blog-content">

		<div class="inline">
			<h4>Example Blog Post 1</h4>
			<h5>Ty | 12/21/16</h5>
			<div class="list-img" style="background: url(\'assets/media/Screen Shot 2015-07-16 at 6.22.22 PM.png\') no-repeat center center; background-size: cover;"></div><p><p><img style="float: left;" src="assets/media/images/Screen%20Shot%202015-07-14%20at%2011.11.52%20PM.png" alt="" width="482" height="308" />Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p></p>
		</div>
	</div>
</div>




<!-- javascript !-->

<script type="text/javascript"> 
$(\'.vids a\').simpleLightboxVideo();
</script>
<script src="js/custom.js"></script>

</body>
</html>
',
    '_isForward' => false,
  ),
  'contentType' => 
  array (
    'id' => 1,
    'name' => 'HTML',
    'description' => 'HTML content',
    'mime_type' => 'text/html',
    'file_extensions' => '',
    'headers' => NULL,
    'binary' => 0,
  ),
  'policyCache' => 
  array (
  ),
  'elementCache' => 
  array (
    '[[*description]]' => '',
    '[[$doc_head]]' => '<!doctype html>
<head>
<meta charset="UTF-8">
<title>Polo Channel</title>
<link href="http://local.polo.com//css/main.css" rel="stylesheet" type="text/css" />
<link rel="icon" type="image/png" href="http://polochannel.com/favicon.png">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script type="text/javascript" src="http://local.polo.com//js/lightbox.min.js"></script>
<script type="text/javascript" src="http://local.polo.com//js/touchSwipe.min.js"></script>
<meta name="description" content="">
        <meta name="viewport" content="width=device-width,initial-scale=1.0">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

        <!-- THIS IS NEEDED FOR FURL -->
        <base href="http://local.polo.com/" />
        
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width,initial-scale=1.0">

        <!-- Facebook OpenGraph Data -->
        <meta property="og:title" content="Polo Channel" />
        <meta property="og:type" content="website" />
        <!-- If it\'s an article:
        <meta property="og:type" content="article" />
        <meta property="article:published_time" content = "" />
        -->
        <meta property="og:description" content="" />        
        <meta property="og:url" content="http://local.polo.com/"/>
        <meta property="og:image" content="http://local.polo.com/images/ogimage.jpg" />
        <meta property="og:site_name" content="Polo Channel" />


<script src="js/respond.min.js"></script>

<script src="http://local.polo.com//js/mapdata.js"></script>
<script src="http://local.polo.com//js/worldmap.js"></script>
<script src="http://local.polo.com//js/jquery.fastLiveFilter.js"></script>
<script src="http://local.polo.com//js/jquery.countdown.min.js"></script>
<script src="http://local.polo.com//js/modernizr.custom.js"></script>
<link rel="stylesheet" type="text/css" href="css/lightbox.min.css">
<meta name="google-site-verification" content="lyzPX5GCX9Yi9NxiLhZGlEnTRGSsqA6xKj1sbBaz_Yo" />
<link href="http://local.polo.com//css/normalize.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<script>
  (function(i,s,o,g,r,a,m){i[\'GoogleAnalyticsObject\']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,\'script\',\'https://www.google-analytics.com/analytics.js\',\'ga\');

  ga(\'create\', \'UA-82621934-1\', \'auto\');
  ga(\'send\', \'pageview\');

</script>
 <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
 <![endif]-->
<script>
    $(function() {
        $(\'#search_input\').fastLiveFilter(\'.search_list\');
        $(\'#search_input_mobile\').fastLiveFilter(\'.search_list\');
    });

       $(window).load(function() {
      $(\'#loading\').fadeOut("slow");
      hideEmpty();
    });

    function hideEmpty() {
$(".vids").each(function(){

 if($(this).children(\'tag:visible\').length == 0) {
   $(this.parentNode.parentNode).hide(); 
}
else {
 $(this.parentNode.parentNode).show();
}
});

}
</script>



<!--[if lt IE 9]>
<script>
document.createElement(\'video\');
</script>
<![endif]-->

</head>',
    '[[$header3_subpage]]' => '<div class="header3">
    <ul class="nav">
      <li><a href="/">Home</a></li>

      <li><a href="/">Videos</a></li>

      <li><a href="/polo-blog">News</a></li>


      <li><div><a href="/" class="subscribe">Subscribe</a></div></li>
    </ul>
</div>',
    '[[*live-stream:notempty=`<li><a href=""><button class="show-stream" type="button">live stream </button></a></li>`]]' => '',
    '[[*link-1]]' => '',
    '[[*link-1-text]]' => '',
    '[[*link-1-text:notempty=`<li><a href="" class="append"><button type="button"></button></a><span id="append-back"></span></li>`]]' => '',
    '[[*link-2]]' => '',
    '[[*link-2-text]]' => '',
    '[[*link-2-text:notempty=`<li><a href=""><button type="button"></button></a></li>`]]' => '',
    '[[$buttons]]' => '<ul class="headerlinks">
	      

      
          <li><a href="https://www.facebook.com/polochannelnetwork" target="_blank"><div class="fa fa-facebook-official  fa-lg"></div></a></li>
      <li><a href="https://instagram.com/thepolochannel" target="_blank"><div class="fa fa-instagram  fa-lg"></div></a></li>
      <li><a href="mailto:contact@horseplay.tv"><div class="fa fa-envelope  fa-lg"></div></a></li>
  </ul>',
    '[[*blog-title]]' => 'Example Blog Post 1',
    '[[*author]]' => 'Ty',
    '[[*publish-date:strtotime:date=`%m/%d/%y`]]' => '12/21/16',
    '[[*blog-img]]' => 'assets/media/Screen Shot 2015-07-16 at 6.22.22 PM.png',
    '[[*blog-des]]' => '<p><img style="float: left;" src="assets/media/images/Screen%20Shot%202015-07-14%20at%2011.11.52%20PM.png" alt="" width="482" height="308" />Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>',
    '[[$doc_footer]]' => '
<!-- javascript !-->

<script type="text/javascript"> 
$(\'.vids a\').simpleLightboxVideo();
</script>
<script src="js/custom.js"></script>

</body>
</html>',
  ),
  'sourceCache' => 
  array (
    'modChunk' => 
    array (
      'doc_head' => 
      array (
        'fields' => 
        array (
          'id' => 1,
          'source' => 1,
          'property_preprocess' => false,
          'name' => 'doc_head',
          'description' => '',
          'editor_type' => 0,
          'category' => 3,
          'cache_type' => 0,
          'snippet' => '<!doctype html>
<head>
<meta charset="UTF-8">
<title>Polo Channel</title>
<link href="[[++site_url]]/css/main.css" rel="stylesheet" type="text/css" />
<link rel="icon" type="image/png" href="http://polochannel.com/favicon.png">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script type="text/javascript" src="[[++site_url]]/js/lightbox.min.js"></script>
<script type="text/javascript" src="[[++site_url]]/js/touchSwipe.min.js"></script>
<meta name="description" content="[[*description]]">
        <meta name="viewport" content="width=device-width,initial-scale=1.0">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

        <!-- THIS IS NEEDED FOR FURL -->
        <base href="[[++site_url]]" />
        
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width,initial-scale=1.0">

        <!-- Facebook OpenGraph Data -->
        <meta property="og:title" content="[[++site_name]]" />
        <meta property="og:type" content="website" />
        <!-- If it\'s an article:
        <meta property="og:type" content="article" />
        <meta property="article:published_time" content = "" />
        -->
        <meta property="og:description" content="[[*description]]" />        
        <meta property="og:url" content="[[++site_url]]"/>
        <meta property="og:image" content="[[++site_url]][[++social_image]]" />
        <meta property="og:site_name" content="[[++site_name]]" />


<script src="js/respond.min.js"></script>

<script src="[[++site_url]]/js/mapdata.js"></script>
<script src="[[++site_url]]/js/worldmap.js"></script>
<script src="[[++site_url]]/js/jquery.fastLiveFilter.js"></script>
<script src="[[++site_url]]/js/jquery.countdown.min.js"></script>
<script src="[[++site_url]]/js/modernizr.custom.js"></script>
<link rel="stylesheet" type="text/css" href="css/lightbox.min.css">
<meta name="google-site-verification" content="lyzPX5GCX9Yi9NxiLhZGlEnTRGSsqA6xKj1sbBaz_Yo" />
<link href="[[++site_url]]/css/normalize.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<script>
  (function(i,s,o,g,r,a,m){i[\'GoogleAnalyticsObject\']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,\'script\',\'https://www.google-analytics.com/analytics.js\',\'ga\');

  ga(\'create\', \'UA-82621934-1\', \'auto\');
  ga(\'send\', \'pageview\');

</script>
 <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
 <![endif]-->
<script>
    $(function() {
        $(\'#search_input\').fastLiveFilter(\'.search_list\');
        $(\'#search_input_mobile\').fastLiveFilter(\'.search_list\');
    });

       $(window).load(function() {
      $(\'#loading\').fadeOut("slow");
      hideEmpty();
    });

    function hideEmpty() {
$(".vids").each(function(){

 if($(this).children(\'tag:visible\').length == 0) {
   $(this.parentNode.parentNode).hide(); 
}
else {
 $(this.parentNode.parentNode).show();
}
});

}
</script>



<!--[if lt IE 9]>
<script>
document.createElement(\'video\');
</script>
<![endif]-->

</head>',
          'locked' => false,
          'properties' => 
          array (
          ),
          'static' => true,
          'static_file' => 'assets/chunks/layout/doc_head.html',
          'content' => '<!doctype html>
<head>
<meta charset="UTF-8">
<title>Polo Channel</title>
<link href="[[++site_url]]/css/main.css" rel="stylesheet" type="text/css" />
<link rel="icon" type="image/png" href="http://polochannel.com/favicon.png">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script type="text/javascript" src="[[++site_url]]/js/lightbox.min.js"></script>
<script type="text/javascript" src="[[++site_url]]/js/touchSwipe.min.js"></script>
<meta name="description" content="[[*description]]">
        <meta name="viewport" content="width=device-width,initial-scale=1.0">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

        <!-- THIS IS NEEDED FOR FURL -->
        <base href="[[++site_url]]" />
        
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width,initial-scale=1.0">

        <!-- Facebook OpenGraph Data -->
        <meta property="og:title" content="[[++site_name]]" />
        <meta property="og:type" content="website" />
        <!-- If it\'s an article:
        <meta property="og:type" content="article" />
        <meta property="article:published_time" content = "" />
        -->
        <meta property="og:description" content="[[*description]]" />        
        <meta property="og:url" content="[[++site_url]]"/>
        <meta property="og:image" content="[[++site_url]][[++social_image]]" />
        <meta property="og:site_name" content="[[++site_name]]" />


<script src="js/respond.min.js"></script>

<script src="[[++site_url]]/js/mapdata.js"></script>
<script src="[[++site_url]]/js/worldmap.js"></script>
<script src="[[++site_url]]/js/jquery.fastLiveFilter.js"></script>
<script src="[[++site_url]]/js/jquery.countdown.min.js"></script>
<script src="[[++site_url]]/js/modernizr.custom.js"></script>
<link rel="stylesheet" type="text/css" href="css/lightbox.min.css">
<meta name="google-site-verification" content="lyzPX5GCX9Yi9NxiLhZGlEnTRGSsqA6xKj1sbBaz_Yo" />
<link href="[[++site_url]]/css/normalize.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<script>
  (function(i,s,o,g,r,a,m){i[\'GoogleAnalyticsObject\']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,\'script\',\'https://www.google-analytics.com/analytics.js\',\'ga\');

  ga(\'create\', \'UA-82621934-1\', \'auto\');
  ga(\'send\', \'pageview\');

</script>
 <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
 <![endif]-->
<script>
    $(function() {
        $(\'#search_input\').fastLiveFilter(\'.search_list\');
        $(\'#search_input_mobile\').fastLiveFilter(\'.search_list\');
    });

       $(window).load(function() {
      $(\'#loading\').fadeOut("slow");
      hideEmpty();
    });

    function hideEmpty() {
$(".vids").each(function(){

 if($(this).children(\'tag:visible\').length == 0) {
   $(this.parentNode.parentNode).hide(); 
}
else {
 $(this.parentNode.parentNode).show();
}
});

}
</script>



<!--[if lt IE 9]>
<script>
document.createElement(\'video\');
</script>
<![endif]-->

</head>',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'header3_subpage' => 
      array (
        'fields' => 
        array (
          'id' => 33,
          'source' => 2,
          'property_preprocess' => false,
          'name' => 'header3_subpage',
          'description' => '',
          'editor_type' => 0,
          'category' => 0,
          'cache_type' => 0,
          'snippet' => '<div class="header3">
    <ul class="nav">
      <li><a href="/">Home</a></li>

      <li><a href="/">Videos</a></li>

      <li><a href="/polo-blog">News</a></li>


      <li><div><a href="/" class="subscribe">Subscribe</a></div></li>
    </ul>
</div>',
          'locked' => false,
          'properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '<div class="header3">
    <ul class="nav">
      <li><a href="/">Home</a></li>

      <li><a href="/">Videos</a></li>

      <li><a href="/polo-blog">News</a></li>


      <li><div><a href="/" class="subscribe">Subscribe</a></div></li>
    </ul>
</div>',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
          'id' => 2,
          'name' => 'Assets',
          'description' => 'SubAdmin Media Access',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
            'basePath' => 
            array (
              'name' => 'basePath',
              'desc' => 'prop_file.basePath_desc',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'assets/media/',
              'lexicon' => 'core:source',
            ),
            'baseUrl' => 
            array (
              'name' => 'baseUrl',
              'desc' => 'prop_file.baseUrl_desc',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'assets/media/',
              'lexicon' => 'core:source',
            ),
          ),
          'is_stream' => true,
        ),
      ),
      'buttons' => 
      array (
        'fields' => 
        array (
          'id' => 15,
          'source' => 1,
          'property_preprocess' => false,
          'name' => 'buttons',
          'description' => '',
          'editor_type' => 0,
          'category' => 3,
          'cache_type' => 0,
          'snippet' => '<ul class="headerlinks">
	      [[*live-stream:notempty=`<li><a href=""><button class="show-stream" type="button">live stream </button></a></li>`]]
[[*link-1-text:notempty=`<li><a href="[[*link-1]]" class="append"><button type="button">[[*link-1-text]]</button></a><span id="append-back"></span></li>`]]
      [[*link-2-text:notempty=`<li><a href="[[*link-2]]"><button type="button">[[*link-2-text]]</button></a></li>`]]
          <li><a href="https://www.facebook.com/polochannelnetwork" target="_blank"><div class="fa fa-facebook-official  fa-lg"></div></a></li>
      <li><a href="https://instagram.com/thepolochannel" target="_blank"><div class="fa fa-instagram  fa-lg"></div></a></li>
      <li><a href="mailto:contact@horseplay.tv"><div class="fa fa-envelope  fa-lg"></div></a></li>
  </ul>',
          'locked' => false,
          'properties' => 
          array (
          ),
          'static' => true,
          'static_file' => 'assets/chunks/layout/buttons.html',
          'content' => '<ul class="headerlinks">
	      [[*live-stream:notempty=`<li><a href=""><button class="show-stream" type="button">live stream </button></a></li>`]]
[[*link-1-text:notempty=`<li><a href="[[*link-1]]" class="append"><button type="button">[[*link-1-text]]</button></a><span id="append-back"></span></li>`]]
      [[*link-2-text:notempty=`<li><a href="[[*link-2]]"><button type="button">[[*link-2-text]]</button></a></li>`]]
          <li><a href="https://www.facebook.com/polochannelnetwork" target="_blank"><div class="fa fa-facebook-official  fa-lg"></div></a></li>
      <li><a href="https://instagram.com/thepolochannel" target="_blank"><div class="fa fa-instagram  fa-lg"></div></a></li>
      <li><a href="mailto:contact@horseplay.tv"><div class="fa fa-envelope  fa-lg"></div></a></li>
  </ul>',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'doc_footer' => 
      array (
        'fields' => 
        array (
          'id' => 2,
          'source' => 1,
          'property_preprocess' => false,
          'name' => 'doc_footer',
          'description' => '',
          'editor_type' => 0,
          'category' => 3,
          'cache_type' => 0,
          'snippet' => '
<!-- javascript !-->

<script type="text/javascript"> 
$(\'.vids a\').simpleLightboxVideo();
</script>
<script src="js/custom.js"></script>

</body>
</html>',
          'locked' => false,
          'properties' => 
          array (
          ),
          'static' => true,
          'static_file' => 'assets/chunks/layout/doc_footer.html',
          'content' => '
<!-- javascript !-->

<script type="text/javascript"> 
$(\'.vids a\').simpleLightboxVideo();
</script>
<script src="js/custom.js"></script>

</body>
</html>',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
    ),
    'modSnippet' => 
    array (
    ),
    'modTemplateVar' => 
    array (
      'live-stream' => 
      array (
        'fields' => 
        array (
          'id' => 23,
          'source' => 2,
          'property_preprocess' => false,
          'type' => 'text',
          'name' => 'live-stream',
          'caption' => 'Live stream link',
          'description' => '',
          'editor_type' => 0,
          'category' => 12,
          'locked' => false,
          'elements' => '',
          'rank' => 0,
          'display' => 'default',
          'default_text' => '',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
            'allowBlank' => 'true',
            'maxLength' => '',
            'minLength' => '',
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
          'id' => 2,
          'name' => 'Assets',
          'description' => 'SubAdmin Media Access',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
            'basePath' => 
            array (
              'name' => 'basePath',
              'desc' => 'prop_file.basePath_desc',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'assets/media/',
              'lexicon' => 'core:source',
            ),
            'baseUrl' => 
            array (
              'name' => 'baseUrl',
              'desc' => 'prop_file.baseUrl_desc',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'assets/media/',
              'lexicon' => 'core:source',
            ),
          ),
          'is_stream' => true,
        ),
      ),
      'link-1' => 
      array (
        'fields' => 
        array (
          'id' => 20,
          'source' => 2,
          'property_preprocess' => false,
          'type' => 'text',
          'name' => 'link-1',
          'caption' => 'Button 1 Link',
          'description' => 'use this for external streaming (espn, etc.) ',
          'editor_type' => 0,
          'category' => 9,
          'locked' => false,
          'elements' => '',
          'rank' => 0,
          'display' => 'default',
          'default_text' => '',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
            'allowBlank' => 'true',
            'maxLength' => '',
            'minLength' => '',
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
          'id' => 2,
          'name' => 'Assets',
          'description' => 'SubAdmin Media Access',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
            'basePath' => 
            array (
              'name' => 'basePath',
              'desc' => 'prop_file.basePath_desc',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'assets/media/',
              'lexicon' => 'core:source',
            ),
            'baseUrl' => 
            array (
              'name' => 'baseUrl',
              'desc' => 'prop_file.baseUrl_desc',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'assets/media/',
              'lexicon' => 'core:source',
            ),
          ),
          'is_stream' => true,
        ),
      ),
      'link-1-text' => 
      array (
        'fields' => 
        array (
          'id' => 19,
          'source' => 2,
          'property_preprocess' => false,
          'type' => 'text',
          'name' => 'link-1-text',
          'caption' => 'Button 1 Text',
          'description' => 'use this for external streaming (espn, etc.)',
          'editor_type' => 0,
          'category' => 9,
          'locked' => false,
          'elements' => '',
          'rank' => 0,
          'display' => 'default',
          'default_text' => '',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
            'allowBlank' => 'true',
            'maxLength' => '',
            'minLength' => '',
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
          'id' => 2,
          'name' => 'Assets',
          'description' => 'SubAdmin Media Access',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
            'basePath' => 
            array (
              'name' => 'basePath',
              'desc' => 'prop_file.basePath_desc',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'assets/media/',
              'lexicon' => 'core:source',
            ),
            'baseUrl' => 
            array (
              'name' => 'baseUrl',
              'desc' => 'prop_file.baseUrl_desc',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'assets/media/',
              'lexicon' => 'core:source',
            ),
          ),
          'is_stream' => true,
        ),
      ),
      'link-2' => 
      array (
        'fields' => 
        array (
          'id' => 22,
          'source' => 2,
          'property_preprocess' => false,
          'type' => 'text',
          'name' => 'link-2',
          'caption' => 'Button 2 Link',
          'description' => '',
          'editor_type' => 0,
          'category' => 9,
          'locked' => false,
          'elements' => '',
          'rank' => 0,
          'display' => 'default',
          'default_text' => '',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
            'allowBlank' => 'true',
            'maxLength' => '',
            'minLength' => '',
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
          'id' => 2,
          'name' => 'Assets',
          'description' => 'SubAdmin Media Access',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
            'basePath' => 
            array (
              'name' => 'basePath',
              'desc' => 'prop_file.basePath_desc',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'assets/media/',
              'lexicon' => 'core:source',
            ),
            'baseUrl' => 
            array (
              'name' => 'baseUrl',
              'desc' => 'prop_file.baseUrl_desc',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'assets/media/',
              'lexicon' => 'core:source',
            ),
          ),
          'is_stream' => true,
        ),
      ),
      'link-2-text' => 
      array (
        'fields' => 
        array (
          'id' => 21,
          'source' => 2,
          'property_preprocess' => false,
          'type' => 'text',
          'name' => 'link-2-text',
          'caption' => 'Button 2 Text',
          'description' => '',
          'editor_type' => 0,
          'category' => 9,
          'locked' => false,
          'elements' => '',
          'rank' => 0,
          'display' => 'default',
          'default_text' => '',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
            'allowBlank' => 'true',
            'maxLength' => '',
            'minLength' => '',
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
          'id' => 2,
          'name' => 'Assets',
          'description' => 'SubAdmin Media Access',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
            'basePath' => 
            array (
              'name' => 'basePath',
              'desc' => 'prop_file.basePath_desc',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'assets/media/',
              'lexicon' => 'core:source',
            ),
            'baseUrl' => 
            array (
              'name' => 'baseUrl',
              'desc' => 'prop_file.baseUrl_desc',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'assets/media/',
              'lexicon' => 'core:source',
            ),
          ),
          'is_stream' => true,
        ),
      ),
      'blog-title' => 
      array (
        'fields' => 
        array (
          'id' => 28,
          'source' => 2,
          'property_preprocess' => false,
          'type' => 'text',
          'name' => 'blog-title',
          'caption' => 'Blog Title',
          'description' => '',
          'editor_type' => 0,
          'category' => 17,
          'locked' => false,
          'elements' => '',
          'rank' => 0,
          'display' => 'default',
          'default_text' => '',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
            'allowBlank' => 'true',
            'maxLength' => '',
            'minLength' => '',
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
          'id' => 2,
          'name' => 'Assets',
          'description' => 'SubAdmin Media Access',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
            'basePath' => 
            array (
              'name' => 'basePath',
              'desc' => 'prop_file.basePath_desc',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'assets/media/',
              'lexicon' => 'core:source',
            ),
            'baseUrl' => 
            array (
              'name' => 'baseUrl',
              'desc' => 'prop_file.baseUrl_desc',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'assets/media/',
              'lexicon' => 'core:source',
            ),
          ),
          'is_stream' => true,
        ),
      ),
      'author' => 
      array (
        'fields' => 
        array (
          'id' => 29,
          'source' => 2,
          'property_preprocess' => false,
          'type' => 'text',
          'name' => 'author',
          'caption' => 'Blog Author',
          'description' => '',
          'editor_type' => 0,
          'category' => 17,
          'locked' => false,
          'elements' => '',
          'rank' => 0,
          'display' => 'default',
          'default_text' => '',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
            'allowBlank' => 'true',
            'maxLength' => '',
            'minLength' => '',
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
          'id' => 2,
          'name' => 'Assets',
          'description' => 'SubAdmin Media Access',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
            'basePath' => 
            array (
              'name' => 'basePath',
              'desc' => 'prop_file.basePath_desc',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'assets/media/',
              'lexicon' => 'core:source',
            ),
            'baseUrl' => 
            array (
              'name' => 'baseUrl',
              'desc' => 'prop_file.baseUrl_desc',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'assets/media/',
              'lexicon' => 'core:source',
            ),
          ),
          'is_stream' => true,
        ),
      ),
      'publish-date' => 
      array (
        'fields' => 
        array (
          'id' => 30,
          'source' => 2,
          'property_preprocess' => false,
          'type' => 'date',
          'name' => 'publish-date',
          'caption' => 'Publish Date',
          'description' => '',
          'editor_type' => 0,
          'category' => 17,
          'locked' => false,
          'elements' => '',
          'rank' => 0,
          'display' => 'default',
          'default_text' => '',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
            'allowBlank' => 'true',
            'disabledDates' => '',
            'disabledDays' => '',
            'minDateValue' => '',
            'minTimeValue' => '',
            'maxDateValue' => '',
            'maxTimeValue' => '',
            'startDay' => '',
            'timeIncrement' => '',
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
          'id' => 2,
          'name' => 'Assets',
          'description' => 'SubAdmin Media Access',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
            'basePath' => 
            array (
              'name' => 'basePath',
              'desc' => 'prop_file.basePath_desc',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'assets/media/',
              'lexicon' => 'core:source',
            ),
            'baseUrl' => 
            array (
              'name' => 'baseUrl',
              'desc' => 'prop_file.baseUrl_desc',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'assets/media/',
              'lexicon' => 'core:source',
            ),
          ),
          'is_stream' => true,
        ),
      ),
      'blog-img' => 
      array (
        'fields' => 
        array (
          'id' => 31,
          'source' => 2,
          'property_preprocess' => false,
          'type' => 'image',
          'name' => 'blog-img',
          'caption' => 'Blog Image',
          'description' => '',
          'editor_type' => 0,
          'category' => 17,
          'locked' => false,
          'elements' => '',
          'rank' => 0,
          'display' => 'default',
          'default_text' => '',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
            'allowBlank' => 'true',
            'disabledDates' => '',
            'disabledDays' => '',
            'minDateValue' => '',
            'minTimeValue' => '',
            'maxDateValue' => '',
            'maxTimeValue' => '',
            'startDay' => '',
            'timeIncrement' => '',
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
          'id' => 2,
          'name' => 'Assets',
          'description' => 'SubAdmin Media Access',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
            'basePath' => 
            array (
              'name' => 'basePath',
              'desc' => 'prop_file.basePath_desc',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'assets/media/',
              'lexicon' => 'core:source',
            ),
            'baseUrl' => 
            array (
              'name' => 'baseUrl',
              'desc' => 'prop_file.baseUrl_desc',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'assets/media/',
              'lexicon' => 'core:source',
            ),
          ),
          'is_stream' => true,
        ),
      ),
      'blog-des' => 
      array (
        'fields' => 
        array (
          'id' => 32,
          'source' => 2,
          'property_preprocess' => false,
          'type' => 'richtext',
          'name' => 'blog-des',
          'caption' => 'Blog Description',
          'description' => '',
          'editor_type' => 0,
          'category' => 17,
          'locked' => false,
          'elements' => '',
          'rank' => 0,
          'display' => 'default',
          'default_text' => '',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
            'allowBlank' => 'true',
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
          'id' => 2,
          'name' => 'Assets',
          'description' => 'SubAdmin Media Access',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
            'basePath' => 
            array (
              'name' => 'basePath',
              'desc' => 'prop_file.basePath_desc',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'assets/media/',
              'lexicon' => 'core:source',
            ),
            'baseUrl' => 
            array (
              'name' => 'baseUrl',
              'desc' => 'prop_file.baseUrl_desc',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'assets/media/',
              'lexicon' => 'core:source',
            ),
          ),
          'is_stream' => true,
        ),
      ),
    ),
  ),
);