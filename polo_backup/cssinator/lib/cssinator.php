<?php

if(isset($_POST["filename"])){
	$file = $_SERVER["DOCUMENT_ROOT"] . "/cssinator/css/" . $_POST["filename"] . ".css";
	$json = $_SERVER["DOCUMENT_ROOT"] . "/cssinator/css/" . $_POST["filename"] . ".json";
	$css = "";
	foreach($_POST["css"] as $class => $properties){
		$css .= "\n" . "." . $class . " {";
		foreach($properties as $k => $v){
			$css .= $k . " : " . $v . "; ";
		}
		$css .= "}";
	}
	//echo $file;
	file_put_contents($file, $css);
	file_put_contents($json, json_encode($_POST));
	//echo $css;
}

$filelist = glob($_SERVER["DOCUMENT_ROOT"] . "/cssinator/css/*.css");
