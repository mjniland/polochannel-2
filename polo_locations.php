<?php
if(!isset($modx)){
	    require_once $_SERVER['DOCUMENT_ROOT'].'/config.core.php';
	    require_once MODX_CORE_PATH.'model/modx/modx.class.php';
	    $modx = new modX();
	    $modx->initialize('web');
	}

$results = array();
$sortBy = (isset($sortBy)) ? $sortBy : "menuindex";
$sortOrder = (isset($sortOrder)) ? $sortOrder : "ASC";
// $pageType = (isset($pageType)) ? $pageType : "Page";
// $count = (isset($count)) ? $count : 1000;
$parentId = (isset($parentId)) ? $parentId : 1;
$templateId = (isset($templateId)) ? $templateId : 1;
//echo $parentId;
$c = $modx->newQuery('modResource');
// $c->where(array('id'=> $parentId ));
$c->where(array('template'=> $templateId ));
$c->sortby($sortBy, $sortOrder);

$resources = $modx->getCollection( 'modResource', $c );

$results = array();

if(!empty($resources)){
	foreach ( $resources as $resource ) {
		$t = array();
	    
	    $t["show_on_map"] = $resource->getTVValue( 'show_on_map' );
	    if($t["show_on_map"] == 1){
	    	$parent = $modx->getObject('modResource', $resource->parent);
		    $t["region"] = strtolower($resource->getTVValue('region'));
	    	$club = $modx->getObject('modResource', $resource->getTVValue('club'));
			$t["club"] = $club->get('pagetitle');
			$t["club_no_dash"] = str_replace("-", " ", $club->get('pagetitle'));
		    $t["tags"] = strtolower($resource->getTVValue('tags'));
		    $t["map_description"] = $resource->getTVValue('map_description');
		    $t["club_image"] = $resource->getTVValue('club_image');
		    $t["latitude"] = $resource->getTVValue('latitude');
		    $t["longitude"] = $resource->getTVValue('longitude');

		    $t["key"] = str_replace(" ", "", $parent->get('pagetitle')) . "_" . $t["region"] . "_" . $t["club"] . "_" . $t["tags"];
		    
		    switch($t["tags"]){

		    	case 'featured':
		    		$t["color"] = 'yellow';
		    	break;
		    	case 'events':
		    		$t["color"] = 'orange';
		    	break;
		    	case 'games':
		    		$t["color"] = 'blue';
		    	break;
		    	case 'profiles':
		    	default:
		    		$t["color"] = 'green';
		    	break;

		    }

		    switch($t["tags"]){

		    	case 'featured':
		    		$t["group"] = '3';
		    	break;
		    	case 'events':
		    		$t["group"] = '0';
		    	break;
		    	case 'games':
		    		$t["group"] = '1';
		    	break;
		    	case 'profiles':
		    	default:
		    		$t["group"] = '2';
		    	break;

		    }

		    $data = array(
				"region_id" => $t["region"],
				"name" => $t["club_no_dash"],
				"description" => '<img width="auto" height="50" style="float:left; margin-right: 10px;" src="'  . $t["club_image"] . '"/>' . $t["map_description"],
				"lat" => $t["latitude"],
				"lng" => $t["longitude"],
				"url" => 'default',
				"size" => '80',
				"type" => 'image',
				"image_source" => 'location(' . $t["color"]  . ').png',
				"group" => $t["group"]		
			);

		    if(!empty($t["key"])){
		    	$results[makeKey($t["key"])] = $data;
		    }
		}
	}
}


echo json_encode($results);

function makeKey($text){

	return strtolower(str_replace(" ", "_", $text));
}